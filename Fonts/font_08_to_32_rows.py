#!/usr/bin/python
from functools import partial

padding = b'\x00\x00\x00\x00\x00\x00\x00\x00'

inFile = "CP437.F08"
inFileHandle = open(inFile, 'rb')

outFile = "CP437_32.bin"
outFileHandle = open(outFile, 'wb')

for fontChunk in iter(partial(inFileHandle.read, 8), b''):
    outFileHandle.write(fontChunk)
    outFileHandle.write(padding)
    outFileHandle.write(padding)
    outFileHandle.write(padding)

inFileHandle.close()
outFileHandle.close()
