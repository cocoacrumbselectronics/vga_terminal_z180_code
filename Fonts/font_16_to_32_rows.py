#!/usr/bin/python
from functools import partial

padding = b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'

inFile = "font8x16u.bin"
inFileHandle = open(inFile, 'rb')

outFile = "font8x16_32.bin"
outFileHandle = open(outFile, 'wb')

for fontChunk in iter(partial(inFileHandle.read, 16), b''):
    outFileHandle.write(fontChunk)
    outFileHandle.write(padding)

inFileHandle.close()
outFileHandle.close()
