#!/bin/sh

rm ./src/*.bin > /dev/null 2>&1
rm ./src/*.lis > /dev/null 2>&1
rm ./src/*.map > /dev/null 2>&1
rm ./src/*.rom > /dev/null 2>&1
rm ./src/*.sym > /dev/null 2>&1
rm ./terminal* > /dev/null 2>&1

export myCWD=($PWD)

pushd .
cd /home/koen/Documents/Projects/Retro/Projects/Tools/z88dk/libsrc/_DEVELOPMENT/target/z180

zcc +z180                                                                                       \
    -mz180                                                                                      \
    -clib=sdcc_iy                                                                               \
    -startup=1                                                                                  \
    -L/home/koen/Documents/Projects/Retro/Projects/Tools/z88dk/libsrc/_DEVELOPMENT/lib/sdcc_iy  \
    @$myCWD/z180_project.lst                                                                    \
    -pragma-include:$myCWD/z180_pragma.inc                                                      \
    -o $myCWD/terminal                                                                          \
    -create-app -Cz"--org 0 --rombase=0x0000 --romsize=0x8000"                                  \
    --c-code-in-asm                                                                             \
    --list                                                                                      \
    -zorg=0                                                                                     \
    -m                                                                                          \
    -s 
    # -v                                                                                          \
    # -Cl-v                                                                                       \

popd


# $ zcc -help
# A config file must be specified with +file as the first argument

# zcc - Frontend for the z88dk Cross-C Compiler - v17056-d10b0f470-20201018

# Usage: zcc +[target] {options} {files}

# Options:

#    -v -verbose                  Output all commands that are run (-vn suppresses)
#    -h -help                     Display this text
#       -o                        Set the basename for linker output files
#       -specs                    Print out compiler specs

# CPU Targetting:
#       -m8080                    Generate output for the i8080
#       -m8085                    Generate output for the i8085
#       -mz80                     Generate output for the z80
#       -mz80n                    Generate output for the z80n
#       -mz180                    Generate output for the z180
#       -mr2k                     Generate output for the Rabbit 2000
#       -mr3k                     Generate output for the Rabbit 3000
#       -mgbz80                   Generate output for the Gameboy Z80

# Target options:
#       -subtype                  Set the target subtype
#       -clib                     Set the target clib type
#       -crt0                     Override the crt0 assembler file to use
#       -startuplib               Override STARTUPLIB - compiler base support routines
#       --no-crt                  Link without crt0 file
#       -startup                  Set the startup type
#       -zorg                     Set the origin (only certain targets)
#       -nostdlib                 If set ignore INCPATH, STARTUPLIB
#       -pragma-redirect          Redirect a function
#       -pragma-define            Define the option in zcc_opt.def
#       -pragma-output            Define the option in zcc_opt.def (same as above)
#       -pragma-export            Define the option in zcc_opt.def and export as public
#       -pragma-need              NEED the option in zcc_opt.def
#       -pragma-bytes             Dump a string of bytes zcc_opt.def
#       -pragma-include           Process include file containing pragmas

# Lifecycle options:
#       -m4                       Stop after processing m4 files
#    -E --preprocess-only         Stop after preprocessing files
#    -c --compile-only            Stop after compiling .c .s .asm files to .o files
#    -a --assemble-only           Stop after compiling .c .s files to .asm files
#    -S --assemble-only           Stop after compiling .c .s files to .asm files
#    -x                           Make a library out of source files
#       -create-app               Run appmake on the resulting binary to create emulator usable file

# M4 options:
#       -Cm                       Add an option to m4

# Preprocessor options:
#       -Cp                       Add an option to the preprocessor
#       -D                        Define a preprocessor option
#       -U                        Undefine a preprocessor option
#       -I                        Add an include directory for the preprocessor
#       -iquote                   Add a quoted include path for the preprocessor
#       -isystem                  Add a system include path for the preprocessor

# Compiler (all) options:
#       -compiler                 Set the compiler type from the command line (sccz80,sdcc)
#       --c-code-in-asm           Add C code to .asm files

# Compiler (sccz80) options:
#       -Cc                       Add an option to sccz80
#       -set-r2l-by-default       (sccz80) Use r2l calling convention by default
#       -O                        Set the peephole optimiser setting for copt
#       --opt-code-speed          Optimize for code speed (sccz80 only)

# Compiler (sdcc) options:
#       -Cs                       Add an option to sdcc
#       --opt-code-size           Optimize for code size (sdcc only)
#       -SO                       Set the peephole optimiser setting for sdcc-peephole
#       --fsigned-char            Use signed chars by default

# Compiler (clang/llvm) options:
#       -Cg                       Add an option to clang
#       -clang                    Stop after translating .c files to llvm ir
#       -llvm                     Stop after llvm-cbe generates new .cbe.c files
#       -Co                       Add an option to llvm-opt
#       -Cv                       Add an option to llvm-cbe
#       -zopt                     Enable llvm-optimizer (clang only)

# Assembler options:
#       -Ca                       Add an option to the assembler
#       -z80-verb                 Make the assembler more verbose

# Linker options:
#       -Cl                       Add an option to the linker
#       -bn                       Set the output file for the linker stage
#       -reloc-info               Generate binary file relocation information
#    -m -gen-map-file             Generate an output map of the final executable
#    -s -gen-symbol-file          Generate a symbol map of the final executable
#       --list                    Generate list files

# Appmake options:
#       -L                        Add a library search path
#       -l                        Add a library
#       -Cz                       Add an option to appmake

# Misc options:
#       -g                        Generate a global defc file of the final executable (-g -gp -gpf:filename)
#       -alias                    Define a command line alias
#       --lstcwd                  Paths in .lst files are relative to the current working dir
#       -custom-copt-rules        Custom user copt rules

# Argument Aliases:
