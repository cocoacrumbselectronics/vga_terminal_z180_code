#!/bin/sh

z88dk-dis -mz180 terminal.rom -x terminal.sym -s 0x0000 -e 0x0200


# z88dk disassembler

# z88dk-dis [options] [file]

#   -o <addr>      Address to load code to
#   -s <addr>      Address to start disassembling from
#   -e <addr>      Address to stop disassembling at
#   -mz80          Disassemble z80 code
#   -mz180         Disassemble z180 code
#   -mez80         Disassemble ez80 code (short mode)
#   -mz80n         Disassemble z80n code
#   -mr2k          Disassemble Rabbit 2000 code
#   -mr3k          Disassemble Rabbit 3000 code
#   -mr800         Disassemble R800 code
#   -mgbz80        Disassemble Gameboy z80 code
#   -m8080         Disassemble 8080 code (with z80 mnenomics)
#   -m8085         Disassemble 8085 code (with z80 mnenomics)
#   -x <file>      Symbol file to read
