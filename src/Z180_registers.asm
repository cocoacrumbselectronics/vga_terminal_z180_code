SECTION code_user

; ****************************************************************************
; Z180 register constants                                       
; ****************************************************************************

CNTLA0:     EQU     _kZ180RegBase + 0x00    ; ASCI Channel Control Register A chan 0
CNTLA1:     EQU     _kZ180RegBase + 0x01    ; ASCI Channel Control Register A chan 1
CNTLB0:     EQU     _kZ180RegBase + 0x02    ; ASCI Channel Control Register B chan 0
CNTLB1:     EQU     _kZ180RegBase + 0x03    ; ASCI Channel Control Register B chan 1
STAT0:      EQU     _kZ180RegBase + 0x04    ; ASCI Status Register channel 0
STAT1:      EQU     _kZ180RegBase + 0x05    ; ASCI Status Register channel 1
TDR0:       EQU     _kZ180RegBase + 0x06    ; ASCI Transmit Data Register channel 0
TDR1:       EQU     _kZ180RegBase + 0x07    ; ASCI Transmit Data Register channel 1
RDR0:       EQU     _kZ180RegBase + 0x08    ; ASCI Receive Register channel 0
RDR1:       EQU     _kZ180RegBase + 0x09    ; ASCI Receive Register channel 1
CNTR:       EQU     _kZ180RegBase + 0x0A    ; CSI/O Contol/Status Register
TRDR:       EQU     _kZ180RegBase + 0x0B    ; CSI/O Transmit/Receive Data Register
TMDR0L:     EQU     _kZ180RegBase + 0x0C    ; Timer Data Register Channel 0 Low
TMDR0H:     EQU     _kZ180RegBase + 0x0D    ; Timer Data Register Channel 0 High
RLDR0L:     EQU     _kZ180RegBase + 0x0E    ; Timer Reload Register Channel 0 Low
RLDR0H:     EQU     _kZ180RegBase + 0x0F    ; Timer Reload Register Channel 0 High
TCR:        EQU     _kZ180RegBase + 0x10    ; Timer Control Register
;   
ASCI0:      EQU     _kZ180RegBase + 0x12    ; ASCI Extension Control Register 0
ASCI1:      EQU     _kZ180RegBase + 0x13    ; ASCI Extension Control Register 1
TMDR1L:     EQU     _kZ180RegBase + 0x14    ; Timer Data Register Channel 1 Low
TMDR1H:     EQU     _kZ180RegBase + 0x15    ; Timer Data Register Channel 1 High
RLDR1L:     EQU     _kZ180RegBase + 0x16    ; Timer Reload Register Channel 1 Low
RLDR1H:     EQU     _kZ180RegBase + 0x17    ; Timer Reload Register Channel 1 High
FRC:        EQU     _kZ180RegBase + 0x18    ; Free Running Counter (read only)
;   
ASTC0L:     EQU     _kZ180RegBase + 0x1A    ; ASCI Time Constant Register ch 0 Low
ASTC0H:     EQU     _kZ180RegBase + 0x1B    ; ASCI Time Constant Register ch 0 High
ASTC1L:     EQU     _kZ180RegBase + 0x1C    ; ASCI Time Constant Register ch 1 Low
ASTC1H:     EQU     _kZ180RegBase + 0x1D    ; ASCI Time Constant Register ch 1 High
CMR:        EQU     _kZ180RegBase + 0x1E    ; Clock Multiplier Register
CCR         EQU     _kZ180RegBase + 0x1F    ; CPU Control Register
SAR0L:      EQU     _kZ180RegBase + 0x20    ; DMA Source Address Register ch 0 Low
SAR0H:      EQU     _kZ180RegBase + 0x21    ; DMA Source Address Register ch 0 High
SAR0B:      EQU     _kZ180RegBase + 0x22    ; DMA Source Address Register ch 0 B
DAR0L:      EQU     _kZ180RegBase + 0x23    ; DMA Destination Address Reg ch 0 Low
DAR0H:      EQU     _kZ180RegBase + 0x24    ; DMA Destination Address Reg ch 0 High
DAR0B:      EQU     _kZ180RegBase + 0x25    ; DMA Destination Address Reg ch 0 B
BCR0L:      EQU     _kZ180RegBase + 0x26    ; DMA Byte Count Register channel 0 Low
BCR0H:      EQU     _kZ180RegBase + 0x27    ; DMA Byte Count Register channel 0 High
MAR1L:      EQU     _kZ180RegBase + 0x28    ; DMA Memory Address Register ch 1 Low
MAR1H:      EQU     _kZ180RegBase + 0x29    ; DMA Memory Address Register ch 1 High
MAR1B:      EQU     _kZ180RegBase + 0x2A    ; DMA Memory Address Register ch 1 B
IAR1L:      EQU     _kZ180RegBase + 0x2B    ; DMA I/O Address Register channel 1 Low
IAR1H:      EQU     _kZ180RegBase + 0x2C    ; DMA I/O Address Register channel 1 High
IAR1B:      EQU     _kZ180RegBase + 0x2D    ; DMA I/O Address Register channel 1 B
BCR1L:      EQU     _kZ180RegBase + 0x2E    ; DMA Byte Count Register channel 1 Low
BCR1H:      EQU     _kZ180RegBase + 0x2F    ; DMA Byte Count Register channel 1 High
DSTAT:      EQU     _kZ180RegBase + 0x30    ; DMA Status Register
DMODE:      EQU     _kZ180RegBase + 0x31    ; DMA Mode Register
DCNTL:      EQU     _kZ180RegBase + 0x32    ; DMA/WAIT Control Register
IL:         EQU     _kZ180RegBase + 0x33    ; Interrupt Vector Register
ITC:        EQU     _kZ180RegBase + 0x34    ; INT/TRAP Control Register
;   
RCR:        EQU     _kZ180RegBase + 0x36    ; Refresh Control Register
;   
CBR:        EQU     _kZ180RegBase + 0x38    ; MMU Control Base Register
BBR:        EQU     _kZ180RegBase + 0x39    ; MMU Bank Base Register
CBAR:       EQU     _kZ180RegBase + 0x3A    ; MMU Common/Bank Register
;  
OMCR:       EQU     _kZ180RegBase + 0x3E    ; Operation Mode Control Register
ICR:        EQU     _kZ180RegBase + 0x3F    ; I/O Control Register

; ****************************************************************************
; Serial status register bits
; ****************************************************************************
; For use with the BIT mnemonic
ST_RDRF:    EQU     7                       ; Receive data register Full
ST_TDRE:    EQU     1                       ; Transmit data register empty

; For use with the TST mnemonic
CNTLA0_EFR  EQU     0x08

STAT0_RDRF  EQU     0x80
STAT0_OVRN  EQU     0x40
STAT0_PE    EQU     0x20
STAT0_FE    EQU     0x10
STAT0_RIE   EQU     0x08
STAT0_DCD0  EQU     0x04
STAT0_TDRE  EQU     0x02
STAT0_TIE   EQU     0x01

STAT1_RDRF  EQU     0x80
STAT1_OVRN  EQU     0x40
STAT1_PE    EQU     0x20
STAT1_FE    EQU     0x10
STAT1_RIE   EQU     0x08
STAT1_CTS1E EQU     0x04
STAT1_TDRE  EQU     0x02
STAT1_TIE   EQU     0x01
