; ****************************************************************************
; Bootstrap the Z180 on the VGA Retro Colour Terminal board
; ****************************************************************************

    INCLUDE "globals.asm"
    INCLUDE "Z180_registers.asm"

; ****************************************************************************

    PUBLIC  _initZ180

; ****************************************************************************

    EXTERN  _main

    ; From 6845.asm
    EXTERN  init6845StartingAddresses
    EXTERN  init6845

    ; From flash.asm
    EXTERN  doSettingsExist
    EXTERN  _installDefaultSettings

    ; From globals.asm
    EXTERN  _kTerminalFgBgColorSetting
    EXTERN  _kTerminalFontSetting

    ; From interruptVectorTable.asm
    EXTERN  asci0_setUp
    EXTERN  asci0_Flush_Rx

    ; From screenHandling.asm
    EXTERN  initScreen
    EXTERN  clearScreen
    
; ****************************************************************************

SECTION code_crt_start

; ****************************************************************************

_initZ180:
                DI                              ; Disable interrupts

; ****************************************************************************

                ; Move Z180 internal I/O address to kZ180RegBase
                LD      A, _kZ180RegBase        ; Start of Z180 internal I/O
                OUT0    (0x3F), A               ; Address ICR = 0x3F before relocation0

; ****************************************************************************

Z180Init:
                ; Set up a stack pointer
                LD      HL, _kStackPointer
                LD      SP, HL

; ****************************************************************************

                ; Check if setings already exist. 
                ; If not then install the default settings.
                CALL    doSettingsExist     ; Returns $FF if no settings were found
                CP      A, $FF
                CALL    Z, _installDefaultSettings

; ****************************************************************************

                ; Initialise Z180 serial port 0 and 1
                ;
                ; Baud rate = PHI/(baud rate divider x prescaler x sampling divider)
                ;   PHI = CPU clock input / 1 = 18.432/1 MHz = 18.432 MHz
                ;   Baud rate divider (1,2,4,8,16,32,or 64) = 1
                ;   Prescaler (10 or 30) = 10
                ;   Sampling divide rate (16 or 64) = 16
                ; Baud rate = 18,432,00 / ( 1 x 10 x 16) = 18432000 / 160 = 115200 baud
                ;
                ; Control Register A for channel 0 (CNTLA0) and channel 1 (CNTLA1)
                ;   Bit 7 = 0    Multiprocessor Mode Enable (MPE)
                ;   Bit 6 = 1    Receiver Enable (RE)
                ;   Bit 5 = 1    Transmitter Enable (TE)
                ;   Bit 4 = 0    REQUest to Send Output (RTS0) (0 = Low, 1 = High)
                ;   Bit 3 = 0    Multiprocessor Bit Receive/Error Flag (MPBR) (0 = Reset)
                ;   Bit 2 = 1    Data Format (0 = 7-bit, 1 = 8-bit)
                ;   Bit 1 = 0    Parity (0 = No Parity, 1 = Parity Enabled)
                ;   Bit 0 = 0    Stop Bits (0 = 1 Stop Bit, 1 = 2 Stop Bits)
                LD      A, 0b01100100
                OUT0    (CNTLA0), A             ; Set up CNTLA for channel 0
                OUT0    (CNTLA1), A             ; And the same for channel 1 

                ; Control Register B for channel 0 (CNTLB0)
                ;   Bit 7 = 0    Multiprocessor Bit Transmit (MPBT)
                ;   Bit 6 = 0    Multiprocessor Mode (MP)
                ;   Bit 5 = 0    Clear to Send/Prescale (CTC/PS) (0 = PHI/10, 1 = PHI/30)
                ;   Bit 4 = 0    Parity Even Odd (PEO) (0 = Even, 1 = Odd)
                ;   Bit 3 = 0    Divide Ratio (DR) (0 = divide 16, 1 = divide 64)
                ;   Bit 2 = 000  Source/Speed Select (SS2-SS0)
                ;    to 0        (0 = /1, 1 = /2, .. 6 = /64, 7 = External clock)
                LD      HL, baudrate_LookupTable        ; Point to beginning of baudrate lookup table
                LD      A, (_kTerminalBaudrateSetting)  ; Load baudrate setting from Flash
                LD      D, $00
                LD      E, A                            
                ADD     HL, DE                          ; Add index to HL
                LD      A, (HL)                         ; Load baudrate divider value
                OUT0    (CNTLB0), A                     ; Set up CNTLB for channel 0

                LD      A, 0b00000000                   ; And the same for channel 1 (connects to PS/2 keyboard
                OUT0    (CNTLB1), A                     ; which always run at 115200 baud).

                ; ASCI Status Register 0 (STAT0)
                ;   Bit 7 = ReadOnly     Receive Data Register Full (RDRF)
                ;   Bit 6 = ReadOnly     Overrun Error (OVRN)   
                ;   Bit 5 = ReadOnly     Parity Error (PE)   
                ;   Bit 4 = ReadOnly     Framing Error (FE)   
                ;   Bit 3 = ReadWrite    Receive Interrupt Enable (RIE)   
                ;   Bit 2 = ReadOnly     Data Carrier Detect (DCD0)   
                ;   Bit 1 = ReadOnly     Transmit Data Register Empty (TDRE)   
                ;   Bit 0 = ReadWrite    Transmit Interrupt Enable (TIE)
                LD      A, 0b00001000
                OUT0    (STAT0), A
                LD      A, 0b00000000
                OUT0    (STAT1), A

                ; Extension Control Register (ASCI0) [Z8S180/L180-class processors only)
                ;   Bit 7 = 0    Receive Interrupt Inhibit (0 = Inhibited, 1 = Enabled)
                ;   Bit 6 = 1    DCD0 Disable (0 = DCD0 Auto-enable Rx, 1 = Advisory)
                ;   Bit 5 = 1    CTS0 Disable (0 = CTS0 Auto-enable Tx, 1 = Advisory)
                ;   Bit 4 = 0    X1 Bit Clock (0 = CKA0/16 or 64, 1 = CKA0 is bit clock)
                ;   Bit 3 = 0    BRG0 Mode (0 = As S180, 1 = Enable 16-BRG counter)
                ;   Bit 2 = 0    Break Feature (0 = Enabled, 1 = Disabled)
                ;   Bit 1 = 0    Break Detect (0 = On, 1 = Off)
                ;   Bit 0 = 0    Send Break (0 = Normal transmit, 1 = Drive TXA Low)
                LD      A, 0b01100000
                OUT0    (ASCI0), A              ; Set up ASCI for channel 0
                OUT0    (ASCI1), A              ; And the same for channel 1 

                CALL    asci0_Flush_Rx          ; Prepare ASCI0 Rx FiFo buffer

; ****************************************************************************

                ; Refresh Control Register (RCR)
                ;   Bit 7 = 0    Refresh Enable (REFE) (0 = Disabled, 1 = Enabled)
                ;   Bit 6 = 0    Refresh Wait (REFW) (0 = 2 clocksm 3 = 3 clocks)
                ;   Bit 1-0 = 0  Cycle Interval (CYC1-0) 
                LD      A, 0b00000000
                OUT0    (RCR), A                ; Turn off memory refresh

                ; DMA/WAIT Control Register (DCNTL)
                ;   Bit 7-6 = 00 Memory Wait Insertion (MWI1-0) (0 to 3 wait states)
                ;   Bit 5-4 = 11 I/O Wait Insertion (IWI1-0) (0 to 3 wait states) 
                ;   Bit 3 = 0    DMA REQUest Sense ch 1 (DMS1) (0 = Level, 1 = Edge)
                ;   Bit 2 = 0    DMA REQUest Sense ch 0 (DMS0) (0 = Level, 1 = Edge)
                ;   Bit 1 = 0    DMA Channel 1 Mode (DIM1) (0 = Mem to I/O, 1 = I/O to Mem)
                ;   Bit 0 = 0    DMA Channel 0 Mode (DIM0) (0 = MARI+1, 1 = MARI-1)
                ;   Bit 3-2 = 00 DMA REQUest Sense (DMS1-0) (0 = Level, 1 = Edge)
                LD      A, 0b00110000
                OUT0    (DCNTL), A              ; Maximum wait states for I/O and Memory

                ; Operating Mode Control Register (OMCR)
                ;   Bit 7 = 0    M1 Enable (M1E) (1 = Default, 0 = Z80 compatibility mode)
                ;   Bit 6 = 0    M1 Temporary Enable (M1TE) (1 = Default, 0 = Z80 mode)
                ;   Bit 5 = 0    I/O Compatibility (IOC) (1 = Default, 0 = Z80 mode)
                ;   Bits 4-0 = 0 Reserved
                LD      A, 0b00000000
                OUT0    (OMCR), A               ; Select Z80 compatibility mode

                ; CPU Control Register (CCR)
                ;   Bit 7 = 1    CPU Clock Divide (0 = Divide 2, 1 = Divide 1)
                ;   Bit 6+3 = 0  Standy/Idle Mode (00 = No Standby, 01 = ...)
                ;   Bit 5 = 0    Bus REQUest Exit (0 = Ignore in standby, 1 = Exit on BR)
                ;   Bit 4 = 0    PHI Clock Drive (0 = Standard, 1 = 33%)
                ;   Bit 3+6 = 0  Standy/Idle Mode (00 = No Standby, 01 = ...)
                ;   Bit 2 = 0    External I/O Drive (0 = Standard, 1 = 33%)
                ;   Bit 1 = 0    Control Signal Drive (0 = Standard, 1 = 33%)
                ;   Bit 0 = 0    Address and Data Drive (0 = Standard, 1 = 33%)
                LD      A, 0b10000000
                OUT0    (CCR), A

                ; Set reload registers for 1ms timer
                ; Timer input clock is PHI/20 = 18,432,000MHz/20 = 921.6kHz
                ; Relead time to give 1ms timer is 921.6 (0x039A when rounded up)
                LD      A, 0x9A
                OUT0    (RLDR0L), A
                LD      A, 0x03
                OUT0    (RLDR0H), A             ; equals 0.999566160521 milliseconds

                ; Timer Control Register
                ;   Bit 7 = 0    Timer 1 interrupt flag
                ;   Bit 6 = 0    Timer 0 interrupt flag
                ;   Bit 5 = 0    Timer 1 interrupt enable
                ;   Bit 4 = 0    Timer 0 interrupt enable
                ;   Bit 3-2 = 00 Inhibit timer output on A18/Tout
                ;   Bit 1 = 0    Timer 1 down count enable
                ;   Bit 0 = 1    Timer 0 down count enable
                LD      A, 0b00000001
                OUT0    (TCR), A

; ****************************************************************************


                LD      A, (_kTerminalFgBgColorSetting) ; Copy the default color setting into RAM so 
                LD      (_kFontBgColor), A              ; that we can easily change it on the fly.
                ;
                LD      A, (_kTerminalFontSetting)
                OR      A, $80                          ; Switch on status LED as well.
                OUT     0x20
                ;
                ; Initialize the HD68B45SP CRT chip.
                CALL    init6845
                CALL    init6845StartingAddresses
                CALL    initScreen
                CALL    clearScreen

; ****************************************************************************

                ; Set up interrupt vector table
                ;
                ; At this point, the Z88DK provided setup will have copied the 
                ; interrupt vector table to the CRT_ORG_VECTOR_TABLE specified
                ; location (here it is 0x8000, the start of the user RAM). So
                ; it's not needed to provide our own interrupt vector table
                ; COPY code.
                ; 
                ; IM    2                   ; Z88DK sets this up for us via 
                                            ; CRT_INTERRUPT_MODE in z180_pragma.inc
                EI                          ; Enable interrupts

; ****************************************************************************

                JP      _main               ; Jump to main() C code.

; ****************************************************************************

baudrate_LookupTable:
                DEFB    $2D     ;    300 baud
                DEFB    $2C     ;    600 baud
                DEFB    $2B     ;   1200 baud
                DEFB    $2A     ;   2400 baud
                DEFB    $29     ;   4800 baud
                DEFB    $28     ;   9600 baud
                DEFB    $21     ;  19200 baud
                DEFB    $20     ;  38400 baud
                DEFB    $01     ;  57600 baud
                DEFB    $00     ; 115200 baud

; ****************************************************************************
