#ifndef __COLORS_H__
#define __COLORS_H__

// Foreground (font) colors
#define fg_BLACK            (0x00)
#define fg_BLUE             (0x01)
#define fg_GREEN            (0x02)
#define fg_CYAN             (0x03)
#define fg_RED              (0x04)
#define fg_MAGENTA          (0x05)
#define fg_YELLOW           (0x06)
#define fg_WHITE            (0x07)
#define fg_BRIGHT_BLACK     (0x00 + 0x08)
#define fg_BRIGHT_BLUE      (0x01 + 0x08)
#define fg_BRIGHT_GREEN     (0x02 + 0x08)
#define fg_BRIGHT_CYAN      (0x03 + 0x08)
#define fg_BRIGHT_RED       (0x04 + 0x08)
#define fg_BRIGHT_MAGENTA   (0x05 + 0x08)
#define fg_BRIGHT_YELLOW    (0x06 + 0x08)
#define fg_BRIGHT_WHITE     (0x07 + 0x08)

// Background colors
#define bg_BLACK            (0x00)
#define bg_BLUE             (0x10)
#define bg_GREEN            (0x20)
#define bg_CYAN             (0x30)
#define bg_RED              (0x40)
#define bg_MAGENTA          (0x50)
#define bg_YELLOW           (0x60)
#define bg_WHITE            (0x70)
#define bg_BRIGHT_BLACK     (0x00 + 0x80)
#define bg_BRIGHT_BLUE      (0x10 + 0x80)
#define bg_BRIGHT_GREEN     (0x20 + 0x80)
#define bg_BRIGHT_CYAN      (0x30 + 0x80)
#define bg_BRIGHT_RED       (0x40 + 0x80)
#define bg_BRIGHT_MAGENTA   (0x50 + 0x80)
#define bg_BRIGHT_YELLOW    (0x60 + 0x80)
#define bg_BRIGHT_WHITE     (0x70 + 0x80)

#endif /* __COLORS_H__ */