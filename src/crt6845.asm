SECTION code_crt_init

; ****************************************************************************
; Utility code to setup the 6845 CRT as used by the VGA terminal board
; ****************************************************************************

    PUBLIC  init6845

    PUBLIC  update6845StartAddressRegisters
    PUBLIC  update6845CursorRegisters
    PUBLIC  init6845StartingAddresses

    ; From globals.asm
    EXTERN  _kTerminalScreenSizeSetting
    EXTERN  _kTerminalWidth
    EXTERN  k6845StartingAddress
    EXTERN  k6845CursorStartAddress
    EXTERN  k6845StartAddress
    EXTERN  _k6845StartAddressL
    EXTERN  _kVRAMStartAddress
    EXTERN  _kCRAMStartAddress
    EXTERN  _kCursorPosH
    EXTERN  _kCursorPosV
    EXTERN  _kCurrentVideoOffset
    EXTERN  _kCurrentCursor_pos

    EXTERN  _kTerminalLines
    EXTERN  _kTerminalLinesConf0
    EXTERN  _kTerminalLinesConf1
    EXTERN  _kTerminalLinesConf2
    EXTERN  _kTerminalLinesConf3
    EXTERN  _kTerminalLinesConf4
    EXTERN  _kTerminalLinesConf5

; ****************************************************************************

; Inits 6845 CRT for a 80 by X display using 8 or 16 scan lines height 
; characters and no empty scan lines (empty lines are part of the character 
; itself). 
;
;       80 by 24
;       $ python 6845.py 
;       ========================================
;       RO  :  99 (0x63) - Nr of Horizontal Characters Total.
;       R1  :  80 (0x50) - Nr of Horizontal Characters Displayed.
;       R2  :  83 (0x53) - Horizontal Sync Position.
;       R3  :   6 (0x 6) - Sync width.
;       R4  :  31 (0x1f) - Vertical Total.
;       R5  :  13 (0x d) - Vertical Total Adjustment.
;       R6  :  24 (0x18) - Nr of Vertical Characters Displayed.
;       R7  :  27 (0x1b) - Vertical Sync Position (might need manual fine tuning).
;       R8  :   0 (0x 0) - Interlace Mode.
;       R9  :  15 (0x f) - Max Scanline Address.
;       R10 : 205 (0xcd) - Cursor Start Scan Line.
;       R11 : 207 (0xcf) - Cursor Stop Scan Line.
;       R12 :  -1 (0xff) - Start Address (High). Real start address is 0x0000.
;       R13 :  -1 (0xff) - Start Address (Low). Real start address is 0x0000.
;       R14 :  -1 (0xff) - Cursor Start Address (High). Cursor will be at position (0, 0).
;       R15 :  -1 (0xff) - Cursor Start Address (Low). Cursor will be at position (0, 0).
;       ========================================
; 
;       80 by 25
;       $ python 6845.py 
;       ========================================
;       RO  :  99 (0x63) - Nr of Horizontal Characters Total.
;       R1  :  80 (0x50) - Nr of Horizontal Characters Displayed.
;       R2  :  83 (0x53) - Horizontal Sync Position.
;       R3  :   6 (0x 6) - Sync width.
;       R4  :  31 (0x1f) - Vertical Total.
;       R5  :  13 (0x d) - Vertical Total Adjustment.
;       R6  :  25 (0x19) - Nr of Vertical Characters Displayed.
;       R7  :  27 (0x1d) - Vertical Sync Position (might need manual fine tuning).
;       R8  :   0 (0x 0) - Interlace Mode.
;       R9  :  15 (0x f) - Max Scanline Address.
;       R10 : 205 (0xcd) - Cursor Start Scan Line.
;       R11 : 207 (0xcf) - Cursor Stop Scan Line.
;       R12 :  -1 (0xff) - Start Address (High). Real start address is 0x0000.
;       R13 :  -1 (0xff) - Start Address (Low). Real start address is 0x0000.
;       R14 :  -1 (0xff) - Cursor Start Address (High). Cursor will be at position (0, 0).
;       R15 :  -1 (0xff) - Cursor Start Address (Low). Cursor will be at position (0, 0).
;       ========================================
;
;       80 by 30 
;       $ python 6845.py 
;       ========================================
;       RO  :  99 (0x63) - Nr of Horizontal Characters Total.
;       R1  :  80 (0x50) - Nr of Horizontal Characters Displayed.
;       R2  :  83 (0x53) - Horizontal Sync Position.
;       R3  :   6 (0x 6) - Sync width.
;       R4  :  31 (0x1f) - Vertical Total.
;       R5  :  13 (0x d) - Vertical Total Adjustment.
;       R6  :  30 (0x1e) - Nr of Vertical Characters Displayed.
;       R7  :  30 (0x1E) - Vertical Sync Position (might need manual fine tuning).
;       R8  :   0 (0x 0) - Interlace Mode.
;       R9  :  15 (0x f) - Max Scanline Address.
;       R10 : 205 (0xcd) - Cursor Start Scan Line.
;       R11 : 207 (0xcf) - Cursor Stop Scan Line.
;       R12 :  -1 (0xff) - Start Address (High). Real start address is 0x0000.
;       R13 :  -1 (0xff) - Start Address (Low). Real start address is 0x0000.
;       R14 :  -1 (0xff) - Cursor Start Address (High). Cursor will be at position (0, 0).
;       R15 :  -1 (0xff) - Cursor Start Address (Low). Cursor will be at position (0, 0).
;       ========================================
;
;       80 by 43
;       $ python 6845.py 
;       ========================================
;       RO  :  99 (0x63) - Nr of Horizontal Characters Total.
;       R1  :  80 (0x50) - Nr of Horizontal Characters Displayed.
;       R2  :  83 (0x53) - Horizontal Sync Position.
;       R3  :   6 (0x 6) - Sync width.
;       R4  :  46 (0x2e) - Vertical Total.
;       R5  :   8 (0x 8) - Vertical Total Adjustment.
;       R6  :  43 (0x2b) - Nr of Vertical Characters Displayed.
;       R7  :  43 (0x2b) - Vertical Sync Position (might need manual fine tuning).
;       R8  :   0 (0x 0) - Interlace Mode.
;       R9  :  10 (0x a) - Max Scanline Address.
;       R10 : 192 (0xc8) - Cursor Start Scan Line.
;       R11 : 202 (0xca) - Cursor Stop Scan Line.
;       R12 :  -1 (0xff) - Start Address (High). Real start address is 0x0000.
;       R13 :  -1 (0xff) - Start Address (Low). Real start address is 0x0000.
;       R14 :  -1 (0xff) - Cursor Start Address (High). Cursor will be at position (0, 0).
;       R15 :  -1 (0xff) - Cursor Start Address (Low). Cursor will be at position (0, 0).
;       ========================================
;
;       80 by 51
;       $ python 6845.py 
;       ========================================
;       RO  :  99 (0x63) - Nr of Horizontal Characters Total.
;       R1  :  80 (0x50) - Nr of Horizontal Characters Displayed.
;       R2  :  83 (0x53) - Horizontal Sync Position.
;       R3  :   6 (0x 6) - Sync width.
;       R4  :  57 (0x40) - Vertical Total.
;       R5  :   3 (0x 5) - Vertical Total Adjustment.
;       R6  :  51 (0x33) - Nr of Vertical Characters Displayed.
;       R7  :  54 (0x37) - Vertical Sync Position (might need manual fine tuning).
;       R8  :   0 (0x 0) - Interlace Mode.
;       R9  :   8 (0x 7) - Max Scanline Address.
;       R10 : 198 (0xc5) - Cursor Start Scan Line.
;       R11 : 200 (0xc7) - Cursor Stop Scan Line.
;       R12 :  -1 (0xff) - Start Address (High). Real start address is 0x0000.
;       R13 :  -1 (0xff) - Start Address (Low). Real start address is 0x0000.
;       R14 :  -1 (0xff) - Cursor Start Address (High). Cursor will be at position (0, 0).
;       R15 :  -1 (0xff) - Cursor Start Address (Low). Cursor will be at position (0, 0).
;       ========================================
;
;       80 by 60
;       $ python 6845.py
;       ========================================
;       RO  :  99 (0x63) - Nr of Horizontal Characters Total.
;       R1  :  80 (0x50) - Nr of Horizontal Characters Displayed.
;       R2  :  83 (0x53) - Horizontal Sync Position.
;       R3  :   6 (0x 6) - Sync width.
;       R4  :  64 (0x40) - Vertical Total.
;       R5  :   5 (0x 5) - Vertical Total Adjustment.
;       R6  :  60 (0x3c) - Nr of Vertical Characters Displayed.
;       R7  :  61 (0x3c) - Vertical Sync Position (might need manual fine tuning).
;       R8  :   0 (0x 0) - Interlace Mode.
;       R9  :   7 (0x 7) - Max Scanline Address.
;       R10 : 197 (0xc5) - Cursor Start Scan Line.
;       R11 : 199 (0xc7) - Cursor Stop Scan Line.
;       R12 :  -1 (0xff) - Start Address (High). Real start address is 0x0000.
;       R13 :  -1 (0xff) - Start Address (Low). Real start address is 0x0000.
;       R14 :  -1 (0xff) - Cursor Start Address (High). Cursor will be at position (0, 0).
;       R15 :  -1 (0xff) - Cursor Start Address (Low). Cursor will be at position (0, 0).
;       ========================================
;
; ****************************************************************************

init6845:
                LD      HL, screenSize_LookupTable
                LD      A, (_kTerminalScreenSizeSetting)
                LD      D, $00                              ; Load index in DE
                LD      E, A
                ADD     HL, DE                              ; Add index 3 times since each entry in the table is 3 bytes
                ADD     HL, DE
                ADD     HL, DE
                JP      (HL)                                ; Jump to selected init code

; ****************************************************************************
;
;   On entry: No parameters required
;   On exit:  6845 chip set up 
;             A, BC and HL modified
init6845_80x24_16_scanlines:
                LD      HL, terminal_80x24_16_scanlines ; Load address of the table containing the settings for a 
                                                        ; 80x24, character height 16 scan lines into HL
                CALL    setUpCRT                        ; Init the 6845 CRT with the terminal_80x24_16scanlines settings
                ;
                LD      A, _kTerminalLinesConf0
                LD      (_kTerminalLines), A
                ;
                RET

; ****************************************************************************
;
;   On entry: No parameters required
;   On exit:  6845 chip set up 
;             A, BC and HL modified
init6845_80x25_16_scanlines:
                LD      HL, terminal_80x25_16_scanlines ; Load address of the table containing the settings for a 
                                                        ; 80x25, character height 16 scan lines into HL
                CALL    setUpCRT                        ; Init the 6845 CRT with the terminal_80x25_16scanlines settings
                ;
                LD      A, _kTerminalLinesConf1
                LD      (_kTerminalLines), A
                ;
                RET

; ****************************************************************************
;
;   On entry: No parameters required
;   On exit:  6845 chip set up 
;             A, BC and HL modified
init6845_80x30_16_scanlines:
                LD      HL, terminal_80x30_16_scanlines ; Load address of the table containing the settings for a 
                                                        ; 80x30, character height 16 scan lines into HL
                CALL    setUpCRT                        ; Init the 6845 CRT with the terminal_80x30_16scanlines settings
                ;
                LD      A, _kTerminalLinesConf2
                LD      (_kTerminalLines), A
                ;
                RET

; ****************************************************************************
;
;   On entry: No parameters required
;   On exit:  6845 chip set up 
;             A, BC and HL modified
init6845_80x43_8_scanlines:
                LD      HL, terminal_80x43_8_scanlines  ; Load address of the table containing the settings for a 
                                                        ; 80x43, character height 8 scan lines into HL
                CALL    setUpCRT                        ; Init the 6845 CRT with the terminal_80x43_16scanlines settings
                ;
                LD      A, _kTerminalLinesConf3
                LD      (_kTerminalLines), A
                ;
                RET

; ****************************************************************************
;
;   On entry: No parameters required
;   On exit:  6845 chip set up 
;             A, BC and HL modified
init6845_80x51_8_scanlines:
                LD      HL, terminal_80x51_8_scanlines  ; Load address of the table containing the settings for a 
                                                        ; 80x51, character height 8 scan lines into HL
                CALL    setUpCRT                        ; Init the 6845 CRT with the terminal_80x51_16scanlines settings
                ;
                LD      A, _kTerminalLinesConf4
                LD      (_kTerminalLines), A
                ;
                RET

; ****************************************************************************
;
;   On entry: No parameters required
;   On exit:  6845 chip set up 
;             A, BC and HL modified
init6845_80x60_8_scanlines:
                LD      HL, terminal_80x60_8_scanlines  ; Load address of the table containing the settings for a 
                                                        ; 80x60, character height 8 scan lines into HL
                CALL    setUpCRT                        ; Init the 6845 CRT with the terminal_80x60_16scanlines settings
                ;
                LD      A, _kTerminalLinesConf5
                LD      (_kTerminalLines), A
                ;
                RET

; ****************************************************************************
; Set Up CRT
;   On entry: HL points to table with the 16, 6845 register settings to be programmed.
;   On exit:  6845 chip set up 
;             A, BC and HL modified
setUpCRT:
                LD      B, $10                  ; B is used as counter (table contains 0x10 entries)
                LD      C, $00                  ; C is used as 6845 register address
_CRTLoop:
                LD      A, C                
                OUT     $00                     ; Store register address in the 6845 address register
                LD      A, (HL)                 ; Pick up value to be stored in the 6845 register
                OUT     $01                     ; Store value in 6845 register
                INC     C                       ; Increment 6845 register address
                INC     HL                      ; Increment table index
                DJNZ    _CRTLoop                ; All values programmed?
                DEC     HL                      ; Decrement HL to point to value of R13
                DEC     HL                      ;   To store the value of the 6845 start address registers
                DEC     HL                      ;   in global memory (not all 6845 are created equally
                LD      BC, _k6845StartAddressL ;   and allow to read the R12/R13 registers)
                LD      A, (HL)                 ; Read R13 value that was stored in R13 (Start Address (Low))
                LD      (BC), A                 ; Store R13 value at _k6845StartAddressL
                DEC     HL                      ; Let HL point to value of R12
                DEC     BC                      ; Set BC to _k6845StartAddressH
                LD      A, (HL)                 ; Read value that was stored in R13 (Start Address (High))
                LD      (BC), A                 ; Store R12 value at _k6845StartAddressL
                RET

; ****************************************************************************
update6845CursorRegisters:
                LD      HL, (_kCurrentCursor_pos)
                LD      A, 14             
                OUT     $00                     ; Store H register address in the 6845 address register
                LD      A, H
                OUT     $01 
                LD      A, 15          
                OUT     $00                     ; Store L register address in the 6845 address register
                LD      A, L
                OUT     $01 
                RET

; ****************************************************************************

update6845StartAddressRegisters:
                LD      HL, (k6845StartAddress)
                LD      A, 12             
                OUT     $00                 ; Store H register address in the 6845 address register
                LD      A, H
                OUT     $01 
                LD      A, 13          
                OUT     $00                 ; Store L register address in the 6845 address register
                LD      A, L
                OUT     $01 
                RET

; ****************************************************************************

init6845StartingAddresses:
                LD      HL, k6845StartingAddress
                LD      (k6845StartAddress), HL
                CALL    update6845StartAddressRegisters
                ;
                LD      HL, k6845CursorStartAddress
                LD      (_kCurrentCursor_pos), HL
                CALL    update6845CursorRegisters
                ;
                LD      A, 0
                LD      (_kCursorPosH), A
                LD      (_kCursorPosV), A
                ;
                RET

; ****************************************************************************

.terminal_80x24_16_scanlines     DEFB    $63, $50, $53, $06, $1F, $0D, $18, $1B, $00, $0F, $CD, $CF, $FF, $FF, $FF, $FF
.terminal_80x25_16_scanlines     DEFB    $63, $50, $53, $06, $1F, $0D, $19, $1B, $00, $0F, $CD, $CF, $FF, $FF, $FF, $FF
.terminal_80x30_16_scanlines     DEFB    $63, $50, $53, $06, $1F, $0D, $1E, $1E, $00, $0F, $CD, $CF, $FF, $FF, $FF, $FF
.terminal_80x43_8_scanlines      DEFB    $63, $50, $53, $06, $40, $05, $2B, $35, $00, $07, $C5, $C7, $FF, $FF, $FF, $FF
.terminal_80x51_8_scanlines      DEFB    $63, $50, $53, $06, $40, $05, $33, $37, $00, $07, $C5, $C7, $FF, $FF, $FF, $FF
.terminal_80x60_8_scanlines      DEFB    $63, $50, $53, $06, $40, $05, $3C, $3C, $00, $07, $C5, $C7, $FF, $FF, $FF, $FF

; ****************************************************************************

screenSize_LookupTable:
    DEFB    $C3
    DEFW    init6845_80x24_16_scanlines 
    DEFB    $C3
    DEFW    init6845_80x25_16_scanlines 
    DEFB    $C3
    DEFW    init6845_80x30_16_scanlines 
    DEFB    $C3
    DEFW    init6845_80x43_8_scanlines  
    DEFB    $C3
    DEFW    init6845_80x51_8_scanlines  
    DEFB    $C3
    DEFW    init6845_80x60_8_scanlines  

; ****************************************************************************
