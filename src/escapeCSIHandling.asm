; ****************************************************************************
; Utility code for the various escaper codes
; ****************************************************************************

    PUBLIC  handleEscapeCSISequence

    ; From globals.asm
    EXTERN  _kCSI_nr_parameters
    EXTERN  _kCSI_n1_value
    EXTERN  _kCSI_n2_value
    EXTERN  _kFontBgColor

    ; From printChar.asm
    EXTERN  fetchNextCharacter
    EXTERN  printChar

    ; From screenHandling.asm
    EXTERN  clearScreen
    EXTERN  clearScreenFromCursor
    EXTERN  clearScreenTillCursor
    EXTERN  clearFromCursorPosTillEndOfLine
    EXTERN  clearFromBeginOfLineToCursorPos
    EXTERN  clearCursorHPosition
    EXTERN  clearCursorVPosition
    EXTERN  incrementHCursor
    EXTERN  decrementHCursor
    EXTERN  incrementVCursor
    EXTERN  incrementVCursorWithScrollUp
    EXTERN  decrementVCursor
    EXTERN  decrementVCursorWithScrollDown
    EXTERN  scrollUp
    EXTERN  scrollDown
    EXTERN  insertLine
    EXTERN  deleteLine

    ; escape CSI Identifiers
    ;      <http://ascii-table.com/documents/vt100/chapter3.php#S3.3.3>
    ;      <https://vt100.net/docs/vt102-ug/chapter5.html>
    ;      <https://www.real-world-systems.com/docs/ANSIcode.html>
    ;      <https://www.gnu.org/software/screen/manual/html_node/Control-Sequences.html> (See end of this file)
    ;
    CursorUp                    EQU     'A'
    CursorDown                  EQU     'B'
    CursorForward               EQU     'C'
    CursorBack                  EQU     'D'
    CursorNextLine              EQU     'E'
    CursorPreviousLine          EQU     'F'
    CursorHorizontalAbsolute    EQU     'G'
    Separator                   EQU     ';'
    CursorPosition              EQU     'H'
    EraseInDisplay              EQU     'J'
    EraseInLine                 EQU     'K'
    InsertLine                  EQU     'L'
    DeleteLine                  EQU     'M'
    ScrollUp                    EQU     'S'
    ScrollDown                  EQU     'T'
    HorizontalVerticalPosition  EQU     'f'
    SelectGraphicRendition      EQU     'm'

; ****************************************************************************

SECTION code_user

; ****************************************************************************

handleEscapeCSISequence:
                CP      A, $3A                              ; $3A - ';' first character after '9'
                JR      NC, handleEscapeCSISequence_1       ; Not a number? Then process character as a CSI
                ;
                LD      HL, _kCSI_n1_value                  ; Store address of n1 in HL
                CALL    handleDigit                         ; Store numeric value in n1 and get next character already in A
                JR      handleEscapeCSISequence             ; Process next character which can be again a number or the actual CSI
                ;
handleEscapeCSISequence_1:
                SUB     A, $3B                              ; ';' is the beginning of the lookup table (i.e. index 0)
                LD      HL, CSI_EscapeSequence_LookupTable  ; Load start of lookup table into HL
                LD      D, $00                              ; Load index in DE
                LD      E, A
                ADD     HL, DE                              ; Add index 3 times since each entry in the table is 3 bytes
                ADD     HL, DE
                ADD     HL, DE
                JP      (HL)                                ; Jump to selected function

; ****************************************************************************

handleCursorUp:
                ; CUU -- Cursor Up -- Host to VT100 and VT100 to Host
                ; ESC [ Pn A	default value: 1

                ; Moves the active position upward without altering the column position.
                ; The number of lines moved is determined by the parameter. A parameter
                ; value of zero or one moves the active position one line upward. A
                ; parameter value of n moves the active position n lines upward. If an
                ; attempt is made to move the cursor above the top margin, the cursor
                ; stops at the top margin. Editor Function

                CALL    normalize_n1
handleCursorUpLoop:
                CALL    decrementVCursor
                LD      HL, _kCSI_n1_value                  ; Get current value of the loop counter (always at least 1)
                DEC     (HL)                                ; Decrement loop counter
                LD      A, (HL)
                CP      A, $00                              ; Is loop counter 0?
                RET     Z                                   ; If YES then return
                JR      handleCursorUpLoop                  ; If NO then do another loop

; ****************************************************************************

handleCursorDown:
                ;  CUD -- Cursor Down -- Host to VT100 and VT100 to Host
                ;  ESC [ Pn B	default value: 1

                ;  The CUD sequence moves the active position downward without altering the
                ;  column position. The number of lines moved is determined by the parameter.
                ;  If the parameter value is zero or one, the active position is moved one
                ;  line downward. If the parameter value is n, the active position is moved n
                ;  lines downward. In an attempt is made to move the cursor below the bottom
                ;  margin, the cursor stops at the bottom margin. Editor Function

                CALL    normalize_n1
handleCursorDownLoop:
                CALL    incrementVCursor
                LD      HL, _kCSI_n1_value                  ; Get current value of the loop counter (always at least 1)
                DEC     (HL)                                ; Decrement loop counter
                LD      A, (HL)
                CP      A, $00                              ; Is loop counter 0?
                RET     Z                                   ; If YES then return
                JR      handleCursorDownLoop                ; If NO then do another loop

; ****************************************************************************

handleCursorForward:
                ; CUF -- Cursor Forward -- Host to VT100 and VT100 to Host
                ; ESC [ Pn C	default value: 1

                ; The CUF sequence moves the active position to the right. The distance moved
                ; is determined by the parameter. A parameter value of zero or one moves the
                ; active position one position to the right. A parameter value of n moves the
                ; active position n positions to the right. If an attempt is made to move the
                ; cursor to the right of the right margin, the cursor stops at the right
                ; margin. Editor Function

                CALL    normalize_n1
handleCursorForwardLoop:
                CALL    incrementHCursor
                LD      HL, _kCSI_n1_value                  ; Get current value of the loop counter (always at least 1)
                DEC     (HL)                                ; Decrement loop counter
                LD      A, (HL)
                CP      A, $00                              ; Is loop counter 0?
                RET     Z                                   ; If YES then return
                JR      handleCursorForwardLoop             ; If NO then do another loop

; ****************************************************************************

handleCursorBack:
                ; CUB -- Cursor Backward -- Host to VT100 and VT100 to Host
                ; ESC [ Pn D	default value: 1

                ; The CUB sequence moves the active position to the left. The distance moved
                ; is determined by the parameter. If the parameter value is zero or one, the
                ; active position is moved one position to the left. If the parameter value is n,
                ; the active position is moved n positions to the left. If an attempt is made to
                ; move the cursor to the left of the left margin, the cursor stops at the left
                ; margin. Editor Function

                CALL    normalize_n1
handleCursorBackLoop:
                CALL    decrementHCursor
                LD      HL, _kCSI_n1_value                  ; Get current value of the loop counter (always at least 1)
                DEC     (HL)                                ; Decrement loop counter
                LD      A, (HL)
                CP      A, $00                              ; Is loop counter 0?
                RET     Z                                   ; If YES then return
                JR      handleCursorBackLoop                ; If NO then do another loop

; ****************************************************************************

handleCursorNextLine:
                ; CNL -- Next Line
                ; ESC [ Pn E

                ; This sequence causes the active position to move to the first position on the
                ; next line downward. If the active position is at the bottom margin, a scroll
                ; up is performed. Format Effector

                CALL    normalize_n1
                CALL    clearCursorHPosition
handleCursorNextLineLoop:
                CALL    incrementVCursorWithScrollUp
                LD      HL, _kCSI_n1_value                  ; Get current value of the loop counter (always at least 1)
                DEC     (HL)                                ; Decrement loop counter
                LD      A, (HL)
                CP      A, $00                              ; Is loop counter 0?
                RET     Z                                   ; If YES then return
                JR      handleCursorNextLineLoop            ; If NO then do another loop

; ****************************************************************************

handleCursorPreviousLine:
                ; CPL -- Previous Line
                ; ESC [ Pn F

                ; This sequence causes the active position to move to the first position on the
                ; previous line upward. If the active position is at the top margin, a scroll
                ; down is performed. Format Effector

                CALL    normalize_n1
                CALL    clearCursorHPosition
handleCursorPreviousLineLoop:
                CALL    decrementVCursorWithScrollDown
                LD      HL, _kCSI_n1_value                  ; Get current value of the loop counter (always at least 1)
                DEC     (HL)                                ; Decrement loop counter
                LD      A, (HL)
                CP      A, $00                              ; Is loop counter 0?
                RET     Z                                   ; If YES then return
                JR      handleCursorPreviousLine            ; If NO then do another loop

; ****************************************************************************

handleCursorHorizontalAbsolute:
                ; CHA - Cursor Horizontal Absolute
                ; ESC [ Pn G

                ; Moves the cursor to column n (default 1).

                CALL    clearCursorHPosition
                CALL    normalize_n1
                LD      HL, _kCSI_n1_value                  ; Get n value
                DEC     (HL)                                ; Decrement because we count 0..79 instead of 1..80
                ;
handleCursorHorizontalAbsoluteLoop:
                ; Increments the H cursor position the lazy way by using a for loop.
                LD      HL, _kCSI_n1_value                  ; Get the wanted H position
                LD      A, (HL)                             ; Check if 0
                CP      A, $00                              ; Is loop counter 0?
                JR      Z, handleCursorHorizontalAbsoluteExit
                DEC     (HL)                                ; Decrement loop counter
                CALL    incrementHCursor                    ; Increment H cursor position
                JR      handleCursorHorizontalAbsoluteLoop
                ;
handleCursorHorizontalAbsoluteExit:
                RET

; ****************************************************************************

handleCursorPosition:
                ; CUP -- Cursor Position
                ; ESC [ Pn ; Pn H	default value: 1

                ; The CUP sequence moves the active position to the position specified by the
                ; parameters. This sequence has two parameter values, the first specifying the
                ; line position and the second specifying the column position. A parameter value
                ; of zero or one for the first or second parameter moves the active position to
                ; the first line or column in the display, respectively. The default condition
                ; with no parameters present is equivalent to a cursor to home action. In the
                ; VT100, this control behaves identically with its format effector counterpart,
                ; HVP. Editor Function

                ; The numbering of lines depends on the state of the Origin Mode (DECOM).

                CALL    clearCursorVPosition
                CALL    normalize_n1
                LD      HL, _kCSI_n1_value                  ; Get n value
                DEC     (HL)                                ; Decrement because we count 0..24 instead of 1..25
handleCursorPositionVLoop:
                ; Increments the V cursor position the lazy way by using a for loop.
                LD      HL, _kCSI_n1_value                  ; Get the wanted V position
                LD      A, (HL)                             ; Check if 0
                CP      A, $00                              ; Is loop counter 0?
                JR      Z, handleCursorHPosition            ; If so, nothing to do anymore since we're already at position 0.
                DEC     (HL)                                ; Decrement loop counter
                CALL    incrementVCursor                    ; Increment V cursor position
                JR      handleCursorPositionVLoop
                ;
handleCursorHPosition:
                CALL    clearCursorHPosition
                CALL    normalize_n2
                LD      HL, _kCSI_n2_value                  ; Get n value
                DEC     (HL)                                ; Decrement because we count 0..79 instead of 1..80
                ;
handleCursorPositionHLoop:
                ; Increments the H cursor position the lazy way by using a for loop.
                LD      HL, _kCSI_n2_value                  ; Get the wanted H position
                LD      A, (HL)                             ; Check if 0
                CP      A, $00                              ; Is loop counter 0?
                JR      Z, handleCursorPositionExit
                DEC     (HL)                                ; Decrement loop counter
                CALL    incrementHCursor                    ; Increment H cursor position
                JR      handleCursorPositionHLoop
                ;
handleCursorPositionExit:
                RET

; ****************************************************************************

handleEraseInDisplay:
                ; ED -- Erase In Display
                ; ESC [ Ps J	default value: 0

                ; This sequence erases some or all of the characters in the display according to
                ; the parameter. Any complete line erased by this sequence will return that line
                ; to single width mode. Editor Function

                ; Parameter	Parameter Meaning
                ; 0	Erase from the active position to the end of the screen, inclusive (default)
                ; 1	Erase from start of the screen to the active position, inclusive
                ; 2	Erase all of the display -- all lines are erased, changed to single-width,
                ;      and the cursor does not move.

                LD      A, (_kCSI_n1_value)                 ; Get n value in A
                CP      A, $00
                JR      Z, handleEraseInDisplay_0           ; 0	Erase from the active position to the end of the screen, inclusive (default)
                ;
                CP      A, $01
                JR      Z, handleEraseInDisplay_1           ; 1	Erase from start of the screen to the active position, inclusive
                ;
                CP      A, $02
                JR      Z, handleEraseInDisplay_2           ; 2	Erase all of the display -- all lines are erased, and the cursor does not move.
                ;
                RET

handleEraseInDisplay_0:
                CALL    clearScreenFromCursor
                RET

handleEraseInDisplay_1:
                CALL    clearScreenTillCursor
                RET

handleEraseInDisplay_2:
                CALL    clearScreen
                RET

; ****************************************************************************

handleEraseInLine:
                ; EL -- Erase In Line
                ; ESC [ Ps K	default value: 0

                ; Erases some or all characters in the active line according to the parameter.
                ; Editor Function

                ; Parameter	Parameter Meaning
                ; 0	Erase from the active position to the end of the line, inclusive (default)
                ; 1	Erase from the start of the screen to the active position, inclusive
                ; 2	Erase all of the line, inclusive

                LD      A, (_kCSI_n1_value)                 ; Get n value in A
                CP      A, $00
                JR      Z, handleEraseInLine_0              ; 0	Erase from the active position to the end of the line, inclusive (default)
                ;
                CP      A, $01
                JR      Z, handleEraseInLine_1              ; 1	Erase from the start of the screen to the active position, inclusive
                ;
                CP      A, $02
                JR      Z, handleEraseInLine_2              ; 2	Erase all of the line, inclusive
                ;
                RET

handleEraseInLine_0:
                CALL    clearFromCursorPosTillEndOfLine
                RET

handleEraseInLine_1:
                CALL    clearFromBeginOfLineToCursorPos
                RET

handleEraseInLine_2:
                CALL    clearFromBeginOfLineToCursorPos
                CALL    clearFromCursorPosTillEndOfLine
                RET


; ****************************************************************************

handleInsertLine:
                ; IL -- Insert Line
                ; ESC [ Pn L

                ; Inserts Pn lines at line with cursor. Lines displayed below cursor move down.
                ; Lines moved past the bottom margin are lost. This sequence is ignored when
                ; cursor is outside scrolling region.

                CALL    normalize_n1

handleInsertLineLoop:
                CALL    insertLine
                LD      HL, _kCSI_n1_value                  ; Get current value of the loop counter (always at least 1)
                DEC     (HL)                                ; Decrement loop counter
                LD      A, (HL)
                CP      A, $00                              ; Is loop counter 0?
                RET     Z                                   ; If YES then return
                JR      handleInsertLineLoop                ; If NO then do another loop

; ****************************************************************************

handleDeleteLine:
                ; DL -- Delete Line (DL)
                ; ESC [ Pn  M

                ; Deletes Pn lines, starting at line with cursor. As lines are deleted, lines
                ; displayed below cursor move up. Lines added to bottom of screen have spaces
                ; with same character attributes as last line moved up. This sequence is ignored
                ; when cursor is outside scrolling region.

                CALL    normalize_n1

handleDeleteLineLoop:
                CALL    deleteLine
                LD      HL, _kCSI_n1_value                  ; Get current value of the loop counter (always at least 1)
                DEC     (HL)                                ; Decrement loop counter
                LD      A, (HL)
                CP      A, $00                              ; Is loop counter 0?
                RET     Z                                   ; If YES then return
                JR      handleDeleteLineLoop                ; If NO then do another loop

; ****************************************************************************

handleScrollUp:
                ; SU - Scroll Up
                ; ESC [ Pn S

                ; Scroll whole page up by n (default 1) lines. New lines are added at the bottom.

                CALL    normalize_n1

handleScrollUpLoop:
                CALL    scrollUp
                LD      HL, _kCSI_n1_value                  ; Get current value of the loop counter (always at least 1)
                DEC     (HL)                                ; Decrement loop counter
                LD      A, (HL)
                CP      A, $00                              ; Is loop counter 0?
                RET     Z                                   ; If YES then return
                JR      handleScrollUpLoop                  ; If NO then do another loop

; ****************************************************************************

handleScrollDown:
                ; SD - Scroll Down
                ; ESC [ Pn T

                ; Scroll whole page down by n (default 1) lines. New lines are added at the top.

                CALL    normalize_n1

handleScrollDownLoop:
                CALL    scrollDown
                LD      HL, _kCSI_n1_value                  ; Get current value of the loop counter (always at least 1)
                DEC     (HL)                                ; Decrement loop counter
                LD      A, (HL)
                CP      A, $00                              ; Is loop counter 0?
                RET     Z                                   ; If YES then return
                JR      handleScrollDownLoop                ; If NO then do another loop

; ****************************************************************************

handleHorizontalVerticalPosition:
                ; HVP -- Horizontal and Vertical Position
                ; ESC [ Pn ; Pn f	default value: 1

                ; Moves the active position to the position specified by the parameters. This
                ; sequence has two parameter values, the first specifying the line position and
                ; the second specifying the column. A parameter value of either zero or one
                ; causes the active position to move to the first line or column in the display,
                ; respectively. The default condition with no parameters present moves the active
                ; position to the home position. In the VT100, this control behaves identically
                ; with its editor function counterpart, CUP. The numbering of lines and columns
                ; depends on the reset or set state of the origin mode (DECOM). Format Effector

                CALL    clearCursorVPosition
                CALL    normalize_n1
                LD      HL, _kCSI_n1_value                  ; Get n value
                DEC     (HL)                                ; Decrement because we count 0..24 instead of 1..25
handleHorizontalVerticalVPositionLoop:
                ; Increments the V cursor position the lazy way by using a for loop.
                LD      HL, _kCSI_n1_value                  ; Get the wanted V position
                LD      A, (HL)                             ; Check if 0
                CP      A, $00                              ; Is loop counter 0?
                JR      Z, handleHorizontalVerticalHPosition; If so, nothing to do anymore since we're already at position 0.
                DEC     (HL)                                ; Decrement loop counter
                CALL    incrementVCursor                    ; Increment V cursor position
                JR      handleHorizontalVerticalVPositionLoop
                ;
handleHorizontalVerticalHPosition:
                CALL    clearCursorHPosition
                CALL    normalize_n2
                LD      HL, _kCSI_n2_value                  ; Get n value
                DEC     (HL)                                ; Decrement because we count 0..79 instead of 1..80
                ;
handleHorizontalVerticalHPositionLoop:
                ; Increments the H cursor position the lazy way by using a for loop.
                LD      HL, _kCSI_n2_value                  ; Get the wanted H position
                LD      A, (HL)                             ; Check if 0
                CP      A, $00                              ; Is loop counter 0?
                JR      Z, handleHorizontalVerticalExit
                DEC     (HL)                                ; Decrement loop counter
                CALL    incrementHCursor                    ; Increment H cursor position
                JR      handleHorizontalVerticalHPositionLoop
                ;
handleHorizontalVerticalExit:
                RET

; ****************************************************************************

handleSelectGraphicRendition:
                ; SGR -- Select Graphic Rendition
                ; ESC [ Ps ; . . . ; Ps m	default value: 0

                ; Invoke the graphic rendition specified by the parameter(s). All following
                ; characters transmitted to the VT100 are rendered according to the parameter(s)
                ; until the next occurrence of SGR. Format Effector

                ; Parameter	Parameter Meaning
                ; 0	Attributes off
                ; 1	Bold or increased intensity
                ; 4	Underscore
                ; 5	Blink
                ; 7	Negative (reverse) image
                ; All other parameter values are ignored.

                ; With the Advanced Video Option, only one type of character attribute is possible
                ; as determined by the cursor selection; in that case specifying either the
                ; underscore or the reverse attribute will activate the currently selected attribute.

                ; From <https://www.lihaoyi.com/post/BuildyourownCommandLinewithANSIescapecodes.html#8-colors>
                ;   Black:              \u001b[30m  |   Bright Black:               \u001b[30;1m
                ;   Red:                \u001b[31m  |   Bright Red:                 \u001b[31;1m
                ;   Green:              \u001b[32m  |   Bright Green:               \u001b[32;1m
                ;   Yellow:             \u001b[33m  |   Bright Yellow:              \u001b[33;1m
                ;   Blue:               \u001b[34m  |   Bright Blue:                \u001b[34;1m
                ;   Magenta:            \u001b[35m  |   Bright Magenta:             \u001b[35;1m
                ;   Cyan:               \u001b[36m  |   Bright Cyan:                \u001b[36;1m
                ;   White:              \u001b[37m  |   Bright White:               \u001b[37;1m
                ;
                ;   Background Black:   \u001b[40m  |   Background Bright Black:    \u001b[40;1m
                ;   Background Red:     \u001b[41m  |   Background Bright Red:      \u001b[41;1m
                ;   Background Green:   \u001b[42m  |   Background Bright Green:    \u001b[42;1m
                ;   Background Yellow:  \u001b[43m  |   Background Bright Yellow:   \u001b[43;1m
                ;   Background Blue:    \u001b[44m  |   Background Bright Blue:     \u001b[44;1m
                ;   Background Magenta: \u001b[45m  |   Background Bright Magenta:  \u001b[45;1m
                ;   Background Cyan:    \u001b[46m  |   Background Bright Cyan:     \u001b[46;1m
                ;   Background White:   \u001b[47m  |   Background Bright White:    \u001b[47;1m
                ;
                LD      A, (_kCSI_n1_value)                 ; Get n value in A
                ;
                CP      A, $00
                JP      Z, handleSelectGraphicRendition_0   ; 0	Intensity Off
                ;
                CP      A, $01
                JP      Z, handleSelectGraphicRendition_1   ; 1	Intensity On
                ;
                CP      A, 30
                JP      Z, handleSelectGraphicRendition_FgBlack
                ;
                CP      A, 31
                JP      Z, handleSelectGraphicRendition_FgRed
                ;
                CP      A, 32
                JP      Z, handleSelectGraphicRendition_FgGreen
                ;
                CP      A, 33
                JP      Z, handleSelectGraphicRendition_FgYellow
                ;
                CP      A, 34
                JP      Z, handleSelectGraphicRendition_FgBlue
                ;
                CP      A, 35
                JP      Z, handleSelectGraphicRendition_FgMagenta
                ;
                CP      A, 36
                JP      Z, handleSelectGraphicRendition_FgCyan
                ;
                CP      A, 37
                JP      Z, handleSelectGraphicRendition_FgWhite
                ;
                CP      A, 40
                JP      Z, handleSelectGraphicRendition_BgBlack
                ;
                CP      A, 41
                JP      Z, handleSelectGraphicRendition_BgRed
                ;
                CP      A, 42
                JP      Z, handleSelectGraphicRendition_BgGreen
                ;
                CP      A, 43
                JP      Z, handleSelectGraphicRendition_BgYellow
                ;
                CP      A, 44
                JP      Z, handleSelectGraphicRendition_BgBlue
                ;
                CP      A, 45
                JP      Z, handleSelectGraphicRendition_BgMagenta
                ;
                CP      A, 46
                JP      Z, handleSelectGraphicRendition_BgCyan
                ;
                CP      A, 47
                JP      Z, handleSelectGraphicRendition_BgWhite
                ;
                RET

                ; Intensity control
handleSelectGraphicRendition_0:
                LD      A, (_kFontBgColor)
                AND     A, $77
                LD      (_kFontBgColor), A
                JP      handleSelectGraphicRenditionIntensity

handleSelectGraphicRendition_1:
                LD      A, (_kFontBgColor)
                OR      A, $08
                LD      (_kFontBgColor), A
                JP      handleSelectGraphicRenditionIntensity

                ; Foreground colors
handleSelectGraphicRendition_FgBlack:
                LD      A, (_kFontBgColor)
                AND     A, $F8
                LD      (_kFontBgColor), A
                JP      handleSelectGraphicRenditionIntensity

handleSelectGraphicRendition_FgRed:
                LD      A, (_kFontBgColor)
                AND     A, $F8
                OR      A, $04
                LD      (_kFontBgColor), A
                JP      handleSelectGraphicRenditionIntensity

handleSelectGraphicRendition_FgGreen:
                LD      A, (_kFontBgColor)
                AND     A, $F8
                OR      A, $02
                LD      (_kFontBgColor), A
                JP      handleSelectGraphicRenditionIntensity

handleSelectGraphicRendition_FgYellow:
                LD      A, (_kFontBgColor)
                AND     A, $F8
                OR      A, $06
                LD      (_kFontBgColor), A
                JP      handleSelectGraphicRenditionIntensity

handleSelectGraphicRendition_FgBlue:
                LD      A, (_kFontBgColor)
                AND     A, $F8
                OR      A, $01
                LD      (_kFontBgColor), A
                JP      handleSelectGraphicRenditionIntensity

handleSelectGraphicRendition_FgMagenta:
                LD      A, (_kFontBgColor)
                AND     A, $F8
                OR      A, $05
                LD      (_kFontBgColor), A
                JP      handleSelectGraphicRenditionIntensity

handleSelectGraphicRendition_FgCyan:
                LD      A, (_kFontBgColor)
                AND     A, $F8
                OR      A, $03
                LD      (_kFontBgColor), A
                JP      handleSelectGraphicRenditionIntensity

handleSelectGraphicRendition_FgWhite:
                LD      A, (_kFontBgColor)
                AND     A, $F8
                OR      A, $07
                LD      (_kFontBgColor), A
                JP      handleSelectGraphicRenditionIntensity

                ; Background colors
handleSelectGraphicRendition_BgBlack:
                LD      A, (_kFontBgColor)
                AND     A, $8F
                LD      (_kFontBgColor), A
                JP      handleSelectGraphicRenditionIntensity

handleSelectGraphicRendition_BgRed:
                LD      A, (_kFontBgColor)
                AND     A, $8F
                OR      A, $40
                LD      (_kFontBgColor), A
                JP      handleSelectGraphicRenditionIntensity

handleSelectGraphicRendition_BgGreen:
                LD      A, (_kFontBgColor)
                AND     A, $8F
                OR      A, $20
                LD      (_kFontBgColor), A
                JP      handleSelectGraphicRenditionIntensity

handleSelectGraphicRendition_BgYellow:
                LD      A, (_kFontBgColor)
                AND     A, $8F
                OR      A, $60
                LD      (_kFontBgColor), A
                JP      handleSelectGraphicRenditionIntensity

handleSelectGraphicRendition_BgBlue:
                LD      A, (_kFontBgColor)
                AND     A, $8F
                OR      A, $10
                LD      (_kFontBgColor), A
                JP      handleSelectGraphicRenditionIntensity

handleSelectGraphicRendition_BgMagenta:
                LD      A, (_kFontBgColor)
                AND     A, $8F
                OR      A, $50
                LD      (_kFontBgColor), A
                JP      handleSelectGraphicRenditionIntensity

handleSelectGraphicRendition_BgCyan:
                LD      A, (_kFontBgColor)
                AND     A, $8F
                OR      A, $30
                LD      (_kFontBgColor), A
                JP      handleSelectGraphicRenditionIntensity

handleSelectGraphicRendition_BgWhite:
                LD      A, (_kFontBgColor)
                AND     A, $8F
                OR      A, $70
                LD      (_kFontBgColor), A
                JP      handleSelectGraphicRenditionIntensity
;
; ---
;
; We arrive here when a second parameter (intensity) was added to the escape sequence.
; After handling the intensity, the actual color parameter is processed.
;
handleSelectGraphicRenditionIntensity:
                ;
                LD      A, (_kCSI_nr_parameters)            ; Is there a second numeric value?
                CP      $01
                RET     Z
                ;
                LD      A, (_kCSI_n2_value)                 ; Get second n value in A
                ;
                CP      A, $00
                JP      Z, handleSelectGraphicRenditionIntensity_0
                ;
                CP      A, $01
                JP      Z, handleSelectGraphicRenditionIntensity_1
                ;
                JP      handleSelectGraphicRendition

handleSelectGraphicRenditionIntensity_0:
                LD      A, (_kCSI_n1_value)
                CP      A, 38
                JP      C, handleSelectGraphicRenditionIntensity_0_Fg
handleSelectGraphicRenditionIntensity_0_Bg:
                LD      A, (_kFontBgColor)
                AND     A, $7F
                LD      (_kFontBgColor), A
                RET
handleSelectGraphicRenditionIntensity_0_Fg:
                LD      A, (_kFontBgColor)
                AND     A, $F7
                LD      (_kFontBgColor), A
                RET
                ;
handleSelectGraphicRenditionIntensity_1:
                LD      A, (_kCSI_n1_value)
                CP      A, 38
                JP      C, handleSelectGraphicRenditionIntensity_1_Fg
handleSelectGraphicRenditionIntensity_1_Bg:
                LD      A, (_kFontBgColor)
                OR      A, $80
                LD      (_kFontBgColor), A
                RET
handleSelectGraphicRenditionIntensity_1_Fg:
                LD      A, (_kFontBgColor)
                OR      A, $08
                LD      (_kFontBgColor), A
                RET

; ****************************************************************************

handleSeparator:
                CALL    fetchNextCharacter                  ; Get the next character to be processed
                CP      A, $3A                              ; $3A - ';' first character after '9'
                JR      NC, handleSeparatorExit             ; Is next character a number? Then process next digit
handleSeparatorLoop:
                LD      HL, _kCSI_n2_value                  ; Store address of n1 in HL
                CALL    handleDigit                         ; Store numeric value in n1 and get next character
                CP      A, $3A                              ; $3A - ';' first character after '9'
                JR      C, handleSeparatorLoop              ; Is next character a number? Then process next digit
                ;
                LD      HL, _kCSI_nr_parameters
                INC     (HL)                                ; Indicate that we have 2 numeric values to handle
                ;
handleSeparatorExit:
                ; Now check if we have a CSI command after the separator and that utilises 2 values
                CP      CursorPosition
                JP      Z, handleCursorPosition
                ;
                CP      HorizontalVerticalPosition
                JP      Z, handleHorizontalVerticalPosition
                ;
                CP      SelectGraphicRendition
                JP      Z, handleSelectGraphicRendition
                ;
                CALL    printChar
                RET

; ****************************************************************************

handleNotImplemented:
                RET

; ****************************************************************************

handleDigit:
                ; Trick used to multiply by 10:
                ;   a = 10 * b
                ;   a = (8 + 2) * b
                ;   a = (8 * b) + (2 * b)
                PUSH    AF                                  ; Remember input value in A for a while
                ;
                LD      A, (HL)                             ; Store original value in A
                SLA     A                                   ; Multiply A by 2
                SLA     (HL)                                ; Multiply original value by 8 at storage location
                SLA     (HL)                                ; Shifting to the left
                SLA     (HL)
                ADD     A, (HL)                             ; Add A with storage value and we get the value multiplied by 10 in A
                LD      (HL), A                             ; Store calculated value
                ;
                POP     AF                                  ; Restore original input value in A
                SUB     A, $30                              ; Convert ASCII to decimal value
                ADD     A, (HL)                             ; Add with any previous value
                LD      (HL), A                             ; Store result
                ;
                CALL    fetchNextCharacter                  ; Get already the next character to be processed by handleEscapeCSISequence
                                                            ; which can be a number or the CSI command.
                RET

; ****************************************************************************

normalize_n1:
                LD      HL, _kCSI_n1_value                  ; Store address of n1 in HL
                LD      A, (HL)                             ; Load n1 value in A
                CP      A, $00                              ; Compare with 0
                RET     NZ                                  ; If not 0 then return
                ;
                INC     (HL)                                ; Increment n1 to 1
                RET

; ****************************************************************************

normalize_n2:
                LD      HL, _kCSI_n2_value                  ; Store address of n2 in HL
                LD      A, (HL)                             ; Load n2 value in A
                CP      A, $00                              ; Compare with 0
                RET     NZ                                  ; If not 0 then return
                ;
                INC     (HL)                                ; Increment n2 to 1
                RET

; ****************************************************************************

CSI_EscapeSequence_LookupTable:
    DEFB    $C3
    DEFW    handleSeparator                     ; ; - Separator
    DEFB    $C3
    DEFW    handleNotImplemented                ; < - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; = - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; > - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; ? - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; @ - Not Implemented
    DEFB    $C3
    DEFW    handleCursorUp                      ; A - CursorUp
    DEFB    $C3
    DEFW    handleCursorDown                    ; B - CursorDown
    DEFB    $C3
    DEFW    handleCursorForward                 ; C - CursorForward
    DEFB    $C3
    DEFW    handleCursorBack                    ; D - CursorBack
    DEFB    $C3
    DEFW    handleCursorNextLine                ; E - CursorNextLine
    DEFB    $C3
    DEFW    handleCursorPreviousLine            ; F - CursorPreviousLine
    DEFB    $C3
    DEFW    handleCursorHorizontalAbsolute      ; G - CursorHorizontalAbsolute
    DEFB    $C3
    DEFW    handleCursorPosition                ; H - CursorPosition
    DEFB    $C3
    DEFW    handleNotImplemented                ; I - Not Implemented
    DEFB    $C3
    DEFW    handleEraseInDisplay                ; J - EraseInDisplay
    DEFB    $C3
    DEFW    handleEraseInLine                   ; K - EraseInLine
    DEFB    $C3
    DEFW    handleInsertLine                    ; L - InsertLine
    DEFB    $C3
    DEFW    handleDeleteLine                    ; M - DeleteLine
    DEFB    $C3
    DEFW    handleNotImplemented                ; N - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; O - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; P - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; Q - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; R - Not Implemented
    DEFB    $C3
    DEFW    handleScrollUp                      ; S - ScrollUp
    DEFB    $C3
    DEFW    handleScrollDown                    ; T - ScrollDown
    DEFB    $C3
    DEFW    handleNotImplemented                ; U - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; V - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; W - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; X - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; Y - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; Z - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; [ - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; \ - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; ] - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; ^ - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; _ - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; ` - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; a - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; b - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; c - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; d - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; e - Not Implemented
    DEFB    $C3
    DEFW    handleHorizontalVerticalPosition    ; f - HorizontalVerticalPosition
    DEFB    $C3
    DEFW    handleNotImplemented                ; g - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; h - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; i - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; j - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; k - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; l - Not Implemented
    DEFB    $C3
    DEFW    handleSelectGraphicRendition        ; m - SelectGraphicRendition
    DEFB    $C3
    DEFW    handleNotImplemented                ; n - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; o - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; p - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; q - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; r - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; s - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; t - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; u - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; v - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; w - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; x - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; y - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; z - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; { - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; | - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; } - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; ~ - Not Implemented
    DEFB    $C3
    DEFW    handleNotImplemented                ; DEL - Not Implemented

; ****************************************************************************

; 11.1 Control Sequences:

; The following is a list of control sequences recognized by screen. ‘(V)’ and ‘(A)’ 
; indicate VT100-specific and ANSI- or ISO-specific functions, respectively.

;    ESC E                           Next Line
;    ESC D                           Index
;    ESC M                           Reverse Index
;    ESC H                           Horizontal Tab Set
;    ESC Z                           Send VT100 Identification String
;    ESC 7                   (V)     Save Cursor and Attributes
;    ESC 8                   (V)     Restore Cursor and Attributes
;    ESC [s                  (A)     Save Cursor and Attributes
;    ESC [u                  (A)     Restore Cursor and Attributes
;    ESC c                           Reset to Initial State
;    ESC g                           Visual Bell
;    ESC Pn p                        Cursor Visibility (97801)
;        Pn = 6                      Invisible
;             7                      Visible
;    ESC =                   (V)     Application Keypad Mode
;    ESC >                   (V)     Numeric Keypad Mode
;    ESC # 8                 (V)     Fill Screen with E's
;    ESC \                   (A)     String Terminator
;    ESC ^                   (A)     Privacy Message String (Message Line)
;    ESC !                           Global Message String (Message Line)
;    ESC k                           Title Definition String
;    ESC P                   (A)     Device Control String
;                                    Outputs a string directly to the host
;                                    terminal without interpretation.
;    ESC _                   (A)     Application Program Command (Hardstatus)
;    ESC ] 0 ; string ^G     (A)     Operating System Command (Hardstatus, xterm
;                                    title hack)
;    ESC ] 83 ; cmd ^G       (A)     Execute screen command. This only works if
;                                    multi-user support is compiled into screen.
;                                    The pseudo-user ":window:" is used to check
;                                    the access control list. Use "addacl :window:
;                                    -rwx #?" to create a user with no rights and
;                                    allow only the needed commands.
;    Control-N               (A)     Lock Shift G1 (SO)
;    Control-O               (A)     Lock Shift G0 (SI)
;    ESC n                   (A)     Lock Shift G2
;    ESC o                   (A)     Lock Shift G3
;    ESC N                   (A)     Single Shift G2
;    ESC O                   (A)     Single Shift G3
;    ESC ( Pcs               (A)     Designate character set as G0
;    ESC ) Pcs               (A)     Designate character set as G1
;    ESC * Pcs               (A)     Designate character set as G2
;    ESC + Pcs               (A)     Designate character set as G3
;    ESC [ Pn ; Pn H                 Direct Cursor Addressing
;    ESC [ Pn ; Pn f                 same as above
;    ESC [ Pn J                      Erase in Display
;          Pn = None or 0            From Cursor to End of Screen
;               1                    From Beginning of Screen to Cursor
;               2                    Entire Screen
;    ESC [ Pn K                      Erase in Line
;          Pn = None or 0            From Cursor to End of Line
;               1                    From Beginning of Line to Cursor
;               2                    Entire Line
;    ESC [ Pn X                      Erase character
;    ESC [ Pn A                      Cursor Up
;    ESC [ Pn B                      Cursor Down
;    ESC [ Pn C                      Cursor Right
;    ESC [ Pn D                      Cursor Left
;    ESC [ Pn E                      Cursor next line
;    ESC [ Pn F                      Cursor previous line
;    ESC [ Pn G                      Cursor horizontal position
;    ESC [ Pn `                      same as above
;    ESC [ Pn d                      Cursor vertical position
;    ESC [ Ps ;...; Ps m             Select Graphic Rendition
;          Ps = None or 0            Default Rendition
;               1                    Bold
;               2            (A)     Faint
;               3            (A)     Standout Mode (ANSI: Italicized)
;               4                    Underlined
;               5                    Blinking
;               7                    Negative Image
;               22           (A)     Normal Intensity
;               23           (A)     Standout Mode off (ANSI: Italicized off)
;               24           (A)     Not Underlined
;               25           (A)     Not Blinking
;               27           (A)     Positive Image
;               30           (A)     Foreground Black
;               31           (A)     Foreground Red
;               32           (A)     Foreground Green
;               33           (A)     Foreground Yellow
;               34           (A)     Foreground Blue
;               35           (A)     Foreground Magenta
;               36           (A)     Foreground Cyan
;               37           (A)     Foreground White
;               39           (A)     Foreground Default
;               40           (A)     Background Black
;               ...                  ...
;               49           (A)     Background Default
;    ESC [ Pn g                      Tab Clear
;          Pn = None or 0            Clear Tab at Current Position
;               3                    Clear All Tabs
;    ESC [ Pn ; Pn r         (V)     Set Scrolling Region
;    ESC [ Pn I              (A)     Horizontal Tab
;    ESC [ Pn Z              (A)     Backward Tab
;    ESC [ Pn L              (A)     Insert Line
;    ESC [ Pn M              (A)     Delete Line
;    ESC [ Pn @              (A)     Insert Character
;    ESC [ Pn P              (A)     Delete Character
;    ESC [ Pn S                      Scroll Scrolling Region Up
;    ESC [ Pn T                      Scroll Scrolling Region Down
;    ESC [ Pn ^                      same as above
;    ESC [ Ps ;...; Ps h             Set Mode
;    ESC [ Ps ;...; Ps l             Reset Mode
;          Ps = 4            (A)     Insert Mode
;               20           (A)     ‘Automatic Linefeed’ Mode.
;               34                   Normal Cursor Visibility
;               ?1           (V)     Application Cursor Keys
;               ?3           (V)     Change Terminal Width to 132 columns
;               ?5           (V)     Reverse Video
;               ?6           (V)     ‘Origin’ Mode
;               ?7           (V)     ‘Wrap’ Mode
;               ?9                   X10 mouse tracking
;               ?25          (V)     Visible Cursor
;               ?47                  Alternate Screen (old xterm code)
;               ?1000        (V)     VT200 mouse tracking
;               ?1047                Alternate Screen (new xterm code)
;               ?1049                Alternate Screen (new xterm code)
;    ESC [ 5 i               (A)     Start relay to printer (ANSI Media Copy)
;    ESC [ 4 i               (A)     Stop relay to printer (ANSI Media Copy)
;    ESC [ 8 ; Ph ; Pw t             Resize the window to ‘Ph’ lines and
;                                    ‘Pw’ columns (SunView special)
;    ESC [ c                         Send VT100 Identification String
;    ESC [ x                 (V)     Send Terminal Parameter Report
;    ESC [ > c                       Send Secondary Device Attributes String
;    ESC [ 6 n                       Send Cursor Position Report