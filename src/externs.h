#ifndef __EXTERNS_H__
#define __EXTERNS_H__

extern const char terminalVersionString[];

// Addresses to settings stored in FLASH
extern unsigned char * kTerminalBaudrateSetting;
extern unsigned char * kTerminalFgBgColorSetting;
extern unsigned char * kTerminalScreenSizeSetting;
extern unsigned char * kTerminalFontSetting;

// Memory map of User RAM:
extern unsigned char * kFontBgColor;
extern unsigned char * kSettingsBackup;

/* Functions written in assembly */
// In flash.asm
extern void installDefaultSettings(void);
extern void writeUpdatedSettingsToFlash(void);

// In mainLoop.asm
extern void mainLoop(void);

// In printChar.asm
extern void printChar(unsigned char character);

// From screenHandling.asm
extern void clearCursorPosition();
extern void incrementHCursor();
extern void incrementVCursor();

// // in serialPorts.asm
extern void outCharA(unsigned char character);
extern char charAvailableB(void);
extern char inCharB(void);

#endif /* __EXTERNS_H__ */

