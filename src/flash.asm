
; ****************************************************************************
; This section contains code to manipulate the FLASH contents of the onboard
; SST39SF010A NOR FLASH chip.
;
; Code here needs to be written as position independent code since it will be 
; relocated outside of the NOR FLASH address region to be able to function 
; correctly.
; ****************************************************************************

    PUBLIC  doSettingsExist
    PUBLIC  _installDefaultSettings
    PUBLIC  _writeUpdatedSettingsToFlash

    PUBLIC  copy_productID_code
    PUBLIC  copy_byteProgram_code
    PUBLIC  copy_sectorErase_code
    PUBLIC  executeCopiedCode

    ; From globals.asm
    EXTERN  _kTerminalFgBgColorSetting
    EXTERN  _kTerminalScreenSizeSetting
    EXTERN  _kTerminalBaudrateSetting
    EXTERN  _kTerminalFontSetting

    EXTERN  kDefaultFontBgColor
    EXTERN  kDefaultScreenSize
    EXTERN  kDefaultBaudrate
    EXTERN  kDefaultFont

    EXTERN  _kSettingsBackup

; ****************************************************************************

SECTION code_user

; ****************************************************************************
; We check if we start from an unitialised FLASH by checking if all settings 
; contain 0xFF. If we don't find 3 times 0xFF, then we return 0x00 in A. 
; Otherwise, we return 0xFF in A.
doSettingsExist:
                LD      A, (_kTerminalFgBgColorSetting)
                CP      A, $FF
                JR      Z, checkScreenSetting
                LD      A, $00
                RET
                ;
checkScreenSetting:
                LD      A, (_kTerminalScreenSizeSetting)
                CP      A, $FF
                JR      Z, checkBaudrateSetting
                LD      A, $00
                RET
                ;
checkBaudrateSetting:
                LD      A, (_kTerminalBaudrateSetting)
                CP      A, $FF
                JR      Z, checkFontSetting
                LD      A, 00
                RET 
                ;
checkFontSetting:
                LD      A, (_kTerminalFontSetting)
                CP      A, $FF
                JR      Z, noSettings
                LD      A, 00                           ; 0x00 indicates that settings have been detected
                RET 
                ;
noSettings:
                LD      A, $FF                          ; 0xFF there are no settings stored yet
                RET

; ****************************************************************************

_installDefaultSettings:
                CALL    copy_sectorErase_code
                CALL    executeCopiedCode
                ;
                CALL    copy_byteProgram_code
                ;
                LD      HL, _kTerminalFgBgColorSetting
                LD      A, kDefaultFontBgColor
                CALL    executeCopiedCode
                ;
                LD      HL, _kTerminalScreenSizeSetting
                LD      A, kDefaultScreenSize
                CALL    executeCopiedCode
                ;
                LD      HL, _kTerminalBaudrateSetting
                LD      A, kDefaultBaudrate
                CALL    executeCopiedCode                
                ;
                LD      HL, _kTerminalFontSetting
                LD      A, kDefaultFont
                CALL    executeCopiedCode  
                ;
                RET

; ****************************************************************************

updateFgBgColorSetting:
                CALL    copyCurrentSettingsToRAM
                LD      (_kSettingsBackup + 0), A
                CALL    _writeUpdatedSettingsToFlash
                ;
                RET

; ****************************************************************************

updateScreenSizeSetting:
                CALL    copyCurrentSettingsToRAM
                LD      (_kSettingsBackup + 1), A
                CALL    _writeUpdatedSettingsToFlash
                ;
                RET

; ****************************************************************************

updateBaudrateSetting:
                CALL    copyCurrentSettingsToRAM
                LD      (_kSettingsBackup + 2), A
                CALL    _writeUpdatedSettingsToFlash
                ;
                RET

; ****************************************************************************

updateFontSetting:
                CALL    copyCurrentSettingsToRAM
                LD      (_kSettingsBackup + 3), A
                CALL    _writeUpdatedSettingsToFlash
                ;
                RET

; ****************************************************************************

copyCurrentSettingsToRAM:
                PUSH    AF
                ;
                LD      A, (_kTerminalBaudrateSetting)
                LD      (_kSettingsBackup + 0), A
                ;
                LD      A, (_kTerminalFgBgColorSetting)
                LD      (_kSettingsBackup + 1), A
                ;
                LD      A, (_kTerminalScreenSizeSetting)
                LD      (_kSettingsBackup + 2), A
                ;
                LD      A, (_kTerminalFontSetting)
                LD      (_kSettingsBackup + 3), A
                ;   
                POP     AF
                ;                                             
                RET

; ****************************************************************************

_writeUpdatedSettingsToFlash:
                CALL    copy_sectorErase_code
                CALL    executeCopiedCode
                ;
                CALL    copy_byteProgram_code
                ;
                LD      HL, _kSettingsBackup + 0
                LD      A, (HL)
                LD      HL, _kTerminalBaudrateSetting
                CALL    executeCopiedCode
                ;
                LD      HL, _kSettingsBackup + 1
                LD      A, (HL)
                LD      HL, _kTerminalFgBgColorSetting
                CALL    executeCopiedCode
                ;
                LD      HL, _kSettingsBackup + 2
                LD      A, (HL)
                LD      HL, _kTerminalScreenSizeSetting
                CALL    executeCopiedCode                
                ;
                LD      HL, _kSettingsBackup + 3
                LD      A, (HL)
                LD      HL, _kTerminalFontSetting
                CALL    executeCopiedCode  
                ;
                RET

; ****************************************************************************

copy_productID_code:
                LD      HL, productID_code
                LD      DE, userRamCopyArea
                LD      BC, productID_code_length
                LDIR
                ;
                RET

copy_byteProgram_code:
                LD      HL, byteProgram_code
                LD      DE, userRamCopyArea
                LD      BC, byteProgram_code_length
                LDIR
                ;
                RET

copy_sectorErase_code:
                LD      HL, sectorErase_code
                LD      DE, userRamCopyArea
                LD      BC, sectorErase_code_length
                LDIR
                ;
                RET

executeCopiedCode:
                JP      userRamCopyArea
                ;
                RET


; ****************************************************************************
;
; The following code needs to be position independent code since it will be 
; copied first into RAM before it's executed and that location can be anywhere.
;
; ****************************************************************************

productID_code:
                ; Software ID Entry
                LD      HL, $5555
                LD      (HL), $AA
                LD      HL, $2AAA
                LD      (HL), $55
                LD      HL, $5555
                LD      (HL), $90
                ; Fetch ID
                LD      A, ($0000)      ; Manufacturer’s ID = 0xBF (SST)
                LD      B, A        
                LD      A, ($0001)      ; SST39SF010A = 0xB5, SST39SF020A = 0xB6, SST39SF040 0xB7
                LD      C, A
                ; Software ID Exit
                LD      HL, $5555
                LD      (HL), $AA
                LD      HL, $2AAA
                LD      (HL), $55
                LD      HL, $5555
                LD      (HL), $F0
                RET


sectorErase_code:
                LD      HL, $5555
                LD      (HL), $AA
                LD      HL, $2AAA
                LD      (HL), $55
                LD      HL, $5555
                LD      (HL), $80
                LD      HL, $5555
                LD      (HL), $AA
                LD      HL, $2AAA
                LD      (HL), $55
                LD      HL, sectorAddressToErase
                LD      (HL), $30
                ;
                LD      HL, $FFFF       ; Big delay loop to give FLASH chip enough time to erase the sector
sectorErase_code_waitloop:
                DEC     HL
                LD      A, H
                CP      A, $00
                JR      NZ, sectorErase_code_waitloop
                ;
                RET


byteProgram_code:
                PUSH    HL              ; Save storage address for later use
                ; Byte Program Entry
                LD      HL, $5555
                LD      (HL), $AA
                LD      HL, $2AAA
                LD      (HL), $55
                LD      HL, $5555
                LD      (HL), $A0
                ; Program Byte
                POP     HL              ; Restore storage address
                LD      (HL), A         ; Store Byte
                ;
                LD      HL, $01FF       ; A delay counter setting to 0x0180 proved 
                                        ; to be OK for a Z180 running at 18.432 MHz
byteProgram_code_waitloop:
                DEC     HL
                LD      A, H
                CP      A, $00
                JR      NZ, byteProgram_code_waitloop
                ;
                RET

; ****************************************************************************

userRamCopyArea         EQU     $8C00
sectorAddressToErase    EQU     $7C00
productID_code_length   EQU       100   ; reality 35 ?
byteProgram_code_length EQU       100    
sectorErase_code_length EQU       100

; ****************************************************************************
