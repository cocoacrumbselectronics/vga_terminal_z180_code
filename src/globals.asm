; ****************************************************************************
; Defines used for the Z180 on the VGA terminal board
; ****************************************************************************

    PUBLIC  _kZ180RegBase
    PUBLIC  k6845StartingAddress
    PUBLIC  k6845CursorStartAddress
    PUBLIC  _kUserRamStart
    PUBLIC  _kStackPointer
    PUBLIC  _kCRAMStartAddress
    PUBLIC  _kCRAMSize
    PUBLIC  _kVRAMStartAddress
    PUBLIC  _kVRAMSize
    PUBLIC  _kVCRAMBitMask

    PUBLIC  _kTerminalWidth
    PUBLIC  _kTerminalLinesConf0
    PUBLIC  _kTerminalLinesConf1
    PUBLIC  _kTerminalLinesConf2
    PUBLIC  _kTerminalLinesConf3
    PUBLIC  _kTerminalLinesConf4
    PUBLIC  _kTerminalLinesConf5

    PUBLIC  _kASCI0_Rx_Size
    PUBLIC  _kASCI0_Rx_FIFO_Buffer
    PUBLIC  _kASCI0_Rx_Counter
    PUBLIC  _kASCI0_Rx_ReadAddr
    PUBLIC  _kASCI0_Rx_WriteAddr

    PUBLIC  _kCurrentVideoOffset
    PUBLIC  _kCurrentCursor_pos

    ; Memory map for storing the terminal settings in FLASH
    PUBLIC  _kTerminalFgBgColorSetting
    PUBLIC  _kTerminalScreenSizeSetting
    PUBLIC  _kTerminalBaudrateSetting
    PUBLIC  _kTerminalFontSetting

    ; Default settings
    PUBLIC  kDefaultFontBgColor
    PUBLIC  kDefaultScreenSize
    PUBLIC  kDefaultBaudrate
    PUBLIC  kDefaultFont

    ; Memory map for global variables
    PUBLIC  _kTerminalWidth
    PUBLIC  _kTerminalLines
    PUBLIC  _kFontBgColor
    PUBLIC  _kCursorPosH
    PUBLIC  _kCursorPosV
    PUBLIC  _k6845StartAddressH
    PUBLIC  _k6845StartAddressL
    PUBLIC  k6845StartAddress

    PUBLIC  _kCSI_nr_parameters
    PUBLIC  _kCSI_n1_value
    PUBLIC  _kCSI_n2_value

    PUBLIC  _kScratchSpace_8Bit
    PUBLIC  _kScratchSpace_16Bit

    PUBLIC  _kSettingsBackup

    PUBLIC  CRAM_startAddressPerLineLookUpTable
    PUBLIC  VRAM_startAddressPerLineLookUpTable

    ; ASCII codes
    PUBLIC  NUL
    PUBLIC  SOH
    PUBLIC  STX
    PUBLIC  ETX
    PUBLIC  EOT
    PUBLIC  ENQ
    PUBLIC  ACK
    PUBLIC  BEL
    PUBLIC  BS
    PUBLIC  HT
    PUBLIC  LF
    PUBLIC  VT
    PUBLIC  FF
    PUBLIC  CR
    PUBLIC  SO
    PUBLIC  SI
    PUBLIC  DLE
    PUBLIC  DCL
    PUBLIC  DC2
    PUBLIC  DC3
    PUBLIC  DC4
    PUBLIC  NAK
    PUBLIC  SYN
    PUBLIC  ETB
    PUBLIC  CAN
    PUBLIC  EM
    PUBLIC  SUB_
    PUBLIC  ESC
    PUBLIC  FS
    PUBLIC  GS
    PUBLIC  RS
    PUBLIC  US
    PUBLIC  SPACE

    PUBLIC  CtrlAltDel

; Defines for I/O
_kZ180RegBase:              EQU $C0                     ; Z180 register base address. Choose from 0x00, 0x40, 0x80, 0xC0.

; Defines to be used for the 6845 CRT controller
k6845StartingAddress        EQU $FFFF                   ; This start address should be the same for all 6845 setups
k6845CursorStartAddress     EQU $FFFF                   ; This start address should be the same for all 6845 setups

; Defines for addresses
_kUserRamStart:             EQU $8000
_kStackPointer:             EQU _kUserRamStart + $07FF  ; Start of stack pointer (2K RAM block).

_kCRAMStartAddress          EQU $C000                   ; Location of video attributes (color) RAM.
_kCRAMSize:                 EQU $2000                   ; 8K bytes supports up to 80 characters by 60 lines.
_kVRAMStartAddress:         EQU $E000                   ; Location of video RAM.
_kVRAMSize:                 EQU $2000                   ; 8K bytes supports up to 80 characters by 60 lines.
_kVCRAMBitMask              EQU $1F                     ; Masks an 8K memory area                   

; Defines for the different video settings
_kTerminalWidth             EQU 80                      ; All possible video modes have a fixed width of 80 characters
_kTerminalLinesConf0        EQU 24
_kTerminalLinesConf1        EQU 25
_kTerminalLinesConf2        EQU 30
_kTerminalLinesConf3        EQU 43
_kTerminalLinesConf4        EQU 51
_kTerminalLinesConf5        EQU 60

; Defines for the default settings
kDefaultFontBgColor         EQU $02                     ; Default is green on black
kDefaultScreenSize          EQU $01                     ; Default is 80x25
kDefaultBaudrate            EQU $09                     ; Default is 115200 baud
kDefaultFont                EQU $00                     ; Default is Font 0 (CP437, 16 pixels high)

; Defines for the ASCI0 Rx FIFO buffer
_kASCI0_Rx_Size             EQU $100
_kASCI0_Rx_FIFO_Buffer      EQU $9000

; Memory map for storing the terminal settings
_kTerminalBaudrateSetting    EQU $7FFC
_kTerminalFgBgColorSetting   EQU $7FFD
_kTerminalScreenSizeSetting  EQU $7FFE
_kTerminalFontSetting        EQU $7FFF

; Memory map for global variables
_kInterruptVectorTable      EQU _kUserRamStart                  ; == 0x8000
_kTerminalLines             EQU _kUserRamStart          + $20   ; == 0x8020
_kFontBgColor               EQU _kTerminalLines         + 1     ; == 0x8021
_kCursorPosH:               EQU _kFontBgColor           + 1     ; == 0x8022
_kCursorPosV:               EQU _kCursorPosH            + 1     ; == 0x8023
_k6845StartAddressH:        EQU _kCursorPosV            + 1     ; == 0x8024
_k6845StartAddressL:        EQU _k6845StartAddressH     + 1     ; == 0x8025
_kASCI0_Rx_Counter          EQU _k6845StartAddressL     + 1     ; == 0x8026
_kASCI0_Rx_ReadAddr         EQU _kASCI0_Rx_Counter      + 2     ; == 0x8028
_kASCI0_Rx_WriteAddr        EQU _kASCI0_Rx_ReadAddr     + 2     ; == 0x802A
_kCurrentVideoOffset        EQU _kASCI0_Rx_WriteAddr    + 2     ; == 0x802C
_kCurrentCursor_pos         EQU _kCurrentVideoOffset    + 2     ; == 0x802E
_kCSI_nr_parameters         EQU _kCurrentCursor_pos     + 2     ; == 0x8030
_kCSI_n1_value              EQU _kCSI_nr_parameters     + 1     ; == 0x8031
_kCSI_n2_value              EQU _kCSI_n1_value          + 1     ; == 0x8032
_kScratchSpace_8Bit         EQU _kCSI_n2_value          + 1     ; == 0x8033
_kScratchSpace_16Bit        EQU _kScratchSpace_8Bit     + 1     ; == 0x8034
_kSettingsBackup            EQU _kScratchSpace_16Bit    + 2     ; == 0x8036
_kEnd                       EQU _kSettingsBackup        + 4     ; == 0x803A

; Aliases   
k6845StartAddress:          EQU _k6845StartAddressH

NUL                         EQU $00     ; Null character
SOH                         EQU $01     ; Start of Header
STX                         EQU $02     ; Start of Text
ETX                         EQU $03     ; End of Text
EOT                         EQU $04     ; End of Transmission
ENQ                         EQU $05     ; Enquiry
ACK                         EQU $06     ; Acknowledge
BEL                         EQU $07     ; Bell
BS                          EQU $08     ; Backspace
HT                          EQU $09     ; Horizontal tab
LF                          EQU $0A     ; Line feed
VT                          EQU $0B     ; Vertical tab
FF                          EQU $0C     ; Form feed
CR                          EQU $0D     ; Carriage return
SO                          EQU $0E     ; Shift out
SI                          EQU $0F     ; Shift in
DLE                         EQU $10     ; Data link escape
DCL                         EQU $11     ; Xon (transmit on)
DC2                         EQU $12     ; Device control 2
DC3                         EQU $13     ; Xoff (transmit off)
DC4                         EQU $14     ; Device control 4
NAK                         EQU $15     ; Negative acknowledge
SYN                         EQU $16     ; Synchronous idle
ETB                         EQU $17     ; End of transmission
CAN                         EQU $18     ; Cancel
EM                          EQU $19     ; End of medium
SUB_                        EQU $1A     ; Substitute
ESC                         EQU $1B     ; Escape
FS                          EQU $1C     ; File separator
GS                          EQU $1D     ; Group separator
RS                          EQU $1E     ; Record separator
US                          EQU $1F     ; Unit separator
SPACE                       EQU $20     ; Space

CtrlAltDel                  EQU $FE     ; Ctrl Alt Delete key combination. Will be used to setup the terminal

; ****************************************************************************

    SECTION data_user

; ****************************************************************************

CRAM_startAddressPerLineLookUpTable:
    DEFW    _kCRAMStartAddress + (0 * 80)
    DEFW    _kCRAMStartAddress + (1 * 80)
    DEFW    _kCRAMStartAddress + (2 * 80)
    DEFW    _kCRAMStartAddress + (3 * 80)
    DEFW    _kCRAMStartAddress + (4 * 80)
    DEFW    _kCRAMStartAddress + (5 * 80)
    DEFW    _kCRAMStartAddress + (6 * 80)
    DEFW    _kCRAMStartAddress + (7 * 80)
    DEFW    _kCRAMStartAddress + (8 * 80)
    DEFW    _kCRAMStartAddress + (9 * 80)
    DEFW    _kCRAMStartAddress + (10 * 80)
    DEFW    _kCRAMStartAddress + (11 * 80)
    DEFW    _kCRAMStartAddress + (12 * 80)
    DEFW    _kCRAMStartAddress + (13 * 80)
    DEFW    _kCRAMStartAddress + (14 * 80)
    DEFW    _kCRAMStartAddress + (15 * 80)
    DEFW    _kCRAMStartAddress + (16 * 80)
    DEFW    _kCRAMStartAddress + (17 * 80)
    DEFW    _kCRAMStartAddress + (18 * 80)
    DEFW    _kCRAMStartAddress + (19 * 80)
    DEFW    _kCRAMStartAddress + (20 * 80)
    DEFW    _kCRAMStartAddress + (21 * 80)
    DEFW    _kCRAMStartAddress + (22 * 80)
    DEFW    _kCRAMStartAddress + (23 * 80)
    DEFW    _kCRAMStartAddress + (24 * 80)
    DEFW    _kCRAMStartAddress + (25 * 80)
    DEFW    _kCRAMStartAddress + (26 * 80)
    DEFW    _kCRAMStartAddress + (27 * 80)
    DEFW    _kCRAMStartAddress + (28 * 80)
    DEFW    _kCRAMStartAddress + (29 * 80)
    DEFW    _kCRAMStartAddress + (30 * 80)
    DEFW    _kCRAMStartAddress + (31 * 80)
    DEFW    _kCRAMStartAddress + (32 * 80)
    DEFW    _kCRAMStartAddress + (33 * 80)
    DEFW    _kCRAMStartAddress + (34 * 80)
    DEFW    _kCRAMStartAddress + (35 * 80)
    DEFW    _kCRAMStartAddress + (36 * 80)
    DEFW    _kCRAMStartAddress + (37 * 80)
    DEFW    _kCRAMStartAddress + (38 * 80)
    DEFW    _kCRAMStartAddress + (39 * 80)
    DEFW    _kCRAMStartAddress + (40 * 80)
    DEFW    _kCRAMStartAddress + (41 * 80)
    DEFW    _kCRAMStartAddress + (42 * 80)
    DEFW    _kCRAMStartAddress + (43 * 80)
    DEFW    _kCRAMStartAddress + (44 * 80)
    DEFW    _kCRAMStartAddress + (45 * 80)
    DEFW    _kCRAMStartAddress + (46 * 80)
    DEFW    _kCRAMStartAddress + (47 * 80)
    DEFW    _kCRAMStartAddress + (48 * 80)
    DEFW    _kCRAMStartAddress + (49 * 80)
    DEFW    _kCRAMStartAddress + (50 * 80)
    DEFW    _kCRAMStartAddress + (51 * 80)
    DEFW    _kCRAMStartAddress + (52 * 80)
    DEFW    _kCRAMStartAddress + (53 * 80)
    DEFW    _kCRAMStartAddress + (54 * 80)
    DEFW    _kCRAMStartAddress + (55 * 80)
    DEFW    _kCRAMStartAddress + (56 * 80)
    DEFW    _kCRAMStartAddress + (57 * 80)
    DEFW    _kCRAMStartAddress + (58 * 80)
    DEFW    _kCRAMStartAddress + (59 * 80)
    DEFW    _kCRAMStartAddress + (60 * 80)
    DEFW    _kCRAMStartAddress + (61 * 80)
    DEFW    _kCRAMStartAddress + (62 * 80)
    DEFW    _kCRAMStartAddress + (63 * 80)
    DEFW    _kCRAMStartAddress + (64 * 80)
    DEFW    _kCRAMStartAddress + (65 * 80)
    DEFW    _kCRAMStartAddress + (66 * 80)
    DEFW    _kCRAMStartAddress + (67 * 80)
    DEFW    _kCRAMStartAddress + (68 * 80)
    DEFW    _kCRAMStartAddress + (69 * 80)
    DEFW    _kCRAMStartAddress + (70 * 80)
    DEFW    _kCRAMStartAddress + (71 * 80)
    DEFW    _kCRAMStartAddress + (72 * 80)
    DEFW    _kCRAMStartAddress + (73 * 80)
    DEFW    _kCRAMStartAddress + (74 * 80)
    DEFW    _kCRAMStartAddress + (75 * 80)
    DEFW    _kCRAMStartAddress + (76 * 80)
    DEFW    _kCRAMStartAddress + (77 * 80)
    DEFW    _kCRAMStartAddress + (78 * 80)
    DEFW    _kCRAMStartAddress + (79 * 80)
    DEFW    _kCRAMStartAddress + (80 * 80)
    DEFW    _kCRAMStartAddress + (81 * 80)
    DEFW    _kCRAMStartAddress + (82 * 80)
    DEFW    _kCRAMStartAddress + (83 * 80)
    DEFW    _kCRAMStartAddress + (84 * 80)
    DEFW    _kCRAMStartAddress + (85 * 80)
    DEFW    _kCRAMStartAddress + (86 * 80)
    DEFW    _kCRAMStartAddress + (87 * 80)
    DEFW    _kCRAMStartAddress + (88 * 80)
    DEFW    _kCRAMStartAddress + (89 * 80)
    DEFW    _kCRAMStartAddress + (90 * 80)
    DEFW    _kCRAMStartAddress + (91 * 80)
    DEFW    _kCRAMStartAddress + (92 * 80)
    DEFW    _kCRAMStartAddress + (93 * 80)
    DEFW    _kCRAMStartAddress + (94 * 80)
    DEFW    _kCRAMStartAddress + (95 * 80)
    DEFW    _kCRAMStartAddress + (96 * 80)
    DEFW    _kCRAMStartAddress + (97 * 80)
    DEFW    _kCRAMStartAddress + (98 * 80)
    DEFW    _kCRAMStartAddress + (99 * 80)
    DEFW    _kCRAMStartAddress + (100 * 80)
    DEFW    _kCRAMStartAddress + (101 * 80)
    DEFW    _kCRAMStartAddress + (102 * 80)
    DEFW    _kCRAMStartAddress + (103 * 80)
    DEFW    _kCRAMStartAddress + (104 * 80)
    DEFW    _kCRAMStartAddress + (105 * 80)

; ****************************************************************************

VRAM_startAddressPerLineLookUpTable:
    DEFW    _kVRAMStartAddress + (0 * 80)
    DEFW    _kVRAMStartAddress + (1 * 80)
    DEFW    _kVRAMStartAddress + (2 * 80)
    DEFW    _kVRAMStartAddress + (3 * 80)
    DEFW    _kVRAMStartAddress + (4 * 80)
    DEFW    _kVRAMStartAddress + (5 * 80)
    DEFW    _kVRAMStartAddress + (6 * 80)
    DEFW    _kVRAMStartAddress + (7 * 80)
    DEFW    _kVRAMStartAddress + (8 * 80)
    DEFW    _kVRAMStartAddress + (9 * 80)
    DEFW    _kVRAMStartAddress + (10 * 80)
    DEFW    _kVRAMStartAddress + (11 * 80)
    DEFW    _kVRAMStartAddress + (12 * 80)
    DEFW    _kVRAMStartAddress + (13 * 80)
    DEFW    _kVRAMStartAddress + (14 * 80)
    DEFW    _kVRAMStartAddress + (15 * 80)
    DEFW    _kVRAMStartAddress + (16 * 80)
    DEFW    _kVRAMStartAddress + (17 * 80)
    DEFW    _kVRAMStartAddress + (18 * 80)
    DEFW    _kVRAMStartAddress + (19 * 80)
    DEFW    _kVRAMStartAddress + (20 * 80)
    DEFW    _kVRAMStartAddress + (21 * 80)
    DEFW    _kVRAMStartAddress + (22 * 80)
    DEFW    _kVRAMStartAddress + (23 * 80)
    DEFW    _kVRAMStartAddress + (24 * 80)
    DEFW    _kVRAMStartAddress + (25 * 80)
    DEFW    _kVRAMStartAddress + (26 * 80)
    DEFW    _kVRAMStartAddress + (27 * 80)
    DEFW    _kVRAMStartAddress + (28 * 80)
    DEFW    _kVRAMStartAddress + (29 * 80)
    DEFW    _kVRAMStartAddress + (30 * 80)
    DEFW    _kVRAMStartAddress + (31 * 80)
    DEFW    _kVRAMStartAddress + (32 * 80)
    DEFW    _kVRAMStartAddress + (33 * 80)
    DEFW    _kVRAMStartAddress + (34 * 80)
    DEFW    _kVRAMStartAddress + (35 * 80)
    DEFW    _kVRAMStartAddress + (36 * 80)
    DEFW    _kVRAMStartAddress + (37 * 80)
    DEFW    _kVRAMStartAddress + (38 * 80)
    DEFW    _kVRAMStartAddress + (39 * 80)
    DEFW    _kVRAMStartAddress + (40 * 80)
    DEFW    _kVRAMStartAddress + (41 * 80)
    DEFW    _kVRAMStartAddress + (42 * 80)
    DEFW    _kVRAMStartAddress + (43 * 80)
    DEFW    _kVRAMStartAddress + (44 * 80)
    DEFW    _kVRAMStartAddress + (45 * 80)
    DEFW    _kVRAMStartAddress + (46 * 80)
    DEFW    _kVRAMStartAddress + (47 * 80)
    DEFW    _kVRAMStartAddress + (48 * 80)
    DEFW    _kVRAMStartAddress + (49 * 80)
    DEFW    _kVRAMStartAddress + (50 * 80)
    DEFW    _kVRAMStartAddress + (51 * 80)
    DEFW    _kVRAMStartAddress + (52 * 80)
    DEFW    _kVRAMStartAddress + (53 * 80)
    DEFW    _kVRAMStartAddress + (54 * 80)
    DEFW    _kVRAMStartAddress + (55 * 80)
    DEFW    _kVRAMStartAddress + (56 * 80)
    DEFW    _kVRAMStartAddress + (57 * 80)
    DEFW    _kVRAMStartAddress + (58 * 80)
    DEFW    _kVRAMStartAddress + (59 * 80)
    DEFW    _kVRAMStartAddress + (60 * 80)
    DEFW    _kVRAMStartAddress + (61 * 80)
    DEFW    _kVRAMStartAddress + (62 * 80)
    DEFW    _kVRAMStartAddress + (63 * 80)
    DEFW    _kVRAMStartAddress + (64 * 80)
    DEFW    _kVRAMStartAddress + (65 * 80)
    DEFW    _kVRAMStartAddress + (66 * 80)
    DEFW    _kVRAMStartAddress + (67 * 80)
    DEFW    _kVRAMStartAddress + (68 * 80)
    DEFW    _kVRAMStartAddress + (69 * 80)
    DEFW    _kVRAMStartAddress + (70 * 80)
    DEFW    _kVRAMStartAddress + (71 * 80)
    DEFW    _kVRAMStartAddress + (72 * 80)
    DEFW    _kVRAMStartAddress + (73 * 80)
    DEFW    _kVRAMStartAddress + (74 * 80)
    DEFW    _kVRAMStartAddress + (75 * 80)
    DEFW    _kVRAMStartAddress + (76 * 80)
    DEFW    _kVRAMStartAddress + (77 * 80)
    DEFW    _kVRAMStartAddress + (78 * 80)
    DEFW    _kVRAMStartAddress + (79 * 80)
    DEFW    _kVRAMStartAddress + (80 * 80)
    DEFW    _kVRAMStartAddress + (81 * 80)
    DEFW    _kVRAMStartAddress + (82 * 80)
    DEFW    _kVRAMStartAddress + (83 * 80)
    DEFW    _kVRAMStartAddress + (84 * 80)
    DEFW    _kVRAMStartAddress + (85 * 80)
    DEFW    _kVRAMStartAddress + (86 * 80)
    DEFW    _kVRAMStartAddress + (87 * 80)
    DEFW    _kVRAMStartAddress + (88 * 80)
    DEFW    _kVRAMStartAddress + (89 * 80)
    DEFW    _kVRAMStartAddress + (90 * 80)
    DEFW    _kVRAMStartAddress + (91 * 80)
    DEFW    _kVRAMStartAddress + (92 * 80)
    DEFW    _kVRAMStartAddress + (93 * 80)
    DEFW    _kVRAMStartAddress + (94 * 80)
    DEFW    _kVRAMStartAddress + (95 * 80)
    DEFW    _kVRAMStartAddress + (96 * 80)
    DEFW    _kVRAMStartAddress + (97 * 80)
    DEFW    _kVRAMStartAddress + (98 * 80)
    DEFW    _kVRAMStartAddress + (99 * 80)
    DEFW    _kVRAMStartAddress + (100 * 80)
    DEFW    _kVRAMStartAddress + (101 * 80)
    DEFW    _kVRAMStartAddress + (102 * 80)
    DEFW    _kVRAMStartAddress + (103 * 80)
    DEFW    _kVRAMStartAddress + (104 * 80)
    DEFW    _kVRAMStartAddress + (105 * 80)

; ****************************************************************************
