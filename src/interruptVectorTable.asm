    INCLUDE "Z180_registers.asm"
    EXTERN  _kZ180RegBase

    PUBLIC  _z180_int_int1
    PUBLIC  _z180_int_int2
    PUBLIC  _z180_int_prt0
    PUBLIC  _z180_int_prt1
    PUBLIC  _z180_int_dma0
    PUBLIC  _z180_int_dma1
    PUBLIC  _z180_int_csio
    PUBLIC  _z180_int_asci0
    PUBLIC  _z180_int_asci1

    PUBLIC  asci0_Flush_Rx
    PUBLIC  asci0_interrupt
    PUBLIC  _asci0_charAvailableA
    PUBLIC  _asci0_inCharA


    EXTERN  __interrupt_vectors_head
    EXTERN  __interrupt_vectors_size
    
    EXTERN  _kASCI0_Rx_Size
    EXTERN  _kASCI0_Rx_FIFO_Buffer
    EXTERN  _kASCI0_Rx_Counter
    EXTERN  _kASCI0_Rx_ReadAddr
    EXTERN  _kASCI0_Rx_WriteAddr    

; ****************************************************************************

    SECTION code_crt_start

; ****************************************************************************

asci0_Flush_Rx:
                LD      HL, $0000
                LD      (_kASCI0_Rx_Counter), HL
                ;
                LD      HL, _kASCI0_Rx_FIFO_Buffer
                LD      (_kASCI0_Rx_ReadAddr), HL
                LD      (_kASCI0_Rx_WriteAddr), HL
                RET

; ****************************************************************************


asci0_interrupt:
                PUSH    AF
                PUSH    HL
                ;
                IN0     A, (STAT0)                          ; load the ASCI0 status register
                TST     STAT0_RDRF                          ; test whether we have received on ASCI0
                JR      Z, ASCI0_TX_CHECK                   ; if not, go check for bytes to transmit
                ;
ASCI0_RX_GET:
                IN0     L, (RDR0)                           ; move Rx byte from the ASCI0 RDR to l
                AND     STAT0_OVRN | STAT0_PE | STAT0_FE    ; test whether we have error on ASCI0
                JR      NZ, ASCI0_RX_ERROR                  ; drop this byte, clear error, and get the next byte
                ;
                LD      A, (_kASCI0_Rx_Counter)
                CP      _kASCI0_Rx_Size-1                   ; check whether there is space in the buffer
                JR      NC, ASCI0_RX_CHECK                  ; buffer full, check whether we need to drain H/W FIFO

                LD      A, L                                ; get Rx byte from l
                LD      HL, (_kASCI0_Rx_WriteAddr)          ; get the pointer to where we poke
                LD      (HL), A                             ; write the Rx byte to the asci0RxIn target

                INC     L                                   ; move the Rx pointer low byte along (works only on 256 byte sized buffers)
                LD      (_kASCI0_Rx_WriteAddr), HL          ; write where the next byte should be poked
                ;
                LD      HL, _kASCI0_Rx_Counter
                INC     (HL)                                ; atomically increment Rx buffer count
                JR      ASCI0_RX_CHECK                      ; check for additional bytes
                ;
ASCI0_RX_ERROR:
                IN0     A, (CNTLA0)                         ; get the CNTRLA0 register
                AND     ~ CNTLA0_EFR                        ; to clear the error flag, EFR, to 0 
                OUT0    (CNTLA0), A                         ; and write it back
                ;
ASCI0_RX_CHECK:
                IN0     A, (STAT0)                          ; load the ASCI0 status register
                TST     STAT0_RDRF                          ; test whether we have received on ASCI0
                JR      NZ, ASCI0_RX_GET                    ; if still more bytes in H/W FIFO, get them
                ;
ASCI0_TX_CHECK:
ASCI0_TX_END:
                ;
                POP     HL
                POP     AF
                EI                                          ; Re-enable interrupts (RETI doesn't do that for you)
                RETI

; ****************************************************************************

_asci0_charAvailableA:
                LD      A, (_kASCI0_Rx_Counter)             ; get the number of bytes in the Rx buffer
                LD      L, A                                ; and put it in hl
                RET

; ****************************************************************************

_asci0_inCharA:
                ; exit     : l = char received
                ;
                ; modifies : af, hl

                LD      HL, _kASCI0_Rx_Counter
                DI
                DEC     (HL)                                ; atomically decrement Rx count
                LD      HL, (_kASCI0_Rx_ReadAddr)           ; get the pointer to place where we pop the Rx byte
                EI
                LD      A, (HL)                             ; get the Rx byte

                INC     L                                   ; move the Rx pointer low byte along
                LD      (_kASCI0_Rx_ReadAddr), HL           ; write where the next byte should be popped

                LD      L, A                                ; put the byte in hl
                RET


; ****************************************************************************

    ; This table must be down here otherwise the value of asm_asci0_interrupt 
    ; to be filled in for _z180_int_asci0 is not correct.
    DEFC    _z180_int_int1  = 0
    DEFC    _z180_int_int2  = 0
    DEFC    _z180_int_prt0  = 0
    DEFC    _z180_int_prt1  = 0
    DEFC    _z180_int_dma0  = 0
    DEFC    _z180_int_dma1  = 0
    DEFC    _z180_int_csio  = 0
    DEFC    _z180_int_asci0 = asci0_interrupt
    DEFC    _z180_int_asci1 = 0
    