#include <string.h>

#include "externs.h"
#include "colors.h"
#include "utils.h"

const char terminalVersionString[] = "v0.9.0";

const char greetingMessagePart1[] = "   ______#      #     #      #      #______#     #      #          #__    #         \r\n" \
                                    "  / ____/#___  #_____#____  #____ _#/ ____/#____#__  __#____ ___  #/ /_  #_____     \r\n" \
                                    " / /   #/ __ \\#/ ___#/ __ \\#/ __ `#/ /   #/ ___#/ / / #/ __ `__ \\#/ __ \\#/ ___/ \r\n" \
                                    "/ /___#/ /_/ #/ /__#/ /_/ #/ /_/ #/ /___#/ /  #/ /_/ #/ / / / / #/ /_/ #(__  )      \r\n" \
                                    "\\____/#\\____/#\\___/#\\____/#\\__,_/#\\____#/_/  # \\__,_#/_/ /_/ /_#/_.___#/____/\r\n" \
                                    "# _ #   #    #  #     #  __#    #  #    #     #   # __# _    #___#    #  #    #  #   #    #    \r\n" \
                                    "#|_)# _ #_|_ #__# _   # /  # _  #| # _  #__   #\\ /#/__#|_|   # | # _  #__#__  #o #__ # _  #|  \r\n" \
                                    "#| \\#(/_# |_ #| #(_)  # \\__#(_) #| #(_) #|    # V #\\_|#| |   # | #(/_ #| #||| #| #| |#(_| #|\r\n" \
                                    "\r\n";

const char greetingMessagePart2[] = "  Terminal Version: ";
const char greetingMessagePart3[] = "  Configuration   : 80 x ";
const char greetingMessagePart4[] = "  Baudrate        : ";
const char greetingMessagePart5[] = "  ROM             : 0x0000 - 0x7FFF\r\n"     \
                                    "  USER RAM        : 0x8000 - 0x9FFF\r\n"     \
                                    "  COLOR RAM       : 0xC000 - 0xDFFF\r\n"     \
                                    "  VIDEO RAM       : 0xE000 - 0xFFFF\r\n";

const char screenSize_24_message[] = {"24\r\n"};
const char screenSize_25_message[] = {"25\r\n"};
const char screenSize_30_message[] = {"30\r\n"};
const char screenSize_43_message[] = {"43\r\n"};
const char screenSize_51_message[] = {"51\r\n"};
const char screenSize_60_message[] = {"60\r\n"};

const char *screenSizeMessage[6] = { screenSize_24_message,
                                     screenSize_25_message,
                                     screenSize_30_message,
                                     screenSize_43_message,
                                     screenSize_51_message,
                                     screenSize_60_message };

const char baudrate____300_message[] = "300 baud (8N1)\r\n";
const char baudrate____600_message[] = "600 baud (8N1)\r\n";
const char baudrate___1200_message[] = "1200 baud (8N1)\r\n";
const char baudrate___2400_message[] = "2400 baud (8N1)\r\n";
const char baudrate___4800_message[] = "4800 baud (8N1)\r\n";
const char baudrate___9600_message[] = "9600 baud (8N1)\r\n";
const char baudrate__19200_message[] = "19200 baud (8N1)\r\n";
const char baudrate__38400_message[] = "38400 baud (8N1)\r\n";
const char baudrate__57600_message[] = "57600 baud (8N1)\r\n";
const char baudrate_115200_message[] = "115200 baud (8N1)\r\n";

const char *baudrate_message[10] = { baudrate____300_message, 
                                     baudrate____600_message,
                                     baudrate___1200_message,
                                     baudrate___2400_message,
                                     baudrate___4800_message,
                                     baudrate___9600_message,
                                     baudrate__19200_message,
                                     baudrate__38400_message,
                                     baudrate__57600_message,
                                     baudrate_115200_message };

/* showGreetingMessage()
   Is only called when the terminal is booted. It is safe to assume that the 
   screen is completely empty and that the color RAM has been filled with the 
   desired character color as set by d0 - d2 of the dipswitch.              */
void showGreetingMessage(void)
{
    unsigned int    idx             = 0;
    unsigned char   currentColor    = 1;
    unsigned char   colorBackup     = getCurrentColor();

    while (greetingMessagePart1[idx] != '\0')
    {
        switch (greetingMessagePart1[idx])
        {
            case '\n':
                currentColor = 1;
                break;
            case '#':
                idx++;
                currentColor++;
                if (currentColor == 8)
                {
                    currentColor++;
                } /* end if */
                if (currentColor == 15)
                {
                    currentColor = 1;
                } /* end if */
            default:
                break;
        } /* end switch */
        setColor(currentColor | (colorBackup & 0xF0));
        printChar(greetingMessagePart1[idx]);
        idx++;
    } /* end while */

    setColor(colorBackup);
    printString(greetingMessagePart2);
    printString(terminalVersionString);
    printString("\r\n");
    printString(greetingMessagePart3);
    printString((char *)screenSizeMessage[*((unsigned char*)&kTerminalScreenSizeSetting)]);
    printString(greetingMessagePart4);
    printString((char *)baudrate_message[*((unsigned char*)&kTerminalBaudrateSetting)]);
    printString(greetingMessagePart5);
} /* end showGreetingMessage */

/* ************************************************************************* */

int main()
{
    showGreetingMessage();
    mainLoop();

    return 0;   // We never get here since "mainLoop()" above is an endless loop.
} /* end main */
