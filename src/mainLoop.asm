; ****************************************************************************
; This is the main loop for the terminal code. Accepting input from the 
; keyboard and send it out and accepting input from the serial port input.
; ****************************************************************************

    PUBLIC  _mainLoop

    ; From globals.asm
    EXTERN  CtrlAltDel

    ; From interruptVectorTable.asm
    EXTERN  _asci0_charAvailableA
    EXTERN  _asci0_inCharA

    ; From printChar.asm
    EXTERN  printChar

    ; From serialPorts.asm
    EXTERN  outCharA
    EXTERN  charAvailableB
    EXTERN  inCharB

    ; From setup.c
    EXTERN  _setup

; ****************************************************************************

SECTION code_user

; ****************************************************************************

_mainLoop:
                CALL    _asci0_charAvailableA               ; Input available from serial in?
                CP      A, $00
                JR      Z, _noCharInAsci0                   ; No, check for keyboard input.
                ;
                CALL    _asci0_inCharA                      ; Character in A
                CALL    printChar                           ; Show it on screen
                JR      _mainLoop                           ; Check if there is more input from serial in.
                ;
_noCharInAsci0:
                CALL    charAvailableB                      ; Character available from the keyboard?
                CP      A, $00
                JR      Z, _mainLoop                        ; No, check for serial in input.
                CALL    inCharB                             ; Fetch keyboard character
                ;
                ; Intercept CtrlAltDel which is used for the setup screen
                CP      A, CtrlAltDel                       ; Ctrl Alt Del calls the setup screen
                CALL    Z, _setup                           ; The setup screen is handled in C code
                ;
                CALL    outCharA                            ; Send it to serial out
                JR      _mainLoop                           ; Check if there is more input from serial in.
