; ****************************************************************************
; Utility code for various screen handling operations
; ****************************************************************************

    PUBLIC  printChar
    PUBLIC  _printChar
    PUBLIC  fetchNextCharacter

    ; From 6845.asm
    EXTERN  init6845StartingAddresses

    ; From escapeCSIHandling.asm
    EXTERN  handleEscapeCSISequence

    ; From interruptVectorTable.asm
    EXTERN  _asci0_charAvailableA
    EXTERN  _asci0_inCharA
    
    ; From globals.asm
    EXTERN  _kFontBgColor
    EXTERN  _kVRAMStartAddress
    EXTERN  _kCRAMStartAddress
    EXTERN  _kCurrentVideoOffset
    EXTERN  _kTerminalLines
    EXTERN  _kCursorPosV
    EXTERN  _kCSI_nr_parameters
    EXTERN  _kCSI_n1_value
    EXTERN  _kCSI_n2_value
    EXTERN  ESC
    EXTERN  SPACE

    ; From screenHandling.asm
    EXTERN  clearScreen
    EXTERN  clearCursorHPosition
    EXTERN  decrementHCursor
    EXTERN  incrementHCursor
    EXTERN  incrementVCursorWithScrollUp
    EXTERN  initScreen
    EXTERN  scrollUp

; ****************************************************************************

    CSI                         EQU     '['

; ****************************************************************************

SECTION code_user

; ****************************************************************************

fetchNextCharacter:
                CALL    _asci0_charAvailableA               ; Input available from serial in?
                CP      A, $00
                JR      Z, fetchNextCharacter               ; No, check for keyboard input.
                ;
                CALL    _asci0_inCharA                      ; Character in A
                RET

; ****************************************************************************

_printChar:
                PUSH    BC
                PUSH    DE
                PUSH    HL
                CALL    printChar
                POP     HL
                POP     DE
                POP     BC
                RET

; ****************************************************************************

printChar:
                CP      A, SPACE                            ; Check if the incoming character is a printable character
                JP      C, handleCtrlCharacters             ; If not check it seperatly.
                ;
printCharAnyway:
                LD      BC, (_kCurrentVideoOffset)
                LD      HL, _kVRAMStartAddress
                ADD     HL, BC
                LD      (HL), A                             ; Char to print in A
                ;
                LD      A, (_kFontBgColor)                  ; Load current font/background colour
                LD      HL, _kCRAMStartAddress
                ADD     HL, BC
                LD      (HL), A                             ; A contains the colour attributes
                ;
                CALL    incrementHCursor
                RET

; ****************************************************************************

handleCtrlCharacters:
                LD      D, $00
                LD      E, A                                ; Load entry number for the lookup table in DE
                LD      HL, ctrlFunctionCallTable           ; Start of function call pointer lookup table
                ADD     HL, DE                              ; Add entry number to start of lookup table
                ADD     HL, DE                              ; Three times because each entry is 3 bytes
                ADD     HL, DE                              ; Add entry number to start of lookup table
                JP      (HL)                                ; Jump to selected function

; ****************************************************************************

handle_NUL:
                RET 

handle_SOH:
                RET 

handle_STX:
                RET 

handle_ETX:
                RET 

handle_EOT:
                RET 

handle_ENQ:
                RET 

handle_ACK:
                RET 

handle_BEL :
                RET 

handle_BS:
                CALL    decrementHCursor
                LD      A, SPACE
                CALL    printCharAnyway
                CALL    decrementHCursor
                RET 

handle_HT:
                RET 

; ---

handle_LF:
                CALL    incrementVCursorWithScrollUp
                RET

; ---

handle_VT:
                RET 

; ---

handle_FF:
                CALL    init6845StartingAddresses
                CALL    initScreen
                CALL    clearScreen
                RET 

; ---

handle_CR:
                CALL    clearCursorHPosition
                RET 

; ---

handle_SO:
                RET 

handle_SI:
                RET 

handle_DLE:
                RET 

handle_DCL:
                RET 

handle_DC2:
                RET 

handle_DC3:
                RET 

handle_DC4:
                RET 

handle_NAK:
                RET 

handle_SYN:
                RET 

handle_ETB:
                RET 

handle_CAN:
                RET 

handle_EM:
                RET 

handle_SUB:
                ; 1A (26) - Treat next byte as a character (to allow PC DOS char codes 1 to 31 to be displayed on screen)
                CALL    fetchNextCharacter
                JP      printCharAnyway

; ---

handle_ESC:
                CALL    fetchNextCharacter
                CP      CSI
                JR      NZ, handle_ESC_1
                ;
                LD      A, $00                              ; Clear place holders for numeric values associated
                LD      (_kCSI_n1_value), A                 ; with various CSI commands
                LD      (_kCSI_n2_value), A
                LD      A, $01                              ; Assume we will receive 1 numeric value for a CSI command
                LD      (_kCSI_nr_parameters), A
                ;
                CALL    fetchNextCharacter
                JP      handleEscapeCSISequence
handle_ESC_1:
                ; If it's not a known escape sequence, then just print out the escape 
                ; character and the character that followed it.
                PUSH    AF                                  ; Save character that followed the escape character on the stack
                LD      A, ESC                              ; Print the character that corresponds to 0x1B of the current font
                CALL    printCharAnyway
                POP     AF                                  ; Fetch the character that followed the escape character
                JP      printCharAnyway

; ---

handle_FS:
                RET 

handle_GS:
                RET 

handle_RS:
                RET 

handle_US:
                RET 


; ****************************************************************************

ctrlFunctionCallTable:
    DEFB    $C3
    DEFW    handle_NUL
    DEFB    $C3
    DEFW    handle_SOH
    DEFB    $C3
    DEFW    handle_STX
    DEFB    $C3
    DEFW    handle_ETX
    DEFB    $C3
    DEFW    handle_EOT
    DEFB    $C3
    DEFW    handle_ENQ
    DEFB    $C3
    DEFW    handle_ACK
    DEFB    $C3
    DEFW    handle_BEL 
    DEFB    $C3
    DEFW    handle_BS
    DEFB    $C3
    DEFW    handle_HT
    DEFB    $C3
    DEFW    handle_LF
    DEFB    $C3
    DEFW    handle_VT
    DEFB    $C3
    DEFW    handle_FF
    DEFB    $C3
    DEFW    handle_CR
    DEFB    $C3
    DEFW    handle_SO
    DEFB    $C3
    DEFW    handle_SI
    DEFB    $C3
    DEFW    handle_DLE
    DEFB    $C3
    DEFW    handle_DCL
    DEFB    $C3
    DEFW    handle_DC2
    DEFB    $C3
    DEFW    handle_DC3
    DEFB    $C3
    DEFW    handle_DC4
    DEFB    $C3
    DEFW    handle_NAK
    DEFB    $C3
    DEFW    handle_SYN
    DEFB    $C3
    DEFW    handle_ETB
    DEFB    $C3
    DEFW    handle_CAN
    DEFB    $C3
    DEFW    handle_EM
    DEFB    $C3
    DEFW    handle_SUB
    DEFB    $C3
    DEFW    handle_ESC
    DEFB    $C3
    DEFW    handle_FS
    DEFB    $C3
    DEFW    handle_GS
    DEFB    $C3
    DEFW    handle_RS
    DEFB    $C3
    DEFW    handle_US
