; ****************************************************************************
; Utility code for various screen handling operations
; ****************************************************************************

    PUBLIC  initScreen

    ; C Callable functions
    PUBLIC  clearScreen
    PUBLIC  clearScreenFromCursor
    PUBLIC  clearScreenTillCursor
    PUBLIC  clearFromBeginOfLineToCursorPos
    PUBLIC  clearFromCursorPosTillEndOfLine
    PUBLIC  clearCursorHPosition
    PUBLIC  clearCursorVPosition
    PUBLIC  incrementHCursor
    PUBLIC  decrementHCursor
    PUBLIC  incrementVCursor
    PUBLIC  incrementVCursorWithScrollUp
    PUBLIC  decrementVCursor
    PUBLIC  decrementVCursorWithScrollDown
    PUBLIC  scrollUp
    PUBLIC  scrollDown
    PUBLIC  insertLine
    PUBLIC  deleteLine

    PUBLIC  _clearCursorPosition
    PUBLIC  _incrementHCursor
    PUBLIC  _incrementVCursor

; ****************************************************************************

SECTION code_user

; ****************************************************************************

    ; From crt6845.asm
    EXTERN  update6845StartAddressRegisters
    EXTERN  update6845CursorRegisters

    ; From cursorHandling.asm
    EXTERN  mapCursorPosToVRAM
    EXTERN  mapCursorPosTo6845Register

    ; From globals.asm
    EXTERN  _kCRAMStartAddress
    EXTERN  _kCRAMSize
    EXTERN  _kVRAMStartAddress
    EXTERN  _kVRAMSize
    EXTERN  _kVCRAMBitMask
    EXTERN  k6845StartAddress
    EXTERN  _kTerminalLinesConf1
    EXTERN  _kTerminalWidth
    EXTERN  _kTerminalLines
    EXTERN  _kFontBgColor
    EXTERN  _kCursorPosH
    EXTERN  _kCursorPosV
    EXTERN  _kCurrentVideoOffset
    EXTERN  _kCurrentCursor_pos
    EXTERN  _kScratchSpace_8Bit
    EXTERN  SPACE
    EXTERN  CRAM_startAddressPerLineLookUpTable
    EXTERN  VRAM_startAddressPerLineLookUpTable

; ****************************************************************************

initScreen:
                LD      HL, $0000
                LD      (_kCurrentVideoOffset), HL
                ;
                RET

; ****************************************************************************

clearScreen:
                PUSH    BC                          ; Save BC, DE and HL registers
                PUSH    DE
                PUSH    HL
                ; Clear video RAM
                LD      HL, _kVRAMStartAddress
                LD      DE, _kVRAMStartAddress + 1
                LD      BC, _kVRAMSize - 1          ; -1 because 1 position is filled in by the next instruction already
                LD      (HL), $00
                LDIR
                ; Clear color RAM
                LD      A, (_kFontBgColor)
                LD      HL, _kCRAMStartAddress
                LD      DE, _kCRAMStartAddress + 1
                LD      BC, _kCRAMSize - 1          ; -1 because 1 position ois filled in by the next instruction already
                LD      (HL), A                     ; Fill video memory with the selected character color
                LDIR                                ; Fill memory
                ; Restore registers
                POP     HL
                POP     DE
                POP     BC
                RET

; ****************************************************************************

clearScreenFromCursor:
                CALL    clearFromCursorPosTillEndOfLine     ; Clear the current line starting from cursor position
                ;
                LD      HL, _kTerminalLines                 ; HL points to location where we store the height of the terminal
                LD      A, (_kCursorPosV)                   ; Fetch current V cursor position in A ([0..24 range])
                INC     A                                   ; Compensate for range [1..25] instead of [0..24] as in _kCursorPosV
                SUB     A, (HL)                             ; Subtract height of terminal with current V cursor position
                CP      A, $00                              ; Are there still lines to be cleared?
                RET     Z                                   ; No? Then return
                ;
                LD      A, (_kCursorPosV)                   ; Load current V cursor position
                INC     A                                   ; Increment to the next line
                LD      (_kScratchSpace_8Bit), A            ; Store in scratch space to be used as a counter
                ;
clearScreenFromCursorLoop:
                ; We now clear the lines below the cursor by using the clearLine subroutine.
                ;   BC has to contain the CRAM start address of the line to be cleared
                ;   DE has to contain the VRAM start address of the line to be cleared
                LD      A, (_kScratchSpace_8Bit)            ; Get value of loop counter in A
                LD      D, $00                              ; Store this in DE (which will be used as an index in the lookup tables)
                LD      E, A
                ;
                LD      HL, VRAM_startAddressPerLineLookUpTable ; Start of VRAM lookup table in HL
                ADD     HL, DE                              ; Add index to HL. Twice because we work with 16 bit addresses
                ADD     HL, DE
                LD      E, (HL)                             ; Store VRAM start address in DE
                INC     HL
                LD      D, (HL)
                ;
                CALL    clearLine                           ; Clear the line pointed to by BC and DE
                ;
                LD      A, (_kScratchSpace_8Bit)            ; Load loop counter in A
                INC     A                                   ; Increment loop counter
                LD      (_kScratchSpace_8Bit), A            ; Store back in scratch space
                LD      HL, _kTerminalLines                 ; Load height of terminal in HL
                SUB     A, (HL)                             ; Subtract from A
                CP      A, $00                              ; Are we done?
                JR      NZ, clearScreenFromCursorLoop       ; If not, repeat loop again
                ;
                RET
                
; ****************************************************************************

clearScreenTillCursor:
                CALL    clearFromBeginOfLineToCursorPos
                ;
                LD      A, (_kCursorPosV)                   ; Fetch current V cursor position in A ([0..24 range])
                CP      A, $00                              ; Are we on the first line?
                RET     Z                                   ; Yes? Then return
                ;
                LD      A, (_kCursorPosV)                   ; Load current V cursor position
                DEC     A                                   ; Decrement to the previous line
                LD      (_kScratchSpace_8Bit), A            ; Store in scratch space to be used as a counter
                ;
clearScreenTillCursorLoop:
                ; We now clear the lines above the cursor by using the clearLine subroutine.
                ;   BC has to contain the CRAM start address of the line to be cleared
                ;   DE has to contain the VRAM start address of the line to be cleared
                LD      A, (_kScratchSpace_8Bit)            ; Get value of loop counter in A
                LD      D, $00                              ; Store this in DE (which will be used as an index in the lookup tables)
                LD      E, A
                ;
                LD      HL, VRAM_startAddressPerLineLookUpTable ; Start of VRAM lookup table in HL
                ADD     HL, DE                              ; Add index to HL. Twice because we work with 16 bit addresses
                ADD     HL, DE
                LD      E, (HL)                             ; Store VRAM start address in DE
                INC     HL
                LD      D, (HL)
                ;
                CALL    clearLine                           ; Clear the line pointed to by BC and DE
                ;
                LD      A, (_kScratchSpace_8Bit)            ; Load loop counter in A
                DEC     A                                   ; Decrement loop counter
                LD      (_kScratchSpace_8Bit), A            ; Store back in scratch space
                CP      A, $FF                              ; Are we done? (check for -1 to be sure we clear line 0 as well)
                JR      NZ, clearScreenTillCursorLoop       ; If not, repeat loop again
                ;
                RET
                
; ****************************************************************************

clearCursorHPosition:
                LD      A, (_kCursorPosH)
                LD      D, $00
                LD      E, A
                OR      A                                   ; Clear carry flag
                LD      HL, (_kCurrentVideoOffset)          ; Get current video offset in HL
                SBC     HL, DE                              ; Subtract horizontal cursor position
                LD      A, H                                ; And roll over when we reach offset 4096 since the 6845
                AND     A, _kVCRAMBitMask                   ; addresses 8K of video RAM
                LD      H, A                                
                LD      (_kCurrentVideoOffset), HL          ; Store new video offset value
                ;
                LD      HL, (_kCurrentCursor_pos)           ; Get current cursor position
                OR      A                                   ; Clear carry flag
                SBC     HL, DE                              ; Subtract horizontal cursor position
                LD      (_kCurrentCursor_pos), HL           ; Store new cursor position value
                ;
                LD      A, 0            
                LD      (_kCursorPosH), A                   ; Set horizontal cursor position to 0
                ;
                CALL    update6845CursorRegisters
                ;
                RET

; ****************************************************************************

clearCursorVPosition:
                LD      A, (_kCursorPosV)                   ; Is current V cursor position 0?
                CP      A, $00
                JR      Z, clearCursorVPositionExit         ; If so, then exit
                ;
                CALL    decrementVCursor                    ; Decrement V cursor position by 1
                JR      clearCursorVPosition                ; Try again
                ;
clearCursorVPositionExit:
                RET

; ****************************************************************************

clearCursorPosition:
_clearCursorPosition:
                CALL    clearCursorHPosition
                CALL    clearCursorVPosition
                RET

; ****************************************************************************

incrementHCursor:
_incrementHCursor:
                LD      HL, _kCursorPosH
                LD      A, _kTerminalWidth                  ; Load terminal width in A (always 80 - 1 (because we count from 0))
                DEC     A
                SUB     A, (HL)                             ; Subtract current H current position [0..79] from A
                JR      Z, incrementHCursor_overflow        ; If we're at the end of the line don't increment the H cursor
                ;
                INC     (HL)                                ; Increment current H cursor position
                LD      HL, (_kCurrentVideoOffset)          ; Increment offset in Video RAM
                INC     HL
                LD      A, H                                ; But roll over when we reach offset 4096 since the 6845
                AND     A, _kVCRAMBitMask                   ; addresses 8K of video RAM
                LD      H, A                                
                LD      (_kCurrentVideoOffset), HL
                ;
                LD      HL, (_kCurrentCursor_pos)           ; Increment current cursor position
                INC     HL
                LD      (_kCurrentCursor_pos), HL
                ;
incrementHCursor_exit:
                CALL    update6845CursorRegisters
                RET
                ;
incrementHCursor_overflow:
                CALL    clearCursorHPosition                ; Move cursor to completely left
                CALL    incrementVCursorWithScrollUp        ; Move to next line, scroll up when needed.
                JP      incrementHCursor_exit


; ****************************************************************************

decrementHCursor:
                LD      HL, _kCursorPosH                    ; Point HL to current H position storage
                LD      A, (HL)                             ; Load current H cursor position in A
                CP      A, $00                              ; Compare with 0
                JP      Z, decrementHCursor_exit            ; If already 0, then exit
                ;
                DEC     (HL)                                ; Decrement current H cursor
                LD      HL, (_kCurrentVideoOffset)          ; Decrement offset in Video RAM
                DEC     HL
                LD      A, H                                ; But roll over when we reach offset 4096 since the 6845
                AND     A, _kVCRAMBitMask                   ; addresses 8K of video RAM
                LD      H, A                                
                LD      (_kCurrentVideoOffset), HL
                ;
                LD      HL, (_kCurrentCursor_pos)           ; Decrement current cursor position
                DEC     HL
                LD      (_kCurrentCursor_pos), HL
                ;
decrementHCursor_exit:
                CALL    update6845CursorRegisters
                RET

; ****************************************************************************

incrementVCursor:
_incrementVCursor:
                LD      HL, _kCursorPosV                    ; Load current V cursor position
                LD      A, (_kTerminalLines)                ; Load current terminal lines in A (e.g. 25)
                DEC     A                                   ; Decrement H position with 1 (e.g. 24)
                SUB     A, (HL)                             ; Subtract current H current position [0..24] from A
                JP      Z, incrementVCursor_exit            ; If we're at the end of the line don't increment the V cursor
                ;
                INC     (HL)                                ; Increment current vertical cursor position
                LD      DE, _kTerminalWidth                 ; Store terminal width (== 80) in DE
                LD      HL, (_kCurrentVideoOffset)          ; Load current video offset in HL
                ADD     HL, DE                              ; Add a new line (== 80) to it
                LD      A, H                                ; But roll over when we reach offset 4096 since the 6845
                AND     A, _kVCRAMBitMask                   ; addresses 8K of video RAM
                LD      H, A                                
                LD      (_kCurrentVideoOffset), HL          ; Store new video offset
                LD      HL, (_kCurrentCursor_pos)           ; Load current cursor position in HL
                ADD     HL, DE                              ; Add a new line (== 80) to it
                LD      (_kCurrentCursor_pos), HL           ; Store new current cursor position
                ;
incrementVCursor_exit:
                CALL    update6845CursorRegisters
                RET

; ****************************************************************************

incrementVCursorWithScrollUp:
                LD      HL, _kCursorPosV                    ; Load current V cursor position
                LD      A, (_kTerminalLines)                ; Load current terminal lines in A (e.g. 25)
                DEC     A                                   ; Decrement V position with 1 (e.g. 24)
                SUB     A, (HL)                             ; Subtract current H current position [0..24] from A
                JR      Z, incrementVCursorWithScrollUp_1   ; If we are at the bottom, then perform a scrollup
                ;
                CALL    incrementVCursor                    ; Simple V cursor position increment
                RET 
                ;
incrementVCursorWithScrollUp_1:
                CALL    scrollUp                            ; Scroll display down
                RET

; ****************************************************************************

decrementVCursor:
                LD      HL, _kCursorPosV                    ; Point HL to current V position storage
                LD      A, (HL)                             ; Load current V cursor position in A
                CP      A, $00                              ; Compare with 0
                JP      Z, decrementVCursor_exit            ; If already at the top then exit
                DEC     (HL)                                ; Decrement current H cursor
                ;
                LD      DE, _kTerminalWidth                 ; Store terminal width (== 80) in DE
                LD      HL, (_kCurrentVideoOffset)          ; Load current video offset in HL
                SBC     HL, DE                              ; Subtract a new line (== 80) to it
                LD      A, H                                ; But roll over when we reach offset 4096 since the 6845
                AND     A, _kVCRAMBitMask                   ; addresses 8K of video RAM
                LD      H, A                                
                LD      (_kCurrentVideoOffset), HL          ; Store new video offset
                LD      HL, (_kCurrentCursor_pos)           ; Load current cursor position in HL
                SBC     HL, DE                              ; Subtract a new line (== 80) to it
                LD      (_kCurrentCursor_pos), HL           ; Store new current cursor position
                ;
decrementVCursor_exit:
                CALL    update6845CursorRegisters
                RET

; ****************************************************************************

decrementVCursorWithScrollDown:
                LD      HL, _kCursorPosV                    ; Point HL to current V position storage
                LD      A, (HL)                             ; Load current V cursor position in A
                CP      A, $00                              ; Compare with 0
                JP      Z, decrementVCursorWithScrollDown_1 ; If already at the top then scroll up
                ;
                CALL    decrementVCursor
                RET
                ;
decrementVCursorWithScrollDown_1:
                CALL    scrollDown
                RET

; ****************************************************************************

clearFirstLine:
                LD      DE, _kVRAMStartAddress
                CALL    clearLine
                RET

; ****************************************************************************

clearLastLine:
                LD      A, (_kTerminalLines)
                DEC     A
                LD      D, $00                              ; Store this in DE (which will be used as an index in the lookup tables)
                LD      E, A
                ;
                LD      HL, VRAM_startAddressPerLineLookUpTable ; Start of VRAM lookup table in HL
                ADD     HL, DE                              ; Add index to HL. Twice because we work with 16 bit addresses
                ADD     HL, DE
                LD      E, (HL)                             ; Store VRAM start address (of the last line) in DE
                INC     HL
                LD      D, (HL)
                ;
                CALL    clearLine
                RET

; ****************************************************************************

clearCurrentLine:
                LD      A, (_kCursorPosV)
                LD      D, $00                              ; Store this in DE (which will be used as an index in the lookup tables)
                LD      E, A
                ;
                LD      HL, VRAM_startAddressPerLineLookUpTable ; Start of VRAM lookup table in HL
                ADD     HL, DE                              ; Add index to HL. Twice because we work with 16 bit addresses
                ADD     HL, DE
                LD      E, (HL)                             ; Store VRAM start address (of the last line) in DE
                INC     HL
                LD      D, (HL)
                ;
                CALL    clearLine
                RET

; ****************************************************************************

clearLine:   
                ; DE contains start address of the line to clear in VRAM as if the 6845 start address would be -1 (initial state).
                LD      HL, (k6845StartAddress)             ; Load the 6845 start address to calculate the real start address.
                ;
                ADD     HL, DE                              ; HL now contains VRAM start address
                INC     HL                                  ; Compensate for -1 offset in k6845StartAddress
                LD      B, _kTerminalWidth                  ; Load counter (terminal width == 80 characters)
clearLineVRAM_Loop:
                LD      A, H                                ; Make sure we have an address within the VRAM address
                OR      A, $E0                              ; range: 0xE000 - 0xFFFF
                LD      H, A                                
                LD      (HL), SPACE                         ; Write a SPACE
                INC     HL                                  ; Increment VRAM address
                DJNZ    clearLineVRAM_Loop
                ;
                RET             

; ****************************************************************************

scrollUp:
                LD      DE, _kTerminalWidth
                LD      HL, (k6845StartAddress)             ; HL contains k6845StartAddress
                ADD     HL, DE                              ; Increment k6845StartAddress with one line
                LD      (k6845StartAddress), HL             ; Update k6845StartAddress
                ;
                LD      HL, (_kCurrentVideoOffset)          ; Load current video offset in HL
                ADD     HL, DE                              ; Add a new line (== 80) to it
                LD      A, H                                ; But roll over when we reach offset 4096 since the 6845
                AND     A, _kVCRAMBitMask                   ; addresses 8K of video RAM
                LD      H, A                                
                LD      (_kCurrentVideoOffset), HL          ; Store new video offset
                LD      HL, (_kCurrentCursor_pos)           ; Load current cursor position in HL
                ADD     HL, DE                              ; Add a new line (== 80) to it
                LD      (_kCurrentCursor_pos), HL           ; Store new current cursor position
                ;
                CALL    clearLastLine
                ;
                CALL    update6845StartAddressRegisters
                CALL    update6845CursorRegisters
                ;
                RET

; ****************************************************************************

scrollDown:
                LD      DE, _kTerminalWidth
                LD      HL, (k6845StartAddress)             ; HL contains k6845StartAddress
                OR      A                                   ; Clear carry flag
                SBC     HL, DE                              ; Decrement k6845StartAddress with one line
                LD      (k6845StartAddress), HL             ; Update k6845StartAddress
                ;
                LD      HL, (_kCurrentVideoOffset)          ; Load current video offset in HL
                OR      A                                   ; Clear carry flag
                SBC     HL, DE                              ; Subtract a new line (== 80) to it
                LD      A, H                                ; But roll over when we reach offset 4096 since the 6845
                AND     A, _kVCRAMBitMask                   ; addresses 8K of video RAM
                LD      H, A                                
                LD      (_kCurrentVideoOffset), HL          ; Store new video offset
                LD      HL, (_kCurrentCursor_pos)           ; Load current cursor position in HL
                OR      A                                   ; Clear carry flag
                SBC     HL, DE                              ; Subtract a new line (== 80) to it
                LD      (_kCurrentCursor_pos), HL           ; Store new current cursor position
                ;
                CALL    clearFirstLine
                ;
                CALL    update6845StartAddressRegisters
                CALL    update6845CursorRegisters
                ;
                RET

; ****************************************************************************

clearFromCursorPosTillEndOfLine:
                LD      A, (_kCursorPosH)
                LD      (_kScratchSpace_8Bit), A            ; Use _kScratchSpace_8Bit to store the loop counter
                LD      BC, (_kCurrentVideoOffset)          ; Use BC as a local video offset
clearFromCursorPosTillEndOfLineLoop:
                LD      HL, BC                              ; Store local video offset in HL
                LD      DE, _kVRAMStartAddress              ; Store VRAM start address in DE
                ADD     HL, DE                              ; Add start address of VRAM to it.
                ;
                LD      A, SPACE                            ; Load SPACE symbol in A
                LD      (HL), A                             ; And store it into VRAM
                ;
                LD      HL, BC                              ; Store local video offset in HL
                LD      DE, _kCRAMStartAddress              ; Store VRAM start address in DE
                ADD     HL, DE                              ; Add start address of CRAM to it.
                ;
                LD      A, (_kFontBgColor)                  ; Load current font color in A                 
                LD      (HL), A                             ; And store it in CRAM
                ;
                INC     BC                                  ; Increment local video offset
                LD      A, B                                ; And roll over when we reach offset 4096 since the 6845
                AND     A, _kVCRAMBitMask                   ; addresses 8K of video RAM
                LD      B, A                                
                ;
                LD      HL, _kScratchSpace_8Bit
                INC     (HL)                                ; Increment loop counter
                LD      A, (HL)                             ; Get loop counter in A
                CP      _kTerminalWidth                     ; Did we reach the end of the line?
                JR      NZ, clearFromCursorPosTillEndOfLineLoop ; If no, then loop once again.
                ;
                RET

; ****************************************************************************

clearFromBeginOfLineToCursorPos:
                LD      A, (_kCursorPosH)
                INC     A
                LD      (_kScratchSpace_8Bit), A            ; Use _kScratchSpace_8Bit to store the loop counter
                LD      BC, (_kCurrentVideoOffset)          ; Use BC as a local video offset
clearFromBeginOfLineToCursorPosLoop:
                LD      HL, BC                              ; Store local video offset in HL
                LD      DE, _kVRAMStartAddress              ; Store VRAM start address in DE
                ADD     HL, DE                              ; Add start address of VRAM to it.
                ;
                LD      A, SPACE                            ; Load SPACE symbol in A
                LD      (HL), A                             ; And store it into VRAM
                ;
                LD      HL, BC                              ; Store local video offset in HL
                LD      DE, _kCRAMStartAddress              ; Store VRAM start address in DE
                ADD     HL, DE                              ; Add start address of CRAM to it.
                ;
                LD      A, (_kFontBgColor)                  ; Load current font color in A                 
                LD      (HL), A                             ; And store it in CRAM
                ;
                DEC     BC                                  ; Decrement local video offset
                LD      A, B                                ; And roll over when we reach offset 4096 since the 6845
                AND     A, _kVCRAMBitMask                   ; addresses 8K of video RAM
                LD      B, A                                
                ;
                LD      HL, _kScratchSpace_8Bit
                DEC     (HL)                                ; Decrement loop counter
                LD      A, (HL)                             ; Get loop counter in A
                CP      A, $00                              ; Did we reach the beginning of the line?
                JR      NZ, clearFromBeginOfLineToCursorPosLoop ; If no, then loop once again.
                ;
                RET

; ****************************************************************************

copyLineDown:
                ; On entry:
                ;   DE contains "from" CRAM start address 
                ;   HL contains "from" VRAM start address 
                PUSH    DE                                  ; Save DE (CRAM start address) for later use on the stack
                ;
                ; Moving a VRAM line
                LD      BC, _kTerminalWidth                 ; Load BC with the terminal width (80)  
                LD      DE, HL                              ; Store VRAM start address in DE
                ADD     HL, BC                              ; Add a line to HL. HL now contains VRAM "to" address offset
                ;
copyLineDownLoop_VRAM:
                LD      A, H                                ; Make sure we have a valid VRAM to address [0xE000 .. 0xEFFF] in HL
                OR      A, $E0
                LD      H, A
                ;
                LD      A, D                                ; Make sure we have a valid VRAM from address [0xE000 .. 0xEFFF] in DE
                OR      A, $E0
                LD      D, A
                ;
                LD      A, (DE)                             ; Move character to the next line
                LD      (HL), A
                ;
                INC     DE                                  ; Increment from address
                INC     HL                                  ; Increment to address
                DEC     BC                                  ; Decrement loop counter
                LD      A, C                                ; Load loop counter in A
                CP      A, $00                              ; Are we done?
                JR      NZ, copyLineDownLoop_VRAM           ; If not, then copy the next character
                ;
                ; Now move a CRAM line
                POP     HL                                  ; Restore CRAM start address in HL
                LD      BC, _kTerminalWidth                 ; Load BC with the terminal width (80)  
                LD      DE, HL                              ; Store CRAM start address in DE
                ADD     HL, BC                              ; Add a line to HL. HL now contains CRAM "to" address offset
                
copyLineDownLoop_CRAM:
                LD      A, H                                ; Make sure we have a valid CRAM to address [0xC000 .. 0xCFFF] in HL
                AND     A, $DF                              ; and roll over if so
                LD      H, A
                ;
                LD      A, D                                ; Make sure we have a valid CRAM from address [0xC000 .. 0xCFFF] in DE
                AND     A, $DF                              ; and roll over if so
                LD      D, A
                ;
                LD      A, (DE)                             ; Move color attributes to the next line
                LD      (HL), A
                ;
                INC     DE                                  ; Increment from address
                INC     HL                                  ; Increment to address
                DEC     BC                                  ; Decrement loop counter
                LD      A, C                                ; Load loop counter in A
                CP      A, $00                              ; Are we done?
                JR      NZ, copyLineDownLoop_CRAM           ; If not, then copy the next character
                ;
                RET

; ****************************************************************************

copyLineUp:
                ; On entry:
                ;   DE contains "from" CRAM start address 
                ;   HL contains "from" VRAM start address 
                PUSH    DE                                  ; Save DE (CRAM start address) for later use on the stack
                ;
                ; Moving a VRAM line
                OR      A                                   ; Clear carry flag
                LD      BC, _kTerminalWidth                 ; Load BC with the terminal width (80)  
                LD      DE, HL                              ; Store VRAM start address in DE                   
                SBC     HL, BC                              ; Subtract a line to HL. HL now contains VRAM "to" address offset
                ;
copyLineUpLoop_VRAM:
                LD      A, H                                ; Make sure we have a valid VRAM to address [0xE000 .. 0xEFFF] in HL
                OR      A, $E0
                LD      H, A
                ;
                LD      A, D                                ; Make sure we have a valid VRAM from address [0xE000 .. 0xEFFF] in DE
                OR      A, $E0
                LD      D, A
                ;
                LD      A, (DE)                             ; Move character to the previous line
                LD      (HL), A
                ;
                INC     DE                                  ; Increment from address
                INC     HL                                  ; Increment to address
                DEC     BC                                  ; Decrement loop counter
                LD      A, C                                ; Load loop counter in A
                CP      A, $00                              ; Are we done?
                JR      NZ, copyLineUpLoop_VRAM             ; If not, then copy the next character
                ;
                ; Now move a CRAM line
                OR      A                                   ; Clear carry flag
                POP     HL                                  ; Restore CRAM start address in HL
                LD      BC, _kTerminalWidth                 ; Load BC with the terminal width (80)  
                LD      DE, HL                              ; Store CRAM start address in DE
                SBC     HL, BC                              ; Subtract a line to HL. HL now contains CRAM "to" address offset
                
copyLineUpLoop_CRAM:
                LD      A, H                                ; Make sure we have a valid CRAM to address [0xC000 .. 0xCFFF] in HL
                AND     A, $DF                              ; and roll over if so
                LD      H, A
                ;
                LD      A, D                                ; Make sure we have a valid CRAM from address [0xC000 .. 0xCFFF] in DE
                AND     A, $DF                              ; and roll over if so
                LD      D, A
                ;
                LD      A, (DE)                             ; Move color attributes to the previous line
                LD      (HL), A
                ;
                INC     DE                                  ; Increment from address
                INC     HL                                  ; Increment to address
                DEC     BC                                  ; Decrement loop counter
                LD      A, C                                ; Load loop counter in A
                CP      A, $00                              ; Are we done?
                JR      NZ, copyLineUpLoop_CRAM             ; If not, then copy the next character
                ;
                RET

; ****************************************************************************

insertLine:
                ; Are we on the first line?
                LD      A, (_kCursorPosV)                   ; Load current V cursor position
                CP      A, $00                              ; Are we at the top of the screen?
                JP      Z, scrollDown                       ; If yes, then we can do a simple, fast scroll down.
                ; Are we on the last line?-
                LD      HL, _kTerminalLines                 ; HL points to the height of the terminal, range [1..25]
                INC     A                                   ; convert V pos range [0..24] to [1..25]
                CP      A, (HL)                             ; Are we on the last line?
                JR      Z, insertLineExit                   ; If yes then only clear the last (== current) line
                ; Now the heavy work cases
                LD      A, (_kTerminalLines)                ; range [1..25]
                DEC     A                                   ; range [0..24]
                DEC     A                                   ; first line to be moved down (line 23 -> line 24)
                LD      (_kScratchSpace_8Bit), A            ; _kScratchSpace_8Bit now points to the lowest line to be moved down first
insertLineLoop:
                LD      HL, _kCursorPosV                    ; HL points to V cursor position
                LD      A, (_kScratchSpace_8Bit)            ; Load line number to be moved down
                CP      A, (HL)                             ; Compare current V cursor position with line to be moved down
                JR      C, insertLineExit                   ; Has it become negative? If so, stop moving lines
                ;
                LD      HL, _kScratchSpace_8Bit             ; HL points to loop counter
                DEC     (HL)                                ; decrement loop counter
                ;
                LD      D, $00                              ; Store line to be moved down in DE
                LD      E, A
                PUSH    DE                                  ; Store value of DE (offset in the lookup tables) on the stack for later re-use
                ; Calculate start address to move down CRAM lines
                LD      HL, CRAM_startAddressPerLineLookUpTable ; Start of CRAM lookup table in HL
                ADD     HL, DE                              ; Add index (DE) to HL. Twice because we work with 16 bit addresses
                ADD     HL, DE
                LD      E, (HL)                             ; Store VRAM start address in DE
                INC     HL
                LD      D, (HL)
                ;
                LD      HL, (k6845StartAddress)             ; HL contains k6845StartAddress
                LD      A, H                                ; But roll over when we reach offset 4096 since the 6845
                AND     A, _kVCRAMBitMask                   ; addresses 8K of video RAM
                LD      H, A                                
                ADD     HL, DE                              ; Add k6845StartAddress to HL to point to the real beginning of the CRAM line to be moved
                ;
                POP     DE                                  ; Restore DE (to be used as the offset in the VRAM_startAddressPerLineLookUpTable)
                PUSH    HL                                  ; Temporary store CRAM start address on the stack
                ; Calculate start address to move down VRAM lines

                LD      HL, VRAM_startAddressPerLineLookUpTable ; Start of VRAM lookup table in HL
                ADD     HL, DE                              ; Add index (DE) to HL. Twice because we work with 16 bit addresses
                ADD     HL, DE
                LD      E, (HL)                             ; Store VRAM start address in DE
                INC     HL
                LD      D, (HL)
                ;
                LD      HL, (k6845StartAddress)             ; HL contains k6845StartAddress
                LD      A, H                                ; But roll over when we reach offset 4096 since the 6845
                AND     A, _kVCRAMBitMask                   ; addresses 8K of video RAM
                LD      H, A                                
                ADD     HL, DE                              ; Add k6845StartAddress to HL to point to the real beginning of the VRAM line to be moved
                POP     DE                                  ; DE points to the real beginning of the CRAM line to be moved
                CALL    copyLineDown                        ; DE -> CRAM; HL -> VRAM
                JR      insertLineLoop
                ;
insertLineExit:
                CALL    clearCurrentLine
                RET

; ****************************************************************************

deleteLine:
                ; Are we on the first line?
                LD      A, (_kCursorPosV)                   ; Load current V cursor position
                CP      A, $00                              ; Are we at the top of the screen?
                JP      Z, scrollUp                         ; If yes, then we can do a simple, fast scroll up.
                ; Are we on the last line?-
                INC     A                                   ; convert V pos range [0..24] to [1..25]
                CP      A, (HL)                             ; Are we on the last line?
                JR      Z, deleteLineExit                   ; If yes then only clear the last (== current) line
                ; Now the heavy work cases
                LD      A, (_kCursorPosV)                   ; Get current V cursor position (== to line)
                INC     A                                   ; A == from line
                LD      (_kScratchSpace_8Bit), A            ; _kScratchSpace_8Bit now points to the highest line to be moved up first
deleteLineLoop:
                LD      HL, _kTerminalLines                 ; HL points to the terminal height
                LD      A, (_kScratchSpace_8Bit)            ; Load line number to be moved down
                CP      A, (HL)                             ; Compare current V cursor position with line to be moved down
                JR      Z, deleteLineExit                   ; Ar we at the last line? If so, stop moving lines
                ;
                LD      HL, _kScratchSpace_8Bit             ; HL points to loop counter
                INC     (HL)                                ; increment loop counter
                ;
                LD      D, $00                              ; Store line to be moved down in DE
                LD      E, A
                PUSH    DE                                  ; Store value of DE (offset in the lookup tables) on the stack for later re-use
                ; Calculate start address to move down CRAM lines
                LD      HL, CRAM_startAddressPerLineLookUpTable ; Start of CRAM lookup table in HL
                ADD     HL, DE                              ; Add index (DE) to HL. Twice because we work with 16 bit addresses
                ADD     HL, DE
                LD      E, (HL)                             ; Store VRAM start address in DE
                INC     HL
                LD      D, (HL)
                ;
                LD      HL, (k6845StartAddress)             ; HL contains k6845StartAddress
                LD      A, H                                ; But roll over when we reach offset 4096 since the 6845
                AND     A, _kVCRAMBitMask                   ; addresses 8K of video RAM
                LD      H, A                                
                ADD     HL, DE                              ; Add k6845StartAddress to HL to point to the real beginning of the CRAM line to be moved
                ;
                POP     DE                                  ; Restore DE (to be used as the offset in the VRAM_startAddressPerLineLookUpTable)
                PUSH    HL                                  ; Temporary store CRAM start address on the stack
                ; Calculate start address to move down VRAM lines

                LD      HL, VRAM_startAddressPerLineLookUpTable ; Start of VRAM lookup table in HL
                ADD     HL, DE                              ; Add index (DE) to HL. Twice because we work with 16 bit addresses
                ADD     HL, DE
                LD      E, (HL)                             ; Store VRAM start address in DE
                INC     HL
                LD      D, (HL)
                ;
                LD      HL, (k6845StartAddress)             ; HL contains k6845StartAddress
                LD      A, H                                ; But roll over when we reach offset 4096 since the 6845
                AND     A, _kVCRAMBitMask                   ; addresses 8K of video RAM
                LD      H, A                                
                ADD     HL, DE                              ; Add k6845StartAddress to HL to point to the real beginning of the VRAM line to be moved
                POP     DE                                  ; DE points to the real beginning of the CRAM line to be moved
                CALL    copyLineUp                          ; DE -> CRAM; HL -> VRAM
                JR      deleteLineLoop
                ;
deleteLineExit:
                CALL    clearLastLine
                RET

; ****************************************************************************
