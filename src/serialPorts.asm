; ****************************************************************************
; Code for the Z180 UART's
; ****************************************************************************

    INCLUDE "Z180_registers.asm"

    PUBLIC  outCharA
    PUBLIC  _outCharA
    ; PUBLIC  _outStringA
    ; PUBLIC  _charAvailableA
    ; PUBLIC  _inCharA

    PUBLIC  charAvailableB
    PUBLIC  _charAvailableB
    PUBLIC  inCharB
    PUBLIC  _inCharB

    ; PUBLIC  _Z180RxByteA
    ; PUBLIC  _Z180TxByteA
    ; PUBLIC  _Z180RxByteB
    ; PUBLIC  _Z180TxByteB

    EXTERN  _kZ180RegBase

; ****************************************************************************

SECTION code_crt_init

; ****************************************************************************

outCharA:
_outCharA:
                CALL    _Z180TxByteA
                JP      Z, outCharA
                RET 


; _outStringA:
;                 LD      A, (HL)
;                 CP      A, $00
;                 RET     Z
;                 CALL    _outCharA
;                 INC     HL
;                 JP      _outStringA

; _charAvailableA:
;                 IN0     A, (STAT0)                          ; Read serial port status register
;                 AND     A, $80                              ; Receive register full?
;                 LD      L, A
;                 RET

; _inCharA:
;                 CALL    _Z180RxByteA
;                 JP      Z, _inCharA
;                 LD      L, A
;                 RET

; ****************************************************************************

charAvailableB:
_charAvailableB:
                IN0     A, (STAT1)                          ; Read serial port status register
                AND     A, $80                              ; Receive register full?
                LD      L, A
                RET

inCharB:
_inCharB:
                CALL    _Z180RxByteB
                JP      Z, inCharB
                LD      L, A
                RET

; ****************************************************************************
; **  Z180's internal serial port 0 drivers (port A)
; ****************************************************************************

; Receive byte
;   On entry: No parameters required
;   On exit:  A = Character input from the device
;             NZ flagged if character input
;             BC DE HL IX IY I AF' BC' DE' HL' preserved
_Z180RxByteA:
                IN0     A, (STAT0)          ; Read serial port status register
                BIT     ST_RDRF, A          ; Receive register full?
                RET     Z                   ; Return Z as no character available
                IN0     A, (RDR0)           ; Read data byte
                RET

; Transmit byte
;   On entry: A = Character to be output to the device
;   On exit:  If character output successful (eg. device was ready)
;                   NZ flagged and A != 0
;             If character output failed (eg. device busy)
;                   Z flagged and A = Character to output
;             BC DE HL IX IY I AF' BC' DE' HL' preserved
_Z180TxByteA:
                PUSH    BC                  ; Preserve BC
                IN0     B, (STAT0)          ; Read serial port status register
                BIT     ST_TDRE, B          ; Transmit register empty?
                POP     BC                  ; Restore BC
                RET     Z                   ; Return Z as character not output
                OUT0    (TDR0), A           ; Write byte to serial port
                OR      0xFF                ; Return success A=0xFF and NZ flagged
                RET

; ****************************************************************************
; **  Z180's internal serial port 1 drivers (port B)
; ****************************************************************************

; Receive byte
;   On entry: No parameters required
;   On exit:  A = Character input from the device
;             NZ flagged if character input
;             BC DE HL IX IY I AF' BC' DE' HL' preserved
_Z180RxByteB:
                IN0     A, (STAT1)          ; Read serial port status register
                BIT     ST_RDRF, A          ; Receive register full?
                RET     Z                   ; Return Z as no character available
                IN0     A, (RDR1)           ; Read data byte
                RET

; Transmit byte
;   On entry: A = Character to be output to the device
;   On exit:  If character output successful (eg. device was ready)
;                   NZ flagged and A != 0
;             If character output failed (eg. device busy)
;                   Z flagged and A = Character to output
;             BC DE HL IX IY I AF' BC' DE' HL' preserved
_Z180TxByteB:
                PUSH    BC                  ; Preserve BC
                IN0     B, (STAT1)          ; Read serial port status register
                BIT     ST_TDRE, B          ; Transmit register empty?
                POP     BC                  ; Restore BC
                RET     Z                   ; Return Z as character not output
                OUT0    (TDR1), A           ; Write byte to serial port
                OR      0xFF                ; Return success A=0xFF and NZ flagged
                RET
