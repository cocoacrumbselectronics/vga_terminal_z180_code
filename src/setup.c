#include <string.h>

#include "externs.h"
#include "colors.h"
#include "utils.h"


#define False                   0
#define True                    1


#define clearScreen             0x0C
#define escape                  0x1B


#define upperLeftCornerThin     ((unsigned char)(0xDA)) 
#define upperRightCornerThin    ((unsigned char)(0xBF))
#define lowerLeftCornerThin     ((unsigned char)(0xC0))
#define lowerRightCornerThin    ((unsigned char)(0xD9))
#define horizontalBarThin       ((unsigned char)(0xC4))
#define verticalBarThin         ((unsigned char)(0xB3))

#define upperLeftCornerThick    ((unsigned char)(0xC9)) 
#define upperRightCornerThick   ((unsigned char)(0xBB))
#define lowerLeftCornerThick    ((unsigned char)(0xC8))
#define lowerRightCornerThick   ((unsigned char)(0xBC))
#define horizontalBarThick      ((unsigned char)(0xCD))
#define verticalBarThick        ((unsigned char)(0xBA))

#define blockCharacter          ((unsigned char)(0xDB))

#define RIGHT                   0x04
#define UP                      0x05
#define TAB                     0x09
#define RETURN                  0x0D
#define LEFT                    0x13
#define DOWN                    0x18
#define ESCAPE                  0x1B
#define SPACE                   0x20

/* ************************************************************************* */

#define setupBackgroundColor    ((unsigned char)(bg_BLUE | fg_YELLOW))

/* ************************************************************************* */

#define nrOfMenuItems           6

const char menuItem_0[]         = "Baudrate";
const char menuItem_1[]         = "ColorSelect";
const char menuItem_2[]         = "ScreenSize";
const char menuItem_3[]         = "Font";
const char menuItem_4[]         = "Exit";
const char menuItem_5[]         = "Default Settings";

char *menuItem[nrOfMenuItems]   = { menuItem_0,
                                    menuItem_1,
                                    menuItem_2,
                                    menuItem_3,
                                    menuItem_4,
                                    menuItem_5 };

/* ************************************************************************* */

#define baudrateX                                    2
#define baudrateY                                    2
#define baudrateWidth                               30
#define baudrateHeight                              15
#define nrOfSupportedBaudrates                      10

const char baudrate____300[]                        = "   300 baud (8N1)";
const char baudrate____600[]                        = "   600 baud (8N1)";
const char baudrate___1200[]                        = "  1200 baud (8N1)";
const char baudrate___2400[]                        = "  2400 baud (8N1)";
const char baudrate___4800[]                        = "  4800 baud (8N1)";
const char baudrate___9600[]                        = "  9600 baud (8N1)";
const char baudrate__19200[]                        = " 19200 baud (8N1)";
const char baudrate__38400[]                        = " 38400 baud (8N1)";
const char baudrate__57600[]                        = " 57600 baud (8N1)";
const char baudrate_115200[]                        = "115200 baud (8N1)";

char *availableBaudrates[nrOfSupportedBaudrates]    = { baudrate____300, 
                                                        baudrate____600,
                                                        baudrate___1200,
                                                        baudrate___2400,
                                                        baudrate___4800,
                                                        baudrate___9600,
                                                        baudrate__19200,
                                                        baudrate__38400,
                                                        baudrate__57600,
                                                        baudrate_115200 };

/* ************************************************************************* */

#define colorsX                  2
#define colorsY                  2
#define colorsWidth             51
#define colorsHeight            14

/* ************************************************************************* */


#define screenSizeX                                      2
#define screenSizeY                                      2
#define screenSizeWidth                                 30
#define screenSizeHeight                                11
#define nrOfSupportedScreenSizes                         6

const char screenSize_24[]                              = "80 x 24 lines";
const char screenSize_25[]                              = "80 x 25 lines";
const char screenSize_30[]                              = "80 x 30 lines";
const char screenSize_43[]                              = "80 x 43 lines";
const char screenSize_51[]                              = "80 x 51 lines";
const char screenSize_60[]                              = "80 x 60 lines";

char *availableScreenSizes[nrOfSupportedScreenSizes]    = { screenSize_24, 
                                                            screenSize_25,
                                                            screenSize_30,
                                                            screenSize_43,
                                                            screenSize_51,
                                                            screenSize_60 };

/* ************************************************************************* */

#define fontSelectX                          2
#define fontSelectY                          2
#define fontSelectWidth                     36
#define fontSelectHeight                    21
#define nrOfSupportedFonts                  16

const char font__0[]                        = "Font  0 CP437 (height 16)";
const char font__1[]                        = "Font  1 CP437 (height  8)";
const char font__2[]                        = "Font  2 (unavailable)";
const char font__3[]                        = "Font  3 (unavailable)";
const char font__4[]                        = "Font  4 (unavailable)";
const char font__5[]                        = "Font  5 (unavailable)";
const char font__6[]                        = "Font  6 (unavailable)";
const char font__7[]                        = "Font  7 (unavailable)";
const char font__8[]                        = "Font  8 (unavailable)";
const char font__9[]                        = "Font  9 (unavailable)";
const char font_10[]                        = "Font 10 (unavailable)";
const char font_11[]                        = "Font 11 (unavailable)";
const char font_12[]                        = "Font 12 (unavailable)";
const char font_13[]                        = "Font 13 (unavailable)";
const char font_14[]                        = "Font 14 (unavailable)";
const char font_15[]                        = "Font 15 (unavailable)";

char *availableFonts[nrOfSupportedFonts]    = { font__0, 
                                                font__1,
                                                font__2,
                                                font__3,
                                                font__4,
                                                font__5,
                                                font__6,
                                                font__7,
                                                font__8,
                                                font__9,
                                                font_10,
                                                font_11,
                                                font_12,
                                                font_13,
                                                font_14,
                                                font_15 };

/* ************************************************************************* */

#define exitX           2
#define exitY           2
#define exitWidth       30
#define exitHeight      5

/* ************************************************************************* */

void positionCursor(int x, int y)
{
    int ctr = 0;

    clearCursorPosition();

    for (ctr=0; ctr<x; ctr++)
        incrementHCursor();
    
    for (ctr=0; ctr<y; ctr++)
        incrementVCursor();
} /* end positionCursor */


void drawThinSmallBoxWithColors(unsigned char x, 
                                unsigned char y, 
                                unsigned char boxColor)
{
    setColor(boxColor);

    positionCursor(x, y);
    printChar(upperLeftCornerThin);
    printChar(horizontalBarThin);
    printChar(upperRightCornerThin);

    positionCursor(x, y + 1);
    printChar(verticalBarThin);
    printChar(' ');
    printChar(verticalBarThin);

    positionCursor(x, y + 2);
    printChar(lowerLeftCornerThin);
    printChar(horizontalBarThin);
    printChar(lowerRightCornerThin);

    positionCursor(x + 1, y + 1);
} /* end drawThinSmallBoxWithColors */


void drawThickSmallBoxWithColors(unsigned char x, 
                                 unsigned char y, 
                                 unsigned char boxColor)
{
    setColor(boxColor);

    positionCursor(x, y);
    printChar(upperLeftCornerThick);
    printChar(horizontalBarThick);
    printChar(upperRightCornerThick);

    positionCursor(x, y + 1);
    printChar(verticalBarThick);
    printChar(' ');
    printChar(verticalBarThick);

    positionCursor(x, y + 2);
    printChar(lowerLeftCornerThick);
    printChar(horizontalBarThick);
    printChar(lowerRightCornerThick);

    positionCursor(x + 1, y + 1);
} /* end drawThickSmallBoxWithColors */


void drawBoxWithColors(unsigned char x, 
                       unsigned char y, 
                       unsigned char width, 
                       unsigned char height, 
                       unsigned char boxColor,
                       unsigned char titleColor,
                       unsigned char *title)
{
    int ctr = 0;
    int idx = 0;

    setColor(boxColor);

    positionCursor(x, y);
    printChar(upperLeftCornerThick);
    for (ctr=1; ctr<width; ctr++)
        printChar(horizontalBarThick);
    printChar(upperRightCornerThick);

    for (ctr = 1; ctr < height; ctr++)
    {
        positionCursor(x, y + ctr);
        printChar(verticalBarThick);
        for (idx=0; idx<=(width-2); idx++)
            printChar(' ');
        printChar(verticalBarThick);
    } /* end for */

    positionCursor(x, y + height);
    printChar(lowerLeftCornerThick);
    for (ctr=1; ctr<width; ctr++)
        printChar(horizontalBarThick);
    printChar(lowerRightCornerThick);

    setColor(titleColor);
    if (title != NULL)
    {
        if (strlen(title) < (width - 2))
        {
            positionCursor(x + 2, y);
            printString(title);
        } /* end if */
    } /* end if */

    positionCursor(x+2, y+1);
} /* end drawBoxWithColor */


void drawSelectLine(unsigned char   enabled,
                    unsigned char*  text,
                    unsigned int    paddingWidth)
{
    unsigned int    ctr = 0;

    setColor((unsigned char)(fg_BLACK + bg_BRIGHT_GREEN));
    printChar(' ');

    if (enabled == True)
    {
        printChar('[');
        setColor((unsigned char)(fg_RED + bg_BRIGHT_GREEN));
        printChar('X');
        setColor((unsigned char)(fg_BLACK + bg_BRIGHT_GREEN));
        printChar(']');
    }
    else
    {
        printString("[ ]");
    } /* end if */
    printChar(' ');
    printString(text);

    if (strlen(text) <= paddingWidth)
    {
        for (ctr=strlen(text); ctr<=paddingWidth; ctr++)
            printChar(' ');
    } /* end if */
} /* end drawSelectLine */


void drawCancelBox(unsigned int x,
                   unsigned int y)
{
    unsigned char   backupColor             = getCurrentColor();
    unsigned char   currentBackGroundColor  = backupColor & 0xF0;

    setColor(fg_BLACK + bg_RED);
    positionCursor(x, y);
    printString(" Cancel ");

    setColor(fg_BLACK + currentBackGroundColor);
    printChar('\xDC');
    positionCursor(x + 1, y + 1);
    printString("\xDF\xDF\xDF\xDF\xDF\xDF\xDF\xDF");

    setColor(backupColor);
} /* end drawCancelBox */


void drawOKBox(unsigned int x,
               unsigned int y)
{
    unsigned char   backupColor             = getCurrentColor();
    unsigned char   currentBackGroundColor  = backupColor & 0xF0;

    setColor(fg_BLACK + bg_GREEN);
    positionCursor(x, y);
    printString("   OK   ");

    setColor(fg_BLACK + currentBackGroundColor);
    printChar('\xDC');
    positionCursor(x + 1, y + 1);
    printString("\xDF\xDF\xDF\xDF\xDF\xDF\xDF\xDF");

    setColor(backupColor);
} /* end drawOKBox */


/* ************************************************************************* */


void positionBaudRateCursor(unsigned int xPos,
                            unsigned int yPos,
                            unsigned int currentlySelected)
{
    positionCursor(xPos + 5, yPos + 2 + currentlySelected);
} /* end positionBaudRateCursor */


void updateBaudRateSelection(unsigned int xPos,
                             unsigned int yPos,
                             unsigned int currentlySelected)
{
    unsigned int    ctr = 0;
    
    setColor((unsigned char)(fg_BLACK + bg_BRIGHT_GREEN));
    for (ctr = 0; ctr < nrOfSupportedBaudrates; ctr++)
    {
        positionCursor(xPos + 3, yPos + 2 + ctr);
        if (ctr == currentlySelected)
            drawSelectLine(True, availableBaudrates[ctr], 19);
        else
            drawSelectLine(False, availableBaudrates[ctr], 19);
    } /* end for */  
} /* end updateBaudRateSelection */


void baudrateSelectBoxDraw(unsigned int xPos,
                           unsigned int yPos,
                           unsigned int width,
                           unsigned int height,
                           unsigned int currentlySelected)
{
    unsigned int    ctr         = 0;
    unsigned char   backupColor = 0;

    drawBoxWithColors(xPos, 
                      yPos, 
                      width, 
                      height, 
                      (unsigned char)(fg_BRIGHT_WHITE | bg_BRIGHT_BLACK), 
                      (unsigned char)(fg_BRIGHT_WHITE | bg_BRIGHT_BLACK), 
                      "Baudrate");
    backupColor = getCurrentColor();

    updateBaudRateSelection(xPos, yPos, currentlySelected);

    setColor(backupColor);

    drawCancelBox(xPos + 3, yPos + height - 2);
    drawOKBox(xPos + width - 10, yPos + height - 2);

    positionBaudRateCursor(xPos, yPos, currentlySelected);
} /* end baudrateSelectBoxDraw */


unsigned int baudrateLoop(unsigned int currentlySelected)
{
    unsigned char   action                  = 0;
    unsigned int    done                    = False;
    unsigned char   backupColor             = getCurrentColor();
    unsigned int    workCurrentlySelected   = currentlySelected;

    baudrateSelectBoxDraw(baudrateX, baudrateY, baudrateWidth, baudrateHeight, currentlySelected);

    while (!done)
    {
        action = keyboardInput();

        switch(action)
        {
            case UP:
                if (workCurrentlySelected != 0)
                    workCurrentlySelected--;
                break;
            case DOWN:
                if (workCurrentlySelected < (nrOfSupportedBaudrates - 1))
                    workCurrentlySelected++;
                break;
            case RETURN:
                currentlySelected = workCurrentlySelected;
                done = True;
                break;
            case ESCAPE:
                done = True;
            default:
                break;
        } /* end switch */
        updateBaudRateSelection(baudrateX, baudrateY, workCurrentlySelected);
        positionBaudRateCursor(baudrateX, baudrateY, workCurrentlySelected);
    } /* end while */

    setColor(backupColor);
    printChar(clearScreen);

    return currentlySelected;
} /* end baudrateLoop */

/* ************************************************************************* */


void colorSelectBoxDraw(unsigned int xPos,
                        unsigned int yPos,
                        unsigned int width,
                        unsigned int height,
                        unsigned int currentlySelectedColors)
{
    unsigned int    ctr                     = 0;
    unsigned char   backupColor             = 0;
    unsigned int    selectedFontColor       = (currentlySelectedColors & 0x0F);
    unsigned int    selectedBackgroundColor = ((currentlySelectedColors >> 4) & 0x0F);

    drawBoxWithColors(xPos, 
                      yPos, 
                      width, 
                      height, 
                      (unsigned char)(fg_BRIGHT_WHITE | bg_BRIGHT_BLACK), 
                      (unsigned char)(fg_BRIGHT_WHITE | bg_BRIGHT_BLACK), 
                      "Select colors");
    backupColor = getCurrentColor();

    positionCursor(xPos + 2, yPos + 2);
    printString("Font Color:");
    for (ctr = 0; ctr < 16; ctr++)
    {
        if (ctr != selectedFontColor)
            drawThinSmallBoxWithColors(xPos + 2 + (ctr * 3), yPos + 3, (unsigned char)(fg_BRIGHT_WHITE | bg_BRIGHT_BLACK));
        else
            drawThickSmallBoxWithColors(xPos + 2 + (ctr * 3), yPos + 3, (unsigned char)(fg_BRIGHT_WHITE | bg_BRIGHT_BLACK));
        setColor(ctr + (selectedBackgroundColor << 4));
        printChar(blockCharacter);
    } /* end for */

    setColor(backupColor);
    positionCursor(xPos + 2, yPos + 7);
    printString("Background Color:");
    for (ctr = 0; ctr < 16; ctr++)
    {
        if (ctr != selectedBackgroundColor)
            drawThinSmallBoxWithColors(xPos + 2 + (ctr * 3), yPos + 8, (unsigned char)(fg_BRIGHT_WHITE | bg_BRIGHT_BLACK));
        else
            drawThickSmallBoxWithColors(xPos + 2 + (ctr * 3), yPos + 8, (unsigned char)(fg_BRIGHT_WHITE | bg_BRIGHT_BLACK));
        setColor(ctr + (selectedBackgroundColor << 4));
        printChar(blockCharacter);
    } /* end for */

    setColor(backupColor);

    drawCancelBox(xPos + 3, yPos + height - 2);
    drawOKBox(xPos + width - 10, yPos + height - 2);

    positionCursor(xPos + 3 + (selectedFontColor * 3), yPos + 4);
} /* end colorSelectBoxDraw */


void updateToThinBox(unsigned int colorNr,
                     unsigned int colorRow)
{
    drawThinSmallBoxWithColors(colorsX + 2 + (colorNr * 3), colorsY + 3 + (colorRow * 5), (unsigned char)(fg_BRIGHT_WHITE | bg_BRIGHT_BLACK));
    setColor(colorNr + (colorRow << 4));
    printChar(blockCharacter);
    positionCursor(colorsX + 3 + (colorNr * 3), colorsY + 4 +(colorRow * 5));
} /* end updateToThinBox */


void updateToThickBox(unsigned int colorNr,
                      unsigned int colorRow)
{
    drawThickSmallBoxWithColors(colorsX + 2 + (colorNr * 3), colorsY + 3 + (colorRow * 5), (unsigned char)(fg_BRIGHT_WHITE | bg_BRIGHT_BLACK));
    setColor(colorNr + (colorRow << 4));
    printChar(blockCharacter);
    positionCursor(colorsX + 3 + (colorNr * 3), colorsY + 4 +(colorRow * 5));
} /* end updateToThickBox */


unsigned char colorSelectLoop(unsigned char currentColors)
{
    unsigned char   action              = 0;
    unsigned int    done                = False;
    unsigned char   backupColor         = getCurrentColor();
    unsigned int    workCurrentColors   = currentColors;
    unsigned int    currentX            = (workCurrentColors & 0x0F);
    unsigned int    currentY            = 0;
    unsigned int    updateColors        = False;

    colorSelectBoxDraw(colorsX, colorsY, colorsWidth, colorsHeight, currentColors);

    while (!done)
    {
        action = keyboardInput();

        switch(action)
        {
            case LEFT:
                updateToThinBox(currentX, currentY);
                if (currentX > 0)
                    currentX--;                
                updateToThickBox(currentX, currentY);
                break;
            case RIGHT:
            case TAB:
                updateToThinBox(currentX, currentY);
                if (currentX < 15)
                    currentX++;                
                updateToThickBox(currentX, currentY);
                break;
            case DOWN:
                if (currentY < 1)
                    currentY++;
                currentX = ((workCurrentColors >> 4) & 0x0F);
                updateToThickBox(currentX, currentY);
                break;
            case UP:
                if (currentY > 0)
                    currentY--;
                currentX = (workCurrentColors & 0x0F);
                updateToThickBox(currentX, currentY);
                break;
            case RETURN:
            case 'O':
            case 'o':
                done         = True;
                updateColors = True;
                break;
            case ESCAPE:
            case 'C':
            case 'c':
                done = True;
            default:
                break;
        } /* end switch */  
        if (currentY == 0)
            workCurrentColors = ((workCurrentColors & 0xF0) | currentX);
        else
            workCurrentColors = ((workCurrentColors & 0x0F) | (currentX << 4)); 
        positionCursor(colorsX + 3 + (currentX * 3), colorsY + 4 +(currentY * 5)); 
    } /* end while */

    setColor(backupColor);
    printChar(clearScreen);

    if (updateColors == True)
    {
        return workCurrentColors;
    }
    else
    {
        return currentColors;
    } /* end if */
} /* end colorSelectLoop */


/* ************************************************************************* */

void positionScreenSizeCursor(unsigned int xPos,
                              unsigned int yPos,
                              unsigned int currentlySelected)
{
    positionCursor(xPos + 5, yPos + 2 + currentlySelected);
} /* end positionScreenSizeCursor */


void updateScreenSizeSelection(unsigned int xPos,
                               unsigned int yPos,
                               unsigned int currentlySelected)
{
    unsigned int    ctr = 0;
    
    setColor((unsigned char)(fg_BLACK + bg_BRIGHT_GREEN));
    for (ctr = 0; ctr < nrOfSupportedScreenSizes; ctr++)
    {
        positionCursor(xPos + 3, yPos + 2 + ctr);
        if (ctr == currentlySelected)
            drawSelectLine(True, availableScreenSizes[ctr], 19);
        else
            drawSelectLine(False, availableScreenSizes[ctr], 19);
    } /* end for */  
} /* end updateScreenSizeSelection */


void screenSizeSelectBoxDraw(unsigned int xPos,
                             unsigned int yPos,
                             unsigned int width,
                             unsigned int height,
                             unsigned int currentlySelected)
{
    unsigned int    ctr         = 0;
    unsigned char   backupColor = 0;

    drawBoxWithColors(xPos, 
                      yPos, 
                      width, 
                      height, 
                      (unsigned char)(fg_BRIGHT_WHITE | bg_BRIGHT_BLACK), 
                      (unsigned char)(fg_BRIGHT_WHITE | bg_BRIGHT_BLACK), 
                      "ScreenSize");
    backupColor = getCurrentColor();

    updateScreenSizeSelection(xPos, yPos, currentlySelected);

    setColor(backupColor);

    drawCancelBox(xPos + 3, yPos + height - 2);
    drawOKBox(xPos + width - 10, yPos + height - 2);

    positionScreenSizeCursor(xPos, yPos, currentlySelected);
} /* end screenSizeSelectBoxDraw */


unsigned int screenSizeLoop(unsigned int currentlySelected)
{
    unsigned char   action                  = 0;
    unsigned int    done                    = False;
    unsigned char   backupColor             = getCurrentColor();
    unsigned int    workCurrentlySelected   = currentlySelected;

    screenSizeSelectBoxDraw(screenSizeX, screenSizeY, screenSizeWidth, screenSizeHeight, currentlySelected);

    while (!done)
    {
        action = keyboardInput();

        switch(action)
        {
            case UP:
                if (workCurrentlySelected != 0)
                    workCurrentlySelected--;
                break;
            case DOWN:
                if (workCurrentlySelected < (nrOfSupportedScreenSizes - 1))
                    workCurrentlySelected++;
                break;
            case RETURN:
                currentlySelected = workCurrentlySelected;
                done = True;
                break;
            case ESCAPE:
                done = True;
            default:
                break;
        } /* end switch */
        updateScreenSizeSelection(screenSizeX, screenSizeY, workCurrentlySelected);
        positionScreenSizeCursor(screenSizeX, screenSizeY, workCurrentlySelected);
    } /* end while */

    setColor(backupColor);
    printChar(clearScreen);

    return currentlySelected;  
} /* end screenSizeLoop */


/* ************************************************************************* */


void positionFontCursor(unsigned int xPos,
                        unsigned int yPos,
                        unsigned int currentlySelected)
{
    positionCursor(xPos + 5, yPos + 2 + currentlySelected);
} /* end positionFontCursor */


void updateFontSelection(unsigned int xPos,
                         unsigned int yPos,
                         unsigned int currentlySelected)
{
    unsigned int    ctr = 0;
    
    setColor((unsigned char)(fg_BLACK + bg_BRIGHT_GREEN));
    for (ctr = 0; ctr < nrOfSupportedFonts; ctr++)
    {
        positionCursor(xPos + 3, yPos + 2 + ctr);
        if (ctr == currentlySelected)
            drawSelectLine(True, availableFonts[ctr], fontSelectWidth - 11);
        else
            drawSelectLine(False, availableFonts[ctr], fontSelectWidth - 11);
    } /* end for */  
} /* end updateFontSelection */


void fontSelectBoxDraw(unsigned int xPos,
                       unsigned int yPos,
                       unsigned int width,
                       unsigned int height,
                       unsigned int currentlySelected)
{
    unsigned int    ctr         = 0;
    unsigned char   backupColor = 0;

    drawBoxWithColors(xPos, 
                      yPos, 
                      width, 
                      height, 
                      (unsigned char)(fg_BRIGHT_WHITE | bg_BRIGHT_BLACK), 
                      (unsigned char)(fg_BRIGHT_WHITE | bg_BRIGHT_BLACK), 
                      "Font");
    backupColor = getCurrentColor();

    updateFontSelection(xPos, yPos, currentlySelected);

    setColor(backupColor);

    drawCancelBox(xPos + 3, yPos + height - 2);
    drawOKBox(xPos + width - 10, yPos + height - 2);

    positionFontCursor(xPos, yPos, currentlySelected);
} /* end fontSelectBoxDraw */


unsigned int FontLoop(unsigned int currentlySelected)
{
    unsigned char   action                  = 0;
    unsigned int    done                    = False;
    unsigned char   backupColor             = getCurrentColor();
    unsigned int    workCurrentlySelected   = currentlySelected;

    fontSelectBoxDraw(fontSelectX, fontSelectY, fontSelectWidth, fontSelectHeight, currentlySelected);

    while (!done)
    {
        action = keyboardInput();

        switch(action)
        {
            case UP:
                if (workCurrentlySelected != 0)
                    workCurrentlySelected--;
                break;
            case DOWN:
                if (workCurrentlySelected < (nrOfSupportedFonts - 1))
                    workCurrentlySelected++;
                break;
            case RETURN:
                currentlySelected = workCurrentlySelected;
                done = True;
                break;
            case ESCAPE:
                done = True;
            default:
                break;
        } /* end switch */
        updateFontSelection(screenSizeX, screenSizeY, workCurrentlySelected);
        positionFontCursor(screenSizeX, screenSizeY, workCurrentlySelected);
    } /* end while */

    setColor(backupColor);
    printChar(clearScreen);

    return currentlySelected;  
} /* end screenSizeLoop */

/* ************************************************************************* */

void exitSelectBoxDraw(unsigned int xPos,
                       unsigned int yPos,
                       unsigned int width,
                       unsigned int height)
{
    unsigned char   backupColor = 0;

    drawBoxWithColors(xPos, 
                      yPos, 
                      width, 
                      height, 
                      (unsigned char)(fg_BRIGHT_WHITE | bg_BRIGHT_BLACK), 
                      (unsigned char)(fg_BRIGHT_WHITE | bg_BRIGHT_BLACK), 
                      "Save your updated settings?");

    drawCancelBox(xPos + 3, yPos + height - 2);
    drawOKBox(xPos + width - 10, yPos + height - 2);

    positionCursor(xPos + width - 7, yPos + height - 2);
} /* end exitSelectBoxDraw */

/* ************************************************************************* */

unsigned int exitLoop(void)
{
    unsigned char   action      = 0;
    unsigned int    done        = False;
    unsigned char   backupColor = getCurrentColor();
    unsigned int    exitCode    = False;

    exitSelectBoxDraw(exitX, exitY, exitWidth, exitHeight);

    while (!done)
    {
        action = keyboardInput();

        switch(action)
        {
            case RETURN:
            case 'O':
            case 'o':
                exitCode    = True;
                done        = True;
                break;
            case ESCAPE:
            case 'C':
            case 'c':
                exitCode    = False;
                done        = True;
            default:
                break;
        } /* end switch */
    } /* end while */

    setColor(backupColor);
    printChar(clearScreen);

    return exitCode;  
} /* end screenSizeLoop */

/* ************************************************************************* */

unsigned int menuItemXposOffset(int selectedItem)
{
    unsigned int    idx     = 0;
    unsigned int    offset  = 0;

    if (selectedItem == 0)
        return 0;
        
    for (idx = 0; idx<selectedItem; idx++)
        offset = offset + strlen(menuItem[idx]) + 2;

    return offset;
} /* end menuItemXposOffset */


void drawMenuBar()
{
    unsigned char   backupColor = getCurrentColor();
    unsigned int    ctr         = 0;

    positionCursor(0, 0);
    setColor((unsigned char)(fg_YELLOW | bg_BRIGHT_BLACK));
    for (ctr=0; ctr<80; ctr++)
        printChar(' ');
    setColor(backupColor);

    positionCursor(0, 0);
    for (ctr = 0; ctr < nrOfMenuItems; ctr++)
    {
        setColor((unsigned char)(fg_RED | bg_BRIGHT_BLACK));
        printChar(menuItem[ctr][0]);
        setColor((unsigned char)((setupBackgroundColor &0x0F) | bg_BRIGHT_BLACK));
        printString(&(menuItem[ctr][1]));
        printString("  ");
    } /* end for */

    setColor((unsigned char)(fg_BLACK | bg_BRIGHT_BLACK));
    printString(terminalVersionString);

    setColor(backupColor);
} /* drawMenuBar */


unsigned int loopMenuBar()
{
    unsigned char   action              = 0;
    unsigned int    done                = False;
    unsigned int    selectMenuItem      = 0;
    unsigned char   currentBaudRate     = (unsigned char)kTerminalBaudrateSetting;
    unsigned char   currentColors       = (unsigned char)kTerminalFgBgColorSetting;
    unsigned char   currentScreenSize   = (unsigned char)kTerminalScreenSizeSetting;
    unsigned char   currentFont         = (unsigned char)kTerminalFontSetting;
    unsigned int    updateSettings      = False;

    drawMenuBar();
    positionCursor(menuItemXposOffset(selectMenuItem), 0);

    while (!done)
    {
        action = keyboardInput();

        switch(action)
        {
            case TAB:
                selectMenuItem = selectMenuItem + 1;
                selectMenuItem = selectMenuItem % nrOfMenuItems;
                positionCursor(menuItemXposOffset(selectMenuItem), 0);
                break;
            case RETURN:
                switch(selectMenuItem)
                {
                    case 0: // Baudrate
                        currentBaudRate = baudrateLoop(currentBaudRate);
                        break;
                    case 1: // ColorSelect
                        currentColors = colorSelectLoop(currentColors);
                        break;
                    case 2: // ScreenSize
                        currentScreenSize = screenSizeLoop(currentScreenSize);
                        break;
                    case 3: // Font
                        currentFont = FontLoop(currentFont);
                        break;
                    case 4: // Exit
                        updateSettings  = exitLoop();
                        done            = True;
                        break;
                    case 5: // Default settings
                        installDefaultSettings();
                        __asm__ ("jp $0000");
                        break;
                    default:
                        break;
                } /* end switch */
                drawMenuBar();
                positionCursor(menuItemXposOffset(selectMenuItem), 0);
                break;
            case 'B':
            case 'b':
                currentBaudRate = baudrateLoop(currentBaudRate);
                break;
            case 'C':
            case 'c':
                currentColors = colorSelectLoop(currentColors);
                break;
            case 'E':
            case 'e':
                updateSettings  = exitLoop();
                done            = True;
                break;
            case 'D':
            case 'd':
                installDefaultSettings();
                __asm__ ("jp $0000");
                break;
            case 'F':
            case 'f':
                currentFont = FontLoop(currentFont);
                break;
            case 'S':
            case 's':
                currentScreenSize = screenSizeLoop(currentScreenSize);
                break;
            default:
                break;
        } /* end switch */
        drawMenuBar();
        positionCursor(menuItemXposOffset(selectMenuItem), 0);
    } /* end while */

    if (updateSettings == True)
    {
        unsigned char  *settingsPtr = (unsigned char*)&kSettingsBackup;

        *settingsPtr       = (unsigned char)currentBaudRate;
        *(settingsPtr + 1) = (unsigned char)currentColors;
        *(settingsPtr + 2) = (unsigned char)currentScreenSize;
        *(settingsPtr + 3) = (unsigned char)currentFont;

        writeUpdatedSettingsToFlash();
    } /* end if */

    return updateSettings;
} /* end loopMenuBar */

/* ************************************************************************* */


void setup(void)
{
    unsigned char   backupColor     = getCurrentColor();
    unsigned int    updateSettings  = False;

    setColor(setupBackgroundColor);
    printChar(clearScreen);

    updateSettings = loopMenuBar();

    setColor(backupColor);
    printChar(clearScreen);
    if (updateSettings == False)
    {
        outCharA(RETURN);
    }
    else
    {   // Reset terminal to take new settings into effect.
        __asm__ ("jp $0000");
    } /* end if */
} /* end setup */

/* ************************************************************************* */