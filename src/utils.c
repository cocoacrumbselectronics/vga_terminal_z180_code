#include <string.h>

#include "externs.h"


unsigned char getCurrentColor(void)
{
    unsigned char  *colorPointer    = (unsigned char*)&kFontBgColor;

    return (unsigned char)*colorPointer;
} /* end getCurrentColor */


void setColor(unsigned char currentColor)
{
    unsigned char  *colorPointer    = (unsigned char*)&kFontBgColor;

    *colorPointer = currentColor;
} /* end setColor */

/* ************************************************************************* */

void printString(char *string)
{
    unsigned int    idx = 0;

    while (string[idx] != 0)
    {
        printChar(string[idx++]);
    } /* end while */
} /* end printString */


void printStringWithColour(char *string, unsigned char color)
{
    setColor(color);
    printString(string);
} /* end printStringWithColour */

/* ************************************************************************* */

unsigned char keyboardInput()
{
    return inCharB();
} /* end keyboardInput */
        
