#ifndef __UTILS_H__
#define __UTILS_H__

unsigned char getCurrentColor(void);
void setColor(unsigned char currentColor);

void printString(char *string);
void printStringWithColour(char *string, unsigned char color);

unsigned char keyboardInput();

#endif /* __UTILS_H__ */
