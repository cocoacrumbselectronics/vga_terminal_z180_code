// COMPILE TIME CRT CONFIGURATION

#pragma output  CRT_ORG_CODE            = 0x0000    // org of compile
#pragma output  CRT_ORG_DATA            = -1        // DATA section appended to the code section
#pragma output  CRT_ORG_BSS             = 0x8200

#pragma output  REGISTER_SP             = 0x9FFF    // typical stack location when using sp1
#pragma output  STACKPTR                = 0x87FF    // place stack at 0x87FF at startup

#pragma output  CRT_ENABLE_CLOSE        = 0         // don't bother closing files on exit
#pragma output  CRT_ENABLE_EIDI         = 0x0       // disable interrupts at start
; #pragma output CRT_ON_EXIT            = 0x30002   // return to basic on exit

#pragma output  CLIB_MALLOC_HEAP_SIZE   = 0         // no malloc heap
#pragma output  CLIB_STDIO_HEAP_SIZE    = 0         // no stdio heap (no files)

#pragma output  CLIB_FOPEN_MAX          = -1        // no FILE* list
#pragma output  CLIB_OPEN_MAX           = -1        // no fd table

// DON'T CREATE A BLOCK MEMORY ALLOCATOR WITH ONE QUEUE

#pragma output  CLIB_BALLOC_TABLE_SIZE  = 0

// SET UP INTERRUPTS AND ITS VECTOR TABLE
#pragma output  CRT_IO_VECTOR_BASE      = 0
#pragma output  CRT_ORG_VECTOR_TABLE    = 0x8000    // Needs be synced with _kInterruptVectorTable in defines.asm !!!
#pragma output  CRT_INTERRUPT_MODE      = 2